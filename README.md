# XivoCC Configuration Server

 This is the recording server of XivoCC

# Configuration
## play.http.context
This configuration key allows you to move the "root" URL to for example /recording. When you can't use a subdomain,
this is a way allowing two applications to exist in one http server.

# Development

Create database :

CREATE USER xuc WITH PASSWORD 'xuc';
CREATE DATABASE xuc_rights WITH OWNER xuc;

CREATE USER recording WITH PASSWORD 'recording';
CREATE DATABASE recording WITH OWNER recording;

## Running in dev

Create a conf file named application-*.conf like `application-dev.conf` and fill the variable : 

```hocon
include "application.conf"

reportinghost ="IP_XIVOCC_HOST"
reportingdbport = "5443" /it's the port of pgxivocc database
configManagement.host = "IP_XIVO_HOST"
dbhost = "IP_XIVOCC_HOST" /In this case dbhost is the host of pgxivocc db
PLAY_AUTH_TOKEN = "PLAY_AUTH_TOKEN" / You can retreive it from xivocc custom.env
```

You can also directly override a configuration key if no env variable is defined.

Starting the application in play framework :

```sh
sbt run -Dconfig.file=conf/application-dev.conf
```

## Testing

**Prerequistes:** 
* a local PostgreSQL instance, with a database asterisktest owned by user asterisk (password: asterisk)

    CREATE USER asterisk WITH PASSWORD 'asterisk';
    CREATE DATABASE asterisktest WITH OWNER asterisk;

    \c asterisktest
    CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

* PostgreSQL module "uuid-ossp" loaded
* You need a current version of the gecko driver available from: https://github.com/mozilla/geckodriver/releases
* You need to harmonize the Selenium library version with the version of your Firefox, potentially also the Guava dependency version.

You must run sbt (or activator) with java options as follows:
```
sbt -Dwebdriver.gecko.driver=PATH_TO_THE_GECKO_DRIVER
```
e.g. `sbt -Dwebdriver.gecko.driver=/home/jirka/dev/tools/geckodriver`

Then during the test Firefox is started and tests are run with Firefox, which is closed once tests are finished.

If you encounter a problem with integration tests check the versions as described above.

New unit tests use Karma:
```
npm ci
npm run test
```

## Docker

    $ docker run -ti --rm --name recordingsrv -e XIVO_HOST=192.168.56.3 --link compose_xuc-rights-mgt_1:xucrightmgt \
    --link compose_postgresrepoing_1:db --link compose_postgresreporting_1:reporting -p 9999:9000 xivoxc/recording-server:1.13.1


## Environment variables
- REPORTING_HOST
- REPORTING_DATABASE_NAME
- XUC_RIGHT_MGT_HOST
- DB_HOST
- DB_NAME
- XIVO_HOST

## License


This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

See the [COPYING](COPYING) and [COPYING.LESSER](COPYING.LESSER) files for details.
