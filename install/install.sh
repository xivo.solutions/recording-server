#!/bin/bash
docker pull xivoxc/recording-server

mkdir -p /var/spool/recording-server
chown daemon:daemon /var/spool/recording-server
chmod 760 /var/spool/recording-server
mkdir /var/log/recording-server
chown daemon:daemon /var/log/recording-server
mkdir -p /etc/docker/recording-server/conf

cd /etc/docker/recording-server
wget https://gitlab.com/xivo-recording/recording-server/raw/master/install/docker-run.conf
wget https://gitlab.com/xivo-recording/recording-server/raw/master/conf/application.conf
mv application.conf conf/
