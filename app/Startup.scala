import com.google.inject.AbstractModule
import org.slf4j.{Logger, LoggerFactory}
import startup.ApplicationStart

import scala.annotation.unused

@unused // Used in reference.conf
class Startup extends AbstractModule {

  val logger: Logger = LoggerFactory.getLogger(getClass)

  override def configure(): Unit = {
    logger.info("Configuring Startup Module")
    bind(classOf[ApplicationStart]).asEagerSingleton()
  }
}
