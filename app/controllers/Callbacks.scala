package controllers

import configuration.AuthConfig
import models.{CallbackTicket, CallbackTicketDao, CallbackTicketPatch}
import org.slf4j.LoggerFactory
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.mvc.{Call, EssentialAction, InjectedController, Result}

import java.security.InvalidParameterException
import javax.inject.Inject
import scala.annotation.unused

@unused // Used in routes.conf
class Callbacks @Inject() (
    val authConfig: AuthConfig,
    callbackTicketDao: CallbackTicketDao,
    secured: Secured
) extends InjectedController {
  private val logger = LoggerFactory.getLogger(getClass)

  @unused // Used in routes.conf
  def createTicket: EssentialAction =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request => {
          request.body.asJson match {
            case None =>
              BadRequest(
                Json.toJson("Callbacks createTicket : No JSON could be decoded")
              )
            case Some(json) =>
              json.validate[CallbackTicket] match {
                case JsError(e) =>
                  logger.error("Callbacks createTicket ", e)
                  BadRequest(Json.toJson(e.toString()))
                case JsSuccess(ticket, _) =>
                  logger.info(
                    s"$user - Callbacks - Req: <createTicket> $ticket"
                  )
                  val res = callbackTicketDao.create(
                    ticket.copy(lastUpdate = Some(ticket.started))
                  )
                  Created(Json.toJson(res))
              }
          }
        }
    )

  @unused // Used in routes.conf
  def updateTicket(uuid: String): EssentialAction =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          WithExceptionCatching {
            request.body.asJson match {
              case None =>
                BadRequest(
                  Json.toJson("updateTicket : No JSON could be decoded")
                )
              case Some(json) =>
                json.validate[CallbackTicketPatch] match {
                  case JsError(e) =>
                    logger.error("updateTicket ", e)
                    BadRequest(Json.toJson(e.toString()))
                  case JsSuccess(patch, _) =>
                    logger.info(
                      s"$user - Callbacks - Req: <updateTicket> $patch"
                    )
                    callbackTicketDao.update(uuid, patch)
                    Ok("")
                }
            }
          }
    )

  @unused // Used in routes.conf
  def exportTickets(listUuid: String): EssentialAction =
    secured.WithAuthenticatedUser(
      None,
      user =>
        _ =>
          WithExceptionCatching {
            logger.info(s"$user - Callbacks - Req: <exportTickets> $listUuid")
            Ok(CallbackTicket.toCsv(callbackTicketDao.byList(listUuid)))
              .withHeaders(
                "Content-Type"        -> "text/csv",
                "Content-Disposition" -> s"attachment;filename=$listUuid.csv"
              )
          }
    )

  @unused // Used in routes.conf
  def getTicket(uuid: String): EssentialAction =
    secured.WithAuthenticatedUser(
      None,
      user =>
        _ =>
          WithExceptionCatching {
            logger.info(s"$user - Callbacks - Req: <getTicket> $uuid")
            Ok(Json.toJson(callbackTicketDao.getTicket(uuid)))
          }
    )

  val loginRoute: Call = routes.Login.login()

  private def WithExceptionCatching(f: => Result): Result =
    try {
      f
    } catch {
      case e: InvalidParameterException =>
        logger.error("Callbacks - InvalidParameterException", e)
        BadRequest(Json.toJson(e.getMessage))
      case e: NoSuchElementException =>
        logger.error("Callbacks - NoSuchElementException", e)
        NotFound(Json.toJson(e.getMessage))
      case e: Exception =>
        logger.error("Callbacks - Exception", e)
        InternalServerError(Json.toJson(e.getMessage))
        throw e
    }
}
