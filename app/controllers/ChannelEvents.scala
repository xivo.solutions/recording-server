package controllers

import configuration.AuthConfig
import models.{ChannelEventDao, GenericError, NotHandledError}
import play.api.libs.json.Json
import play.api.mvc.{Call, EssentialAction, InjectedController}

import javax.inject.Inject
import scala.util.{Failure, Success}

class ChannelEvents @Inject() (
    val authConfig: AuthConfig,
    channelEventDao: ChannelEventDao,
    secured: Secured
) extends InjectedController {
  val loginRoute: Call = routes.Login.login()

  def list(from: Int, limit: Int): EssentialAction =
    secured.WithAuthenticatedUser(
      None,
      _ =>
        _ => {
          channelEventDao.list(from, limit) match {
            case Failure(f) =>
              GenericError(
                NotHandledError,
                s"Unable to get cels : ${f.getMessage}"
              ).toResult
            case Success(cels) => Ok(Json.toJson(cels))
          }
        }
    )
}
