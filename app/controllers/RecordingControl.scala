package controllers

import configuration.AuthConfig

import javax.inject.Inject
import models.authentication.RightsHelper
import models.{
  FilteredIncalls,
  FilteredInternalNumbers,
  NumberAlreadyExistsException
}
import play.api.db.{Database, NamedDatabase}
import play.api.libs.json.{JsString, Json}
import play.api.mvc._
import play.api.i18n.I18nSupport

import scala.annotation.unused

class RecordingControl @Inject() (
    val authConfig: AuthConfig,
    val rightsHelper: RightsHelper,
    @NamedDatabase("default") dbDefault: Database,
    cc: ControllerComponents,
    secured: Secured
) extends AbstractController(cc)
    with RightsCheck
    with I18nSupport {

  val loginRoute: Call = routes.Login.login()

  @unused // Used in routes.conf
  def page(): EssentialAction =
    secured.IsAuthenticated(
      Some(loginRoute),
      implicit request => {
        if (isAdmin())
          Ok(views.html.recording_control(rightsHelper))
        else
          Forbidden(
            views.html
              .forbidden(rightsHelper)(request.lang, request, request.messages)
          )
      }
    )

  @unused // Used in routes.conf
  def allFilteredIncalls(): EssentialAction =
    secured.IsAuthenticated(
      None,
      implicit request => {
        if (isAdmin())
          dbDefault.withConnection(implicit c =>
            Ok(Json.toJson(FilteredIncalls.all()))
          )
        else
          Forbidden
      }
    )

  @unused // Used in routes.conf
  def getFilteredIncall(number: String): Action[AnyContent] =
    Action { _ =>
      dbDefault.withConnection(implicit c =>
        FilteredIncalls.find(number) match {
          case Some(n) => Ok(Json.toJson(n))
          case None    => NotFound
        }
      )
    }

  @unused // Used in routes.conf
  def deleteFilteredIncall(number: String): EssentialAction =
    secured.IsAuthenticated(
      None,
      implicit request => {
        if (isAdmin()) {
          dbDefault.withConnection(implicit c => FilteredIncalls.delete(number))
          NoContent
        } else Forbidden
      }
    )

  @unused // Used in routes.conf
  def createFilteredIncall: EssentialAction =
    secured.IsAuthenticated(
      None,
      implicit request => {
        if (isAdmin()) {
          request.body.asJson match {
            case Some(JsString(number)) =>
              try {
                dbDefault.withConnection(implicit c =>
                  FilteredIncalls.create(number)
                )
                Created
              } catch {
                case _: NumberAlreadyExistsException =>
                  BadRequest(Json.toJson(s"Le numéro $number existe déjà"))
              }
            case _ => BadRequest
          }
        } else Forbidden
      }
    )

  @unused // Used in routes.conf
  def allFilteredInternalNumbers: EssentialAction =
    secured.IsAuthenticated(
      None,
      implicit request => {
        if (isAdmin())
          dbDefault.withConnection(implicit c =>
            Ok(Json.toJson(FilteredInternalNumbers.all()))
          )
        else
          Forbidden
      }
    )

  @unused // Used in routes.conf
  def getFilteredInternalNumber(num: String): Action[AnyContent] =
    Action { _ =>
      dbDefault.withConnection(implicit c =>
        FilteredInternalNumbers.find(num) match {
          case None         => NotFound
          case Some(number) => Ok(Json.toJson(number))
        }
      )
    }

  @unused // Used in routes.conf
  def createFilteredInternalNumber: EssentialAction =
    secured.IsAuthenticated(
      None,
      implicit request => {
        if (isAdmin()) {
          request.body.asJson match {
            case Some(JsString(number)) =>
              try {
                dbDefault.withConnection(implicit c =>
                  FilteredInternalNumbers.create(number)
                )
                Created
              } catch {
                case _: NumberAlreadyExistsException =>
                  BadRequest(Json.toJson(s"Le numéro $number existe déjà"))
              }
            case _ => BadRequest
          }
        } else Forbidden
      }
    )

  @unused // Used in routes.conf
  def deleteFilteredInternalNumber(number: String): EssentialAction =
    secured.IsAuthenticated(
      None,
      implicit request => {
        if (isAdmin()) {
          dbDefault.withConnection(implicit c =>
            FilteredInternalNumbers.delete(number)
          )
          NoContent
        } else Forbidden
      }
    )

}
