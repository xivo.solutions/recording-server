package controllers

import configuration.AuthConfig
import models.authentication.RightsHelper
import models.{CsvLoggerHelper, Record}
import org.slf4j.{Logger, LoggerFactory}
import play.api.Configuration
import play.api.db.{Database, NamedDatabase}
import play.api.mvc._

import java.io._
import java.nio.charset.StandardCharsets
import java.nio.file.{Path, Paths, Files => nioFiles}
import javax.inject.Inject
import scala.jdk.CollectionConverters._
import scala.util.{Properties, Try}
import play.api.i18n.I18nSupport

import java.util

class Files @Inject() (
    val authConfig: AuthConfig,
    configuration: Configuration,
    val rightsHelper: RightsHelper,
    csvHelper: CsvLoggerHelper,
    @NamedDatabase("stats") dbStats: Database,
    cc: ControllerComponents,
    secured: Secured
) extends AbstractController(cc)
    with RightsCheck
    with I18nSupport {

  val audioFolder: String = configuration.get[String]("audio.folder")
  val audioExtensions: util.List[String] =
    configuration.underlying.getStringList("audio.extensions")
  private val fallbackUrl: String =
    configuration.getOptional[String]("audio.fileFallbackUrl").getOrElse("")

  val logger: Logger = LoggerFactory.getLogger(getClass.getName)

  def get(globalId: String, action: String = "result"): EssentialAction =
    secured.IsAuthenticated(
      Some(loginRoute),
      implicit request => {
        val file   = new File(globalId).getName
        val gwName = file.split("-").head
        dbStats.withConnection({ implicit c =>
          Record
            .callidByGlobalId(globalId)
            .foreach(callId =>
              csvHelper.logCsv(
                generateCsvLine(
                  request.session.get("username").getOrElse("unknown"),
                  file,
                  callId,
                  action
                )
              )
            )
        })

        action match {
          case "listen" => Ok
          case "result" | "download" =>
            val extension = audioExtensions.asScala.filter(audioExtension =>
              new File(s"$audioFolder/$gwName/$file.$audioExtension").exists()
            )
            if (extension.nonEmpty) {
              val realFile =
                new File(s"$audioFolder/$gwName/$file.${extension.head}")
              RangeResult.ofFile(
                realFile,
                request.headers.get(RANGE),
                Some(s"audio/${extension.head}")
              )
            } else if (fallbackUrl.nonEmpty) {
              logger.warn(
                s"File $audioFolder/$gwName/$file.[$audioExtensions] not found locally, redirecting to backup url"
              )
              Redirect(fallbackUrl.format(globalId))
            } else {
              logger.warn(
                s"File $audioFolder/$gwName/$file.[$audioExtensions] not found"
              )
              NotFound(views.html.nosuchfile(rightsHelper))
            }
          case _ => BadRequest
        }
      }
    )

  def generateCsvLine(
      username: String,
      filename: String,
      recordingId: String,
      action: String
  ): String = {
    List(username, filename, recordingId, action).mkString(",")
  }

  private def generateCsvHeader: String = {
    List("date", "username", "filename", "recordingid", "action")
      .mkString(",")
      .concat(Properties.lineSeparator)
  }

  private def listCsvLogFiles(filePath: String): Option[List[Path]] = {
    val p = Paths.get(filePath)

    Try(nioFiles.newDirectoryStream(p))
      .map(stream =>
        stream
          .iterator()
          .asScala
          .toList
          .filter(_.getFileName.toString.startsWith("access."))
          .filter(_.toString.endsWith(".csv"))
          .sortBy(_.getFileName)
      )
      .toOption
      .filter(_.nonEmpty)
  }

  private def buildCsvResponse(files: List[Path]): String = {
    val charset = StandardCharsets.UTF_8

    val stringBuilder = new StringBuilder();
    stringBuilder.append(generateCsvHeader)
    files.foreach(file =>
      stringBuilder.append(
        new String(
          nioFiles.readAllBytes(file),
          charset
        )
      )
    )
    stringBuilder.toString()
  }

  def getAccessLog: EssentialAction =
    secured.IsAuthenticated(
      None,
      implicit request => {
        if (isAdmin()) {
          listCsvLogFiles(csvHelper.filePath) match {
            case Some(files) =>
              Ok(buildCsvResponse(files))
                .as("text/csv")
                .withHeaders(
                  "Content-Disposition" -> s"attachment;filename=access.csv"
                )
            case None => NotFound
          }
        } else
          Forbidden
      }
    )

  val loginRoute: Call = routes.Login.login()
}
