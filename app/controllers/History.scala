package controllers

import java.security.InvalidParameterException
import configuration.AuthConfig
import date_helpers.DateHelper

import javax.inject.Inject
import models.CallDetailDao
import play.api.Logger
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.mvc._
import xc.{CustomerCallDetailDao, FindCustomerCallHistoryRequest}

import scala.annotation.unused

class History @Inject() (
    val authConfig: AuthConfig,
    customerCallDetailDao: CustomerCallDetailDao,
    callDetailDao: CallDetailDao,
    dateHelper: DateHelper,
    secured: Secured
) extends InjectedController {
  val format         = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
  val logger: Logger = Logger(getClass.getName)

  def search: EssentialAction =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          WithExceptionCatching {
            val queryMap = request.queryString.map { case (k, v) =>
              k -> v.mkString
            }

            if (queryMap.size > 1)
              badReqWithLog(
                "search",
                s"Should not try to get the call history with multiple arguments at the same time: $queryMap has too many arguments"
              )
            else
              request.body.asJson.flatMap(value =>
                (value \ "interface").asOpt[String]
              ) match {
                case None =>
                  badReqWithLog(
                    "search",
                    "Missing required parameter interface"
                  )
                case Some(interface) =>
                  queryMap.headOption match {
                    case None =>
                      logger.info(
                        s"$user - History - Req: <search> - interface: $interface, size: 10"
                      )
                      Ok(
                        Json.toJson(
                          callDetailDao.historyByInterface(interface, 10)
                        )
                      )
                    case Some(("size", size)) =>
                      logger.info(
                        s"$user - History - Req: <search> - interface: $interface, size: $size"
                      )
                      Ok(
                        Json.toJson(
                          callDetailDao.historyByInterface(
                            interface,
                            size.toInt
                          )
                        )
                      )

                    case Some(("days", days)) =>
                      logger.info(
                        s"$user - History - Req: <search> - interface: $interface, nb of days: $days"
                      )
                      Ok(
                        Json.toJson(
                          callDetailDao.historyByDate(
                            interface,
                            dateHelper.dateByNumberOfDays(days.toInt)
                          )
                        )
                      )
                    case Some(something) =>
                      badReqWithLog(
                        "search",
                        s"Unrecognized argument when getting the call history: $something"
                      )
                  }
              }
          }
    )

  def searchAgent(size: Int, days: Int): EssentialAction =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          WithExceptionCatching {
            request.body.asJson.flatMap(value =>
              (value \ "agentNum").asOpt[String]
            ) match {
              case None => badReqWithLog("searchAgent")
              case Some(agentNumber) =>
                logger.info(
                  s"$user - History - Req: <searchAgent> - agentNumber: $agentNumber, size: $size, nb of days: $days"
                )
                Ok(
                  Json.toJson(
                    callDetailDao.historyByAgentNum(
                      agentNumber,
                      size,
                      dateHelper.dateByNumberOfDays(days.toInt)
                    )
                  )
                )
            }
          }
    )

  @unused // Used in routes.conf
  def searchCustomer: EssentialAction =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          WithExceptionCatching {
            request.body.asJson match {
              case None => badReqWithLog("searchCustomer", "No JSON found")
              case Some(json) =>
                json.validate[FindCustomerCallHistoryRequest] match {
                  case JsSuccess(f, _) =>
                    logger.info(
                      s"$user - History - Req: <searchCustomer> - filters: ${f.filters}, size: ${f.size}"
                    )
                    Ok(
                      Json.toJson(
                        customerCallDetailDao.findHistoryByCustomer(
                          f.filters,
                          f.size
                        )
                      )
                    )
                  case JsError(e) =>
                    badReqWithLog("searchCustomer", e.toString())
                }
            }
          }
    )

  private def WithExceptionCatching(f: => Result): Result =
    try {
      f
    } catch {
      case e: InvalidParameterException =>
        badReqWithLog("exception InvalidParameter", e.getMessage)
      case e: NoSuchElementException =>
        logger.error(s"History - exception NoSuchElement ${e.getMessage}")
        NotFound(Json.toJson(e.getMessage))
      case e: Exception =>
        logger.error(s"History - exception ${e.getMessage}")
        InternalServerError(Json.toJson(e.getMessage))
    }

  val loginRoute: Call = routes.Login.login()

  private def badReqWithLog(action: String, message: String = "") = {
    logger.error(s"History - Req: <$action> $message")
    BadRequest(Json.toJson(message))
  }
}
