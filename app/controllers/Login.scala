package controllers

import configuration.AuthConfig
import models.{
  AuthenticatedToken,
  AuthenticatedUser,
  CredentialsValidation,
  InvalidCredentials
}
import models.authentication.{
  AuthenticationProviderImpl,
  RightsHelper,
  SupervisorRight,
  TeacherRight
}
import play.api._
import play.api.data.Form
import play.api.data.Forms._
import play.api.http.HttpEntity
import play.api.libs.ws.WSClient
import play.api.mvc._
import views.html
import play.api.i18n.I18nSupport

import javax.inject.Inject
import play.api.i18n.Messages

import scala.annotation.unused

class Login @Inject() (
    authConfig: AuthConfig,
    wsClient: WSClient,
    rightsHelper: RightsHelper
) extends InjectedController
    with I18nSupport {

  val logger: Logger = Logger(getClass.getName)
  private val authProvider =
    new AuthenticationProviderImpl(authConfig, wsClient)

  def login: Action[AnyContent] =
    Action { implicit req => Ok(html.login(loginForm)) }

  private val loginForm = Form(tuple("login" -> text, "mdp" -> text))

  @unused // Used in routes.conf
  def authenticate: Action[AnyContent] =
    Action { implicit request =>
      val isSample = checkIfSample(request)
      loginForm
        .bindFromRequest()
        .fold(
          formWithErrors => {
            BadRequest(html.login(formWithErrors))
          },
          {
            case (login, pwd) =>
              authProvider.authenticate(login, pwd) match {
                case Some(user) =>
                  checkRights(user, isSample)
                case _ =>
                  BadRequest(
                    html.login(
                      loginForm
                        .withGlobalError(Messages("InvalidLoginOrPassword"))
                    )
                  )
              }
            case _ =>
              BadRequest(
                html.login(
                  loginForm.withGlobalError(Messages("InvalidRequestFormat"))
                )
              )
          }
        )
    }

  def checkRights(user: CredentialsValidation, isSample: Boolean = false)(
      implicit request: Request[_]
  ): Result = {
    user match {
      case AuthenticatedUser(username, superAdmin) =>
        rightsHelper.getFreshUserRights(username) match {
          case None if superAdmin =>
            Redirect(routes.Application.index())
              .withSession("username" -> username, "isSuperAdmin" -> "true")
          case None =>
            BadRequest(
              html.login(
                loginForm.withGlobalError(Messages("NoAccessRights"))
              )
            )
          case Some(SupervisorRight(_, _, _, false, _)) =>
            BadRequest(
              html.login(
                loginForm.withGlobalError(Messages("NoAccessRights"))
              )
            )
          case Some(TeacherRight(_, _, _, start, end))
              if end.isBeforeNow || start.isAfterNow =>
            BadRequest(
              html.login(
                loginForm.withGlobalError(Messages("InvalidAccessRights"))
              )
            )
          case _ =>
            if (isSample)
              Result(ResponseHeader(OK), HttpEntity.NoEntity).withSession(
                "username" -> username
              )
            else
              Redirect(routes.Application.index())
                .withSession("username" -> username)
        }

      case AuthenticatedToken =>
        Redirect(routes.Application.index())
          .withSession("isSuperAdmin" -> "true")
      case InvalidCredentials =>
        BadRequest(
          html.login(
            loginForm.withGlobalError(Messages("NoAccessRights"))
          )
        )
    }

  }

  @unused // Used in routes.conf
  def logout: Action[AnyContent] =
    Action { implicit request =>
      logger.info(
        s"L'utilisateur ${request.session.get("username")} s'est déconnecté"
      )
      if (checkIfSample(request))
        Result(ResponseHeader(OK), HttpEntity.NoEntity).withNewSession
      else Redirect(routes.Login.login()).withNewSession
    }

  private def checkIfSample(request: Request[AnyContent]): Boolean = {
    val queryMap = request.queryString.map { case (k, v) => k -> v.mkString }
    if (queryMap.contains("isSample") && queryMap("isSample") == "true") true
    else false
  }
}
