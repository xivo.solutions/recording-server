package controllers

import models.Record
import org.joda.time.format.{PeriodFormatter, PeriodFormatterBuilder}
import play.api.libs.json.{JsObject, JsString, JsValue, Json}

class PaginationHelper {
  val format = "yyyy-MM-dd HH:mm:ss"
  val periodFormat: PeriodFormatter = new PeriodFormatterBuilder()
    .minimumPrintedDigits(2)
    .printZeroAlways()
    .appendHours()
    .appendSeparator(":")
    .appendMinutes()
    .appendSeparator(":")
    .appendSeconds()
    .toFormatter

  def paginateAndJsonify(records: List[Record], pageSize: Int): JsValue = {
    Json.toJson(
      Json.obj(
        "hasNext" -> (records.size > pageSize),
        "records" -> toJson(
          if (records.size > pageSize) records.init else records
        )
      )
    )
  }

  def toJson(records: List[Record]): List[JsObject] =
    records.map(record =>
      Json.obj(
        "id"    -> record.recordId,
        "start" -> record.start.toString(format),
        "duration" -> JsString(record.duration match {
          case None    => ""
          case Some(d) => periodFormat.print(d.toPeriod())
        }),
        "src_num"       -> record.srcNum,
        "dst_num"       -> record.dstNum,
        "agent"         -> record.agent,
        "queue"         -> record.queue,
        "status"        -> record.status.toString,
        "attached_data" -> record.attachedData,
        "unique-id"     -> record.recordId.map(_.split("-").last)
      )
    )

}
