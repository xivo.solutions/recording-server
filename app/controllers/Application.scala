package controllers

import configuration.AuthConfig
import models._
import models.authentication.{AdminRight, Right, RightsHelper}
import play.api._
import play.api.db.{Database, NamedDatabase}
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.mvc._
import play.api.i18n.I18nSupport

import java.security.InvalidParameterException
import javax.inject.Inject
import scala.annotation.unused

class Application @Inject() (
    paginationHelper: PaginationHelper,
    val authConfig: AuthConfig,
    val rightsHelper: RightsHelper,
    @NamedDatabase("stats") dbStats: Database,
    secured: Secured
) extends InjectedController
    with I18nSupport {

  private lazy val httpContext =
    authConfig.config.getOptional[String]("play.http.context")

  val loginRoute: Option[Call] = Some(
    Call("GET", s"${httpContext.getOrElse("")}${routes.Login.login()}")
  )

  val logger: Logger = Logger(getClass.getName)

  def index(): EssentialAction =
    secured.IsAuthenticated(
      loginRoute,
      implicit request =>
        Ok(
          views.html
            .index(rightsHelper)(request.lang, request, request.messages)
        )
    )

  def redirect(page: String): Action[AnyContent] =
    Action(Redirect(s"${httpContext.getOrElse("")}/$page"))

  def simpleSearch(page: Int, pageSize: Int): EssentialAction =
    secured.WithAuthenticatedUser(
      None,
      implicit user =>
        implicit request =>
          WithExceptionCatching {
            dbStats.withConnection(implicit connection => {
              val offset = (page - 1) * pageSize
              implicit val paginator: Paginator =
                Paginator(offset, pageSize + 1)
              request.body.asJson match {
                case None => BadRequest(Json.toJson("No JSON found"))
                case Some(query) =>
                  logger.info(
                    s"$user - Recordings - simple search request : $query  $page"
                  )
                  query.validate[SimpleSearchCriteria] match {
                    case JsSuccess(criteria, _) =>
                      criteria.validate()
                      WithAuthenticatedRights(
                        user,
                        {
                          case None =>
                            BadRequest("No right found for the user")
                          case Some(r) =>
                            val records = Record.search(criteria, r)
                            val resultRecords = {
                              if (records.nonEmpty)
                                Record.recordsWithTransfers(
                                  records,
                                  Record.transfer(records)
                                )
                              else
                                records
                            }
                            Ok(
                              paginationHelper.paginateAndJsonify(
                                resultRecords,
                                pageSize
                              )
                            )
                        }
                      )
                    case JsError(e) => BadRequest(s"Cannot decode JSON: $e")
                  }
              }
            })
          }
    )

  def search(page: Int, pageSize: Int): EssentialAction =
    secured.WithAuthenticatedUser(
      None,
      implicit user =>
        implicit request =>
          WithExceptionCatching {
            dbStats.withConnection(implicit connection => {
              val offset = (page - 1) * pageSize
              implicit val paginator: Paginator =
                Paginator(offset, pageSize + 1)
              request.body.asJson match {
                case None => BadRequest(Json.toJson("No JSON found"))
                case Some(query) =>
                  logger.info(
                    s"$user - Recordings - search query : $query $page"
                  )
                  query.validate[SearchCriteria] match {
                    case JsSuccess(criteria, _) =>
                      criteria.validate()
                      WithAuthenticatedRights(
                        user,
                        {
                          case None =>
                            BadRequest("No right found for the user")
                          case Some(r) =>
                            val records = Record.search(criteria, r)
                            val resultRecords = {
                              if (records.nonEmpty)
                                Record.recordsWithTransfers(
                                  records,
                                  Record.transfer(records)
                                )
                              else
                                records
                            }
                            Ok(
                              paginationHelper.paginateAndJsonify(
                                resultRecords,
                                pageSize
                              )
                            )
                        }
                      )
                    case JsError(e) => BadRequest(s"Cannot decode JSON: $e")
                  }
              }
            })
          }
    )

  @unused // Used in routes.conf
  def callidSearch(callid: String): EssentialAction =
    secured.WithAuthenticatedUser(
      None,
      implicit user =>
        _ => {
          dbStats.withConnection(implicit c => {
            implicit val paginator: Paginator = Paginator(0, 1)
            WithAuthenticatedRights(
              user,
              { right =>
                right match {
                  case None => BadRequest("No right found for the user")
                  case Some(right) =>
                    logger.info(
                      s"$user - Recordings - Requête pour le call with id: $callid"
                    )
                    val records = Record.searchByCallid(callid, right)
                    Ok(paginationHelper.paginateAndJsonify(records, 1))
                }
              }
            )
          })
        }
    )

  def addAttachedDataById(id: Long): EssentialAction =
    secured.IsAuthenticated(
      None,
      implicit request => {
        request.body.asJson match {
          case Some(json) =>
            json.validate[List[AttachedData]] match {
              case JsSuccess(dataList, _) =>
                dbStats.withConnection(implicit c =>
                  Calls.addAttachedDataById(id, dataList)
                )
                Created("")
              case JsError(e) => BadRequest(s"Error decoding JSON: $e")
            }
          case None => BadRequest("No JSON could be decoded")
        }
      }
    )

  @unused // Used in routes.conf
  def addAttachedDataByLinkedId(id: String): EssentialAction =
    secured.IsAuthenticated(
      None,
      implicit request => {
        request.body.asJson match {
          case Some(json) =>
            json.validate[AttachedData] match {
              case JsSuccess(attachedData, _) =>
                dbStats.withConnection({ implicit c =>
                  Calls.addAttachedDataByCallId(id, attachedData) match {
                    case SuccessAttachedDataReponse(message) => Created(message)
                    case FailedAttachedDataReponse(message) =>
                      BadRequest(message)
                  }
                })
              case JsError(e) => BadRequest(s"Error decoding JSON: $e")
            }
          case None => BadRequest("No JSON could be decoded")
        }
      }
    )

  def getAllAttachedData: EssentialAction = {
    secured.WithAuthenticatedUser(
      None,
      implicit user => { implicit request =>
        {
          dbStats.withConnection(implicit connection => {
            val attachedData = AttachedDataKey.getAllKeys()
            Ok(Json.toJson(attachedData))
          })
        }
      }
    )
  }

  def WithExceptionCatching(f: => Result): Result =
    try {
      f
    } catch {
      case e: InvalidParameterException => BadRequest(Json.toJson(e.getMessage))
      case e: NoSuchElementException    => NotFound(Json.toJson(e.getMessage))
      case e: Exception                 => InternalServerError(Json.toJson(e.getMessage))
    }

  def WithAuthenticatedRights(
      user: CredentialsValidation,
      f: Option[Right] => Result
  ): Result = {
    user match {
      case AuthenticatedUser(_, superAdmin) if superAdmin =>
        f(Some(AdminRight()))
      case AuthenticatedUser(username, _) =>
        f(rightsHelper.getUserRights(username))
      case AuthenticatedToken => f(Some(AdminRight()))
      case InvalidCredentials => f(None)
    }
  }
}
