package models

import java.util.Date
import anorm.SqlParser._
import anorm._
import com.google.inject.Inject
import org.joda.time.format.{
  DateTimeFormat,
  PeriodFormatter,
  PeriodFormatterBuilder
}
import org.joda.time.{DateTime, Duration}
import play.api.db.{Database, NamedDatabase}
import play.api.libs.json.{Json, Writes}
import xc.FieldValueTyper

case class CallDetail(
    start: DateTime,
    duration: Option[Duration],
    srcNum: Option[String],
    dstNum: Option[String],
    status: CallStatus.CallStatus,
    srcFirstName: Option[String] = None,
    srcLastName: Option[String] = None,
    dstFirstName: Option[String] = None,
    dstLastName: Option[String] = None
)

object CallDetail {
  private val formatCallDetail =
    DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
  val periodFormat: PeriodFormatter = new PeriodFormatterBuilder()
    .minimumPrintedDigits(2)
    .printZeroAlways()
    .appendHours()
    .appendSeparator(":")
    .appendMinutes()
    .appendSeparator(":")
    .appendSeconds()
    .toFormatter

  implicit val writes: Writes[CallDetail] = (call: CallDetail) =>
    Json.obj(
      "start"         -> call.start.toString(formatCallDetail),
      "duration"      -> call.duration.map(d => periodFormat.print(d.toPeriod)),
      "src_num"       -> call.srcNum,
      "dst_num"       -> call.dstNum,
      "status"        -> call.status.toString,
      "src_firstname" -> call.srcFirstName,
      "src_lastname"  -> call.srcLastName,
      "dst_firstname" -> call.dstFirstName,
      "dst_lastname"  -> call.dstLastName
    )
}

class CallDetailDao @Inject() (@NamedDatabase("stats") dbStats: Database) {

  implicit val typer: FieldValueTyper = new FieldValueTyper {
    def to(field: String, value: String): ParameterValue =
      field match {
        case _ => ToParameterValue[String].apply(value)
      }
  }

  val simple: RowParser[CallDetail] = get[Date]("start_time") ~
    get[Option[Date]]("end_time") ~
    get[Option[String]]("src_num") ~
    get[Option[String]]("dst_num") ~
    get[String]("status") ~
    get[Option[String]]("src_firstname") ~
    get[Option[String]]("src_lastname") ~
    get[Option[String]]("dst_firstname") ~
    get[Option[String]]("dst_lastname") map {
      case start ~ end ~ src ~ dst ~ status ~ src_firstname ~ src_lastname ~ dst_firstname ~ dst_lastname =>
        CallDetail(
          new DateTime(start),
          end.map(d => new Duration(new DateTime(start), new DateTime(d))),
          src,
          dst,
          CallStatus.withName(status),
          src_firstname,
          src_lastname,
          dst_firstname,
          dst_lastname
        )
    }

  def all_interface_names(interface: String): List[String] = {
    val names =
      if (interface.startsWith("SIP/") || interface.startsWith("PJSIP/")) {
        val name = interface.split("/", 2).last
        List(
          s"SIP/${name}",
          s"SIP/${name}_w",
          s"PJSIP/${name}",
          s"PJSIP/${name}_w"
        )
      } else {
        List(interface, s"${interface}_w")
      }
    names
  }

  def historyByInterface(interface: String, size: Int): List[CallDetail] = {
    val interface_names = all_interface_names(interface)
    dbStats.withConnection { implicit c =>
      SQL(
        """SELECT DISTINCT ON (call_data_id) t.call_data_id, t.start_time, t.end_time,
                  t.src_num, u_src.firstname as src_firstname, u_src.lastname as src_lastname,
                  t.dst_num, u_dst.firstname as dst_firstname, case when u_dst.id is not null then u_dst.lastname else q_dst.displayname end as dst_lastname,
                  t.status
           FROM (
                  (
                      SELECT cd.id AS call_data_id, cd.start_time, cd.end_time, cd.src_num, cd.dst_num, 'emitted' AS status
                      FROM call_data cd
                      WHERE src_interface IN ({interface})
                      ORDER BY call_data_id DESC LIMIT {limit}
                  )
                  UNION
                  (
                      SELECT cd.id AS call_data_id, ce.start_time, ce.end_time, src_num, dst_num,
                             CASE WHEN ce.answer_time IS NULL AND ce.end_time IS NULL THEN 'ongoing'
                                  WHEN ce.answer_time IS NULL AND ce.end_time IS NOT NULL THEN 'missed'
                                  ELSE 'answered' END
                      FROM call_data cd
                      JOIN call_element ce ON cd.id = ce.call_data_id
                      WHERE ce.interface IN ({interface})
                      ORDER BY call_data_id DESC LIMIT {limit}
                   )
           ) t
           LEFT JOIN extensions e_src on e_src.type='user' and e_src.exten=t.src_num
           LEFT JOIN userfeatures u_src on u_src.id=e_src.typeval::int
           LEFT JOIN extensions e_dst on e_dst.type in ('user', 'queue') and  e_dst.exten=t.dst_num
           LEFT JOIN userfeatures u_dst on e_dst.type='user' and u_dst.id=e_dst.typeval::int
           LEFT JOIN queuefeatures q_dst on e_dst.type='queue' and q_dst.id=e_dst.typeval::int
           ORDER BY call_data_id DESC, start_time DESC, end_time DESC LIMIT {limit}"""
      )
        .on(
          Symbol("limit")     -> size,
          Symbol("interface") -> interface_names
        )
        .as(simple.*)
    }
  }

  def historyByDate(interface: String, date: String): List[CallDetail] = {
    val interface_names = all_interface_names(interface)
    dbStats.withConnection { implicit c =>
      SQL(s"""SELECT DISTINCT ON (call_data_id) t.call_data_id, t.start_time, t.end_time,
                  t.src_num, u_src.firstname as src_firstname, u_src.lastname as src_lastname,
                  t.dst_num, u_dst.firstname as dst_firstname, case when u_dst.id is not null then u_dst.lastname else q_dst.displayname end as dst_lastname,
                  t.status
           FROM (
                  (
                      SELECT cd.id AS call_data_id, cd.start_time, cd.end_time, cd.src_num, cd.dst_num, 'emitted' AS status
                      FROM call_data cd
                      WHERE src_interface IN ({interface})
                      ORDER BY call_data_id DESC
                  )
                  UNION
                  (
                      SELECT cd.id AS call_data_id, ce.start_time, ce.end_time, src_num, dst_num,
                             CASE WHEN ce.answer_time IS NULL AND ce.end_time IS NULL THEN 'ongoing'
                                  WHEN ce.answer_time IS NULL AND ce.end_time IS NOT NULL THEN 'missed'
                                  ELSE 'answered' END
                      FROM call_data cd
                      JOIN call_element ce ON cd.id = ce.call_data_id
                      WHERE ce.interface IN ({interface})
                      ORDER BY call_data_id DESC
                   )
           ) t
           LEFT JOIN extensions e_src on e_src.type='user' and e_src.exten=t.src_num
           LEFT JOIN userfeatures u_src on u_src.id=e_src.typeval::int
           LEFT JOIN extensions e_dst on e_dst.type in ('user', 'queue') and  e_dst.exten=t.dst_num
           LEFT JOIN userfeatures u_dst on e_dst.type='user' and u_dst.id=e_dst.typeval::int
           LEFT JOIN queuefeatures q_dst on e_dst.type='queue' and q_dst.id=e_dst.typeval::int
           WHERE t.start_time > '$date'
           ORDER BY call_data_id DESC, start_time DESC, end_time DESC""")
        .on(Symbol("interface") -> interface_names)
        .as(simple.*)
    }
  }

  def historyByAgentNum(
      agentNum: String,
      size: Int,
      date: String
  ): List[CallDetail] =
    dbStats.withConnection { implicit c =>
      SQL("""WITH calls AS (
        SELECT cd.id AS call_data_id, start_time, end_time, src_num, COALESCE(cd.dst_num, cq.agent_num) AS dst_num,
            COALESCE(cq.agent_num, src_agent, dst_agent) AS agent_num, queue_ref,
            u_src.firstname AS src_firstname, u_src.lastname AS src_lastname,
            u_dst.firstname AS dst_firstname, u_dst.lastname AS dst_lastname,
            CASE
               WHEN cd.src_agent = {agentNum} AND cd.status::text = 'answer' AND cd.call_direction = 'outgoing' THEN 'emitted'
               WHEN cd.src_agent = {agentNum} AND cd.status IS NULL AND cd.call_direction = 'outgoing' THEN 'emitted'
               WHEN cd.src_agent = {agentNum} AND cd.status::text = 'answer' THEN 'emitted'
               WHEN cd.src_agent = {agentNum} AND cd.status::text IS NULL AND cd.call_direction = 'internal' THEN 'emitted'
               WHEN cd.status::text = 'answer' THEN 'answered'
               ELSE COALESCE(cq.status::text, 'abandoned')
            END AS status,
            ad.value as cuid

        FROM
          (SELECT
              id,
              uniqueid,
              start_time,
              end_time,
              src_num,
              dst_num,
              src_agent,
              dst_agent,
              call_direction,
              status::text
          FROM call_data
          WHERE start_time > {date}::timestamp
          ) AS cd

          LEFT JOIN
          (SELECT
              id,
              callid,
              queue_time,
              queue_ref,
              agent_num,
              status::text
          FROM call_on_queue
          WHERE queue_time > {date}::timestamp
          ) AS cq
          ON cq.callid = cd.uniqueid

        LEFT JOIN extensions e_src on e_src.type='user' and e_src.exten=cd.src_num
        LEFT JOIN extensions e_dst on e_dst.type in ('user', 'queue') and e_dst.exten=COALESCE(cd.dst_num, cq.agent_num)
        LEFT JOIN userfeatures u_src on u_src.id=e_src.typeval::int
        LEFT JOIN userfeatures u_dst on e_dst.type='user' and u_dst.id=e_dst.typeval::int
        LEFT JOIN attached_data ad ON ad.key = 'XIVO_CUID' and ad.id_call_data = cd.id

        WHERE (cq.agent_num = {agentNum} OR cd.src_agent = {agentNum} OR cd.dst_agent = {agentNum})

        )
        SELECT
            call_data_id, start_time, end_time, src_num, dst_num, agent_num, queue_ref,
            src_firstname, src_lastname, dst_firstname, dst_lastname, status
            FROM (
                SELECT * FROM calls WHERE calls.cuid IS NULL
                UNION
                SELECT DISTINCT ON (cuid) * FROM calls WHERE calls.cuid IS NOT NULL ORDER BY cuid
            ) as ac
            ORDER BY start_time DESC
            LIMIT {limit}""")
        .on(
          Symbol("limit")    -> size,
          Symbol("agentNum") -> agentNum,
          Symbol("date")     -> date
        )
        .as(simple.*)
    }
}
