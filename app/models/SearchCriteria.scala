package models

import java.lang.reflect.Field
import java.security.InvalidParameterException
import java.sql.Connection
import java.util.Date

import anorm.{NamedParameter, ParameterValue, ToParameterValue}
import models.CallDirection.CallDirection
import models.QueueCallStatus.QueueCallStatus
import org.joda.time.DateTime
import org.joda.time.format.{DateTimeFormat, DateTimeFormatter}
import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Reads}

case class QueryField(parameterName: String, filter: String)

abstract class SearchCriteria {

  val queryFields: Map[Field, QueryField]

  implicit def anyToParameterValue(obj: Any): ParameterValue =
    obj match {
      case Some(o)     => anyToParameterValue(o)
      case d: DateTime => ToParameterValue[Date].apply(d.toDate)
      case i: Int      => ToParameterValue[Int].apply(i)
      case t           => ToParameterValue[String].apply(t.toString)
    }

  def getCriteriaField(fieldName: String): Field = {
    val field = getClass.getDeclaredField(fieldName)
    field.setAccessible(true)
    field
  }

  def buildWhereClause()(implicit c: Connection): String

  def buildParams(): List[NamedParameter] =
    queryFields
      .filter(t => t._1.get(this) != None)
      .view
      .map(t => NamedParameter(t._2.parameterName, t._1.get(this)))
      .to(List)

  def validate(): Unit
}

case class GenericSearchCriteria(
    callee: Option[String] = None,
    caller: Option[String] = None,
    agent: Option[String] = None,
    queue: Option[String] = None,
    start: Option[DateTime] = None,
    end: Option[DateTime] = None,
    direction: CallDirection = CallDirection.All,
    queueCallStatus: Option[QueueCallStatus] = None,
    key: Option[String] = None,
    value: Option[String] = None
) extends SearchCriteria {

  override val queryFields: Map[Field, QueryField] = Map(
    getCriteriaField("callee") -> QueryField(
      "callee",
      "cd.dst_num like {callee}"
    ),
    getCriteriaField("caller") -> QueryField(
      "caller",
      "cd.src_num like {caller}"
    ),
    getCriteriaField("start") -> QueryField(
      "start",
      "({start} <= cd.start_time OR {start} <= cd.end_time)"
    ),
    getCriteriaField("end") -> QueryField("end", "{end} >= cd.start_time"),
    getCriteriaField("queueCallStatus") -> QueryField(
      "queueCallStatus",
      "(cq.status = {queueCallStatus}::call_exit_type OR cd.status = 'answer')"
    ),
    getCriteriaField("key")   -> QueryField("key", "ad.key like {key}"),
    getCriteriaField("value") -> QueryField("value", "ad.value like {value}")
  )

  override def buildWhereClause()(implicit c: Connection): String = {
    var res = "cd.end_time IS NOT NULL"
    for ((objectField, queryField) <- queryFields) {
      if (objectField.get(this) != None)
        res += s" AND ${queryField.filter}"
    }
    if (direction == CallDirection.Incoming)
      res += " AND call_direction = 'incoming'::call_direction_type"
    else if (direction == CallDirection.Outgoing)
      res += " AND call_direction = 'outgoing'::call_direction_type"
    if (queue.isDefined) {
      val queueNames = queue.get :: XivoObjects.queueNamesForPattern(queue.get)
      res += s" AND ${SqlUtils.createInClauseOrTrue("cq.queue_ref", queueNames)}"
    }
    if (agent.isDefined) {
      val agentNumbers =
        agent.get :: XivoObjects.agentsNumbersForPattern(agent.get)
      res +=
        s""" AND (${SqlUtils.createInClauseOrTrue("cq.agent_num", agentNumbers)}
           OR ${SqlUtils.createInClauseOrTrue("cd.src_agent", agentNumbers)}
           OR ${SqlUtils.createInClauseOrTrue("cd.dst_agent", agentNumbers)})"""
    }
    res
  }
  override def validate(): Unit = {
    if (value.isDefined && key.isEmpty)
      throw new InvalidParameterException(
        "key is mandatory when searching a value"
      )
  }
}

case class SimpleSearchCriteria(
    number: Option[String] = None,
    start: Option[DateTime] = None,
    end: Option[DateTime] = None,
    key: Option[String] = None,
    value: Option[String] = None,
    queueCallStatus: Option[QueueCallStatus] = None
) extends SearchCriteria {

  override val queryFields: Map[Field, QueryField] = Map(
    getCriteriaField("number") -> QueryField(
      "number",
      "(cd.src_num like {number} OR cd.dst_num like {number})"
    ),
    getCriteriaField("start") -> QueryField(
      "start",
      "({start} <= cd.start_time OR {start} <= cd.end_time)"
    ),
    getCriteriaField("queueCallStatus") -> QueryField(
      "queueCallStatus",
      "(cq.status = {queueCallStatus}::call_exit_type OR cd.status = 'answer')"
    ),
    getCriteriaField("end")   -> QueryField("end", "{end} >= cd.start_time"),
    getCriteriaField("key")   -> QueryField("key", "ad.key like {key}"),
    getCriteriaField("value") -> QueryField("value", "ad.value like {value}")
  )

  override def buildWhereClause()(implicit c: Connection): String = {
    var res = "cd.end_time IS NOT NULL"
    for ((objectField, queryField) <- queryFields) {
      if (objectField.get(this) != None)
        res += s" AND ${queryField.filter}"
    }
    res
  }

  override def validate(): Unit = ()
}

case class CallidSearchCriteria(callid: String) extends SearchCriteria {
  override val queryFields: Map[Field, QueryField] = Map(
    getCriteriaField("callid") -> QueryField("callid", "cd.uniqueid = {callid}")
  )

  override def buildWhereClause()(implicit c: Connection): String =
    "cd.uniqueid = {callid}"

  override def validate(): Unit = ()
}

object SearchCriteria {
  val formatter: DateTimeFormatter =
    DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")

  implicit val reads: Reads[SearchCriteria] = (
    (JsPath \ "callee").readNullable[String] and
      (JsPath \ "caller").readNullable[String] and
      (JsPath \ "agent").readNullable[String] and
      (JsPath \ "queue").readNullable[String] and
      (JsPath \ "start")
        .readNullable[String]
        .map(_.map(formatter.parseDateTime)) and
      (JsPath \ "end")
        .readNullable[String]
        .map(_.map(formatter.parseDateTime)) and
      (JsPath \ "direction")
        .readNullable[String]
        .map(_.map(CallDirection.withName).getOrElse(CallDirection.All)) and
      (JsPath \ "queueCallStatus")
        .readNullable[String]
        .map(_.map(QueueCallStatus.withName)) and
      (JsPath \ "key").readNullable[String] and
      (JsPath \ "value").readNullable[String]
  )(GenericSearchCriteria.apply _)
}

object SimpleSearchCriteria {
  val formatter: DateTimeFormatter =
    DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")

  implicit val reads: Reads[SimpleSearchCriteria] = (
    (JsPath \ "number").readNullable[String] and
      (JsPath \ "start")
        .readNullable[String]
        .map(_.map(formatter.parseDateTime)) and
      (JsPath \ "end")
        .readNullable[String]
        .map(_.map(formatter.parseDateTime)) and
      (JsPath \ "key").readNullable[String] and
      (JsPath \ "value").readNullable[String] and
      (JsPath \ "queueCallStatus")
        .readNullable[String]
        .map(_.map(QueueCallStatus.withName))
  )(SimpleSearchCriteria.apply _)
}

object CallDirection extends Enumeration {
  type CallDirection = Value
  val Incoming: Value = Value("incoming")
  val Outgoing: Value = Value("outgoing")
  val Internal: Value = Value("internal")
  val All: Value      = Value("all")
}
