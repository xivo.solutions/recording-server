package models

import ch.qos.logback.classic.LoggerContext
import org.slf4j.{Logger, LoggerFactory}

class CsvLoggerHelper {

  private def getCsvLogger: Logger = LoggerFactory.getLogger("CSVLogger")

  def filePath: String = {
    val lc: LoggerContext =
      LoggerFactory.getILoggerFactory.asInstanceOf[LoggerContext]
    lc.getProperty("ACCESS_LOG_PATH")
  }

  def logCsv(message: String): Unit = getCsvLogger.info(message)
}
