package models

import java.sql.Connection
import anorm._
import anorm.SqlParser._

object XivoObjects {
  def incallExtenById(id: Int)(implicit c: Connection): Option[String] =
    SQL(
      "SELECT exten FROM extensions WHERE type = 'incall' AND typeval = {incallId}"
    ).on(Symbol("incallId") -> id.toString)
      .as((get[String]("exten")).*)
      .headOption

  def agentNumbersForGroups(
      ids: List[Int]
  )(implicit c: Connection): List[String] =
    SQL(s"SELECT number FROM agentfeatures WHERE ${SqlUtils
      .createInClauseOrFalse("numgroup::varchar", ids.map(_.toString))}")
      .as(get[String]("number").*)

  def queueNameById(id: Int)(implicit c: Connection): Option[String] =
    SQL("SELECT name FROM queuefeatures WHERE id = {id}")
      .on(Symbol("id") -> id)
      .as(get[String]("name").*)
      .headOption

  def agentsNumbersForPattern(p: String)(implicit c: Connection): List[String] =
    SQL("""SELECT number FROM agentfeatures WHERE number LIKE '%' || {p} || '%' OR lower(firstname) LIKE '%' || lower({p}) || '%'
        OR lower(lastname) LIKE '%' || lower({p}) || '%'""")
      .on(Symbol("p") -> p)
      .as(get[String]("number").*)

  def queueNamesForPattern(p: String)(implicit c: Connection): List[String] =
    SQL(
      "SELECT name FROM queuefeatures WHERE number LIKE '%' || {p} || '%' OR lower(name) LIKE '%' || lower({p}) || '%'"
    )
      .on(Symbol("p") -> p)
      .as(get[String]("name").*)

}
