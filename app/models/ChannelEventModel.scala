package models

import anorm.{Column, Macro, MetaDataItem, RowParser, SQL, TypeDoesNotMatch}
import com.google.inject.Inject

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import play.api.db.{Database, NamedDatabase}
import play.api.libs.json.{JsObject, Json, Writes}

import scala.util.Try

case class ChannelEventLog(
    id: Int,
    eventtype: String,
    eventtime: LocalDateTime,
    userdeftype: String,
    cid_name: String,
    cid_num: String,
    cid_ani: String,
    cid_rdnis: String,
    cid_dnid: String,
    exten: String,
    context: String,
    channame: String,
    appname: String,
    appdata: String,
    amaflags: Int,
    accountcode: String,
    peeraccount: String,
    uniqueid: String,
    linkedid: String,
    userfield: String,
    peer: String,
    call_log_id: Option[Int],
    extra: Option[String]
)

object ChannelEventLog {
  implicit val writes: Writes[ChannelEventLog] = new Writes[ChannelEventLog] {
    val formatter: DateTimeFormatter =
      DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSSSSS")
    def writes(cel: ChannelEventLog): JsObject = {
      val call_log_id = cel.call_log_id.map(_.toString).getOrElse("")
      val extra       = cel.extra.getOrElse("")
      Json.obj(
        "id"          -> cel.id.toString,
        "eventtype"   -> cel.eventtype,
        "eventtime"   -> cel.eventtime.format(formatter),
        "userdeftype" -> cel.userdeftype,
        "cid_name"    -> cel.cid_name,
        "cid_num"     -> cel.cid_num,
        "cid_ani"     -> cel.cid_ani,
        "cid_rdnis"   -> cel.cid_rdnis,
        "cid_dnid"    -> cel.cid_dnid,
        "exten"       -> cel.exten,
        "context"     -> cel.context,
        "channame"    -> cel.channame,
        "appname"     -> cel.appname,
        "appdata"     -> cel.appdata,
        "amaflags"    -> cel.amaflags.toString,
        "accountcode" -> cel.accountcode,
        "peeraccount" -> cel.peeraccount,
        "uniqueid"    -> cel.uniqueid,
        "linkedid"    -> cel.linkedid,
        "userfield"   -> cel.userfield,
        "peer"        -> cel.peer,
        "call_log_id" -> call_log_id,
        "extra"       -> extra
      )
    }
  }
}

class ChannelEventDao @Inject() (@NamedDatabase("stats") dbStats: Database) {

  implicit val timestampToJodaLocalDateTime: Column[LocalDateTime] =
    Column.nonNull { (value, meta) =>
      val MetaDataItem(qualified, _, _) = meta

      value match {
        case date: java.sql.Timestamp => Right(date.toLocalDateTime)
        case _ =>
          Left(
            TypeDoesNotMatch(
              s"Cannot convert $value to Joda LocalDateTime for column $qualified"
            )
          )
      }
    }

  val parser: RowParser[ChannelEventLog] = Macro.namedParser[ChannelEventLog]

  def list(from: Int, limit: Int): Try[List[ChannelEventLog]] =
    dbStats.withConnection { implicit c =>
      Try {
        SQL("""SELECT id, eventtype, eventtime, userdeftype, cid_name, cid_num, cid_ani, cid_rdnis, cid_dnid, cid_dnid, exten, context, channame, appname, appdata, amaflags, accountcode, peeraccount, uniqueid, linkedid, userfield, peer, call_log_id, extra
           FROM cel
           WHERE id > {from}
           ORDER BY id
           LIMIT {limit}""")
          .on(Symbol("from") -> from, Symbol("limit") -> limit)
          .executeQuery()
          .as(parser.*)
      }
    }
}
