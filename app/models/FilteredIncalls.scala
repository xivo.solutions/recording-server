package models

import java.sql.Connection
import anorm.{RowParser, SQL}
import anorm.SqlParser._

object FilteredIncalls {

  val simpleString: RowParser[String] = get[String]("number")

  def delete(number: String)(implicit c: Connection): Int =
    SQL(
      "DELETE FROM filtered_numbers WHERE number = {number} AND type = 'incall'::number_type"
    )
      .on(Symbol("number") -> number)
      .executeUpdate()

  def all()(implicit c: Connection): List[String] =
    SQL(
      "SELECT number FROM filtered_numbers WHERE type = 'incall'::number_type"
    )
      .as(simpleString.*)
      .sorted

  def create(number: String)(implicit c: Connection): Int =
    SQL(
      "SELECT number FROM filtered_numbers WHERE number = {number} AND type = 'incall'::number_type"
    )
      .on(Symbol("number") -> number) as scalar[String].singleOpt match {
      case None =>
        SQL(
          "INSERT INTO filtered_numbers(number, type) VALUES ({number}, 'incall'::number_type)"
        ).on(Symbol("number") -> number).executeUpdate()
      case Some(_) => throw new NumberAlreadyExistsException(number)
    }

  def find(number: String)(implicit c: Connection): Option[String] =
    SQL(
      "SELECT number FROM filtered_numbers WHERE number = {number} AND type = 'incall'::number_type"
    ).on(Symbol("number") -> number).as(simpleString.*).headOption

}
