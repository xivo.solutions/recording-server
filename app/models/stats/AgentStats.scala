package models.stats

import java.sql.Connection
import anorm.SqlParser._
import anorm._
import play.api.libs.json.{Json, OWrites}

object AgentStats {

  case class AgentNumber(agentNumber: String)
  implicit val agentNumberWriter: OWrites[AgentNumber] =
    Json.writes[AgentNumber]

  def searchLastAgentForCaller(callerNo: String, since: Int)(implicit
      connection: Connection
  ): Option[AgentNumber] = {

    val simple =
      get[String]("agent_num") map (agentNumber => AgentNumber(agentNumber))

    def getLastAgentForCaller(caller: String, since: Int)(implicit
        connection: Connection
    ): Option[AgentNumber] = {
      SQL(
        s"""SELECT agent_num FROM
             call_on_queue WHERE callid=(SELECT t.uniqueid FROM
               (SELECT uniqueid, src_num, status FROM call_data WHERE src_num={caller} AND status='answer' AND
                start_time > current_timestamp - interval '${since} days' ORDER BY start_time DESC LIMIT 1 ) AS t)"""
      )
        .on(Symbol("caller") -> caller)
        .as(simple.*)
        .headOption
    }

    getLastAgentForCaller(callerNo, since)
  }
}
