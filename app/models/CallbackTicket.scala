package models

import java.security.InvalidParameterException
import java.util.{Date, UUID}
import anorm.SqlParser._
import anorm._

import javax.inject.Inject
import models.SqlUtils.uuidToStatement
import org.joda.time.format.{DateTimeFormat, DateTimeFormatter}
import org.joda.time.{DateTime, LocalDate, LocalTime}
import play.api.db.{Database, NamedDatabase}
import play.api.libs.functional.syntax._
import play.api.libs.json._

object CallbackStatus extends Enumeration {
  type CallbackStatus = Value
  val Fax: models.CallbackStatus.Value       = Value("fax")
  val NoAnswer: models.CallbackStatus.Value  = Value("noanswer")
  val Answered: models.CallbackStatus.Value  = Value("answered")
  val Callback: models.CallbackStatus.Value  = Value("callback")
  val Voicemail: models.CallbackStatus.Value = Value("voicemail")
  val Email: models.CallbackStatus.Value     = Value("email")
}

case class CallbackTicket(
    uuid: Option[UUID],
    listUuid: UUID,
    requestUuid: UUID,
    queueRef: String,
    agentNum: String,
    dueDate: LocalDate,
    started: DateTime,
    lastUpdate: Option[DateTime],
    callid: Option[String] = None,
    status: Option[CallbackStatus.CallbackStatus] = None,
    comment: Option[String] = None,
    phoneNumber: Option[String] = None,
    mobilePhoneNumber: Option[String] = None,
    firstName: Option[String] = None,
    lastName: Option[String] = None,
    company: Option[String] = None,
    description: Option[String] = None,
    periodStart: Option[LocalTime] = None,
    periodEnd: Option[LocalTime] = None
)

object CallbackTicket {
  val formatter: DateTimeFormatter =
    DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")

  implicit val reads: Reads[CallbackTicket] = (
    (JsPath \ "uuid").readNullable[String].map(_.map(UUID.fromString)) and
      (JsPath \ "listUuid").read[String].map(UUID.fromString) and
      (JsPath \ "requestUuid").read[String].map(UUID.fromString) and
      (JsPath \ "queueRef").read[String] and
      (JsPath \ "agentNum").read[String] and
      (JsPath \ "dueDate").read[String].map(LocalDate.parse) and
      (JsPath \ "started").read[String].map(formatter.parseDateTime) and
      (JsPath \ "lastUpdate")
        .readNullable[String]
        .map(_.map(formatter.parseDateTime)) and
      (JsPath \ "callid").readNullable[String] and
      (JsPath \ "status")
        .readNullable[String]
        .map(_.map(CallbackStatus.withName)) and
      (JsPath \ "comment").readNullable[String] and
      (JsPath \ "phoneNumber").readNullable[String] and
      (JsPath \ "mobilePhoneNumber").readNullable[String] and
      (JsPath \ "firstName").readNullable[String] and
      (JsPath \ "lastName").readNullable[String] and
      (JsPath \ "company").readNullable[String] and
      (JsPath \ "description").readNullable[String] and
      (JsPath \ "periodStart")
        .readNullable[String]
        .map(_.map(LocalTime.parse)) and
      (JsPath \ "periodEnd").readNullable[String].map(_.map(LocalTime.parse))
  )(CallbackTicket.apply _)

  implicit val writes: Writes[CallbackTicket] = (ticket: CallbackTicket) =>
    Json.obj(
      "uuid"              -> ticket.uuid,
      "listUuid"          -> ticket.listUuid,
      "requestUuid"       -> ticket.requestUuid,
      "queueRef"          -> ticket.queueRef,
      "agentNum"          -> ticket.agentNum,
      "dueDate"           -> ticket.dueDate.toString("yyyy-MM-dd"),
      "started"           -> ticket.started.toString(formatter),
      "lastUpdate"        -> ticket.lastUpdate.map(_.toString(formatter)),
      "callid"            -> ticket.callid,
      "status"            -> ticket.status.map(_.toString),
      "comment"           -> ticket.comment,
      "phoneNumber"       -> ticket.phoneNumber,
      "mobilePhoneNumber" -> ticket.mobilePhoneNumber,
      "firstName"         -> ticket.firstName,
      "lastName"          -> ticket.lastName,
      "company"           -> ticket.company,
      "description"       -> ticket.description,
      "periodStart"       -> ticket.periodStart.map(_.toString("HH:mm:ss")),
      "periodEnd"         -> ticket.periodEnd.map(_.toString("HH:mm:ss"))
    )

  def toCsv(tickets: List[CallbackTicket]): String = {
    val header = List(
      "uuid",
      "listUuid",
      "requestUuid",
      "queueRef",
      "agentNum",
      "dueDate",
      "started",
      "lastUpdate",
      "callid",
      "status",
      "comment",
      "phoneNumber",
      "mobilePhoneNumber",
      "firstName",
      "lastName",
      "company",
      "description",
      "periodStart",
      "periodEnd"
    ).mkString(",") + "\n"
    val lines = tickets.map(toList(_).map(quote).mkString(","))
    header + lines.mkString("\n")
  }

  private def quote(s: String): String =
    "\"%s\"".format(s.replace("\"", "\"\""))

  def toList(t: CallbackTicket): List[String] = {
    t.uuid.map(_.toString).getOrElse("") ::
      t.listUuid.toString ::
      t.requestUuid.toString ::
      t.queueRef ::
      t.agentNum ::
      t.dueDate.toString("yyyy-MM-dd") ::
      t.started.toString(formatter) ::
      t.lastUpdate.map(_.toString(formatter)).getOrElse("") ::
      t.callid.getOrElse("") ::
      t.status.map(_.toString).getOrElse("") ::
      t.comment.getOrElse("") ::
      t.phoneNumber.getOrElse("") ::
      t.mobilePhoneNumber.getOrElse("") ::
      t.firstName.getOrElse("") ::
      t.lastName.getOrElse("") ::
      t.company.getOrElse("") ::
      t.description.getOrElse("") ::
      t.periodStart.map(_.toString("HH:mm:ss")).getOrElse("") ::
      t.periodEnd.map(_.toString("HH:mm:ss")).getOrElse("") ::
      Nil
  }
}

class CallbackTicketDao @Inject() (@NamedDatabase("stats") dbStats: Database) {

  def create(ticket: CallbackTicket): CallbackTicket =
    dbStats.withConnection({ implicit c =>
      val uuid = SQL(
        """INSERT INTO callback_ticket(callback_list_uuid, callback_request_uuid, queue_ref, agent_num, due_date, started, last_update, callid, status,
       comment, phone_number, mobile_phone_number, firstname, lastname, company, description) VALUES ({listUuid}, {rqUuid},
      {queue}, {agent}, {dueDate}, {started}, {update}, {callid}, {status}::callback_status, {comment}, {phone}, {mobile}, {firstname}, {lastname},
      {company}, {description})"""
      ).on(
        Symbol("listUuid")    -> ticket.listUuid,
        Symbol("rqUuid")      -> ticket.requestUuid,
        Symbol("queue")       -> ticket.queueRef,
        Symbol("agent")       -> ticket.agentNum,
        Symbol("started")     -> ticket.started.toDate,
        Symbol("update")      -> ticket.lastUpdate.map(_.toDate),
        Symbol("callid")      -> ticket.callid,
        Symbol("status")      -> ticket.status.map(_.toString),
        Symbol("comment")     -> ticket.comment,
        Symbol("phone")       -> ticket.phoneNumber,
        Symbol("mobile")      -> ticket.mobilePhoneNumber,
        Symbol("firstname")   -> ticket.firstName,
        Symbol("lastname")    -> ticket.lastName,
        Symbol("company")     -> ticket.company,
        Symbol("description") -> ticket.description,
        Symbol("dueDate")     -> ticket.dueDate.toDate
      ).executeInsert(get[UUID]("uuid").*)
        .headOption
      ticket.copy(uuid = uuid)
    })

  def update(uuid: String, patch: CallbackTicketPatch): Int =
    dbStats.withConnection({ implicit c =>
      val realUuid = validateUuid(uuid)
      SQL(
        "SELECT status::varchar, comment, callid FROM callback_ticket WHERE uuid = {uuid}"
      ).on(Symbol("uuid") -> realUuid)
        .as(
          (get[Option[String]]("status").map(_.map(CallbackStatus.withName)) ~
            get[Option[String]]("comment") ~
            get[Option[String]]("callid")).*
        )
        .headOption match {
        case None =>
          throw new NoSuchElementException(
            s"No CallbackTicket found with uuid $uuid"
          )
        case Some(status ~ comment ~ callid) =>
          val realPatch = CallbackTicketPatch(
            patch.status.orElse(status),
            patch.comment.orElse(comment),
            patch.callid.orElse(callid)
          )
          SQL(
            "UPDATE callback_ticket SET comment = {comment}, status = {status}::callback_status, callid = {callid}, last_update = now() WHERE uuid = {uuid}"
          )
            .on(
              Symbol("status")  -> realPatch.status.map(_.toString),
              Symbol("comment") -> realPatch.comment,
              Symbol("uuid")    -> realUuid,
              Symbol("callid")  -> realPatch.callid
            )
            .executeUpdate()
      }
    })

  private def validateUuid(uuid: String): UUID =
    try {
      UUID.fromString(uuid)
    } catch {
      case _: Exception =>
        throw new InvalidParameterException(s"Invalid UUID : $uuid")
    }

  val simple: RowParser[CallbackTicket] = get[Option[UUID]]("uuid") ~
    get[UUID]("callback_list_uuid") ~
    get[UUID]("callback_request_uuid") ~
    get[String]("queue_ref") ~
    get[String]("agent_num") ~
    get[Date]("due_date") ~
    get[Date]("started") ~
    get[Option[Date]]("last_update") ~
    get[Option[String]]("callid") ~
    get[Option[String]]("status") ~
    get[Option[String]]("comment") ~
    get[Option[String]]("phone_number") ~
    get[Option[String]]("mobile_phone_number") ~
    get[Option[String]]("firstname") ~
    get[Option[String]]("lastname") ~
    get[Option[String]]("company") ~
    get[Option[String]]("description") ~
    get[Option[Date]]("period_start") ~
    get[Option[Date]]("period_end") map {
      case uuid ~ listUuid ~ cbUuid ~ queue ~ agent ~ dueDate ~ started ~ updated ~ callid ~ status ~ comment ~
          phone ~ mobile ~ firstname ~ lastname ~ company ~ description ~ periodStart ~ periodEnd =>
        CallbackTicket(
          uuid,
          listUuid,
          cbUuid,
          queue,
          agent,
          new LocalDate(dueDate),
          new DateTime(started),
          updated.map(new DateTime(_)),
          callid,
          status.map(CallbackStatus.withName),
          comment,
          phone,
          mobile,
          firstname,
          lastname,
          company,
          description,
          periodStart.map(new LocalTime(_)),
          periodEnd.map(new LocalTime(_))
        )
    }

  def byList(listUuid: String): List[CallbackTicket] =
    dbStats.withConnection({ implicit c =>
      val realUuid = validateUuid(listUuid)
      SQL("""SELECT t.uuid, t.callback_list_uuid, t.callback_request_uuid, t.queue_ref, t.agent_num, t.due_date, t.started, t.last_update, t.callid, t.status::varchar,
      t.comment, t.phone_number, t.mobile_phone_number, t.firstname, t.lastname, t.company, t.description, p.period_start, p.period_end
      FROM callback_ticket t LEFT JOIN callback_request r ON t.callback_request_uuid = r.uuid
      LEFT JOIN preferred_callback_period p ON r.preferred_callback_period_uuid = p.uuid
      WHERE callback_list_uuid = {lUuid}""")
        .on(Symbol("lUuid") -> realUuid)
        .as(simple.*)
    })

  def getTicket(uuid: String): CallbackTicket =
    dbStats.withConnection({ implicit c =>
      val realUuid = validateUuid(uuid)
      SQL(
        """SELECT t.uuid, t.callback_list_uuid, t.callback_request_uuid, t.queue_ref, t.agent_num, t.due_date, t.started, t.last_update, t.callid, t.status::varchar,
           t.comment, t.phone_number, t.mobile_phone_number, t.firstname, t.lastname, t.company, t.description, p.period_start, p.period_end
           FROM callback_ticket t LEFT JOIN callback_request r ON t.callback_request_uuid = r.uuid
           LEFT JOIN preferred_callback_period p ON r.preferred_callback_period_uuid = p.uuid WHERE t.uuid = {uuid}"""
      )
        .on(Symbol("uuid") -> realUuid)
        .as(simple.*)
        .headOption match {
        case None =>
          throw new NoSuchElementException(s"No ticket with uuid = $uuid found")
        case Some(ticket) => ticket
      }
    })
}
