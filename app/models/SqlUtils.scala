package models

import java.util.UUID

import anorm.ToStatement

object SqlUtils {

  def createInClauseOrTrue(column: String, values: List[String]): String =
    createInClauseOrOption(column, values, "TRUE")

  private def createInClauseOrOption(
      column: String,
      values: List[String],
      replacement: String
  ): String = {
    var clause = ""
    if (values.isEmpty)
      clause = replacement
    else {
      clause = s"$column IN ("
      values.foreach(cid => clause += s"'$cid',")
      clause = clause.substring(0, clause.length - 1)
      clause += ")"
    }
    clause
  }

  def createInClauseOrFalse(column: String, values: List[String]): String =
    createInClauseOrOption(column, values, "FALSE")

  implicit def uuidToStatement: ToStatement[UUID] =
    (s: java.sql.PreparedStatement, index: Int, aValue: UUID) =>
      s.setObject(index, aValue)
}
