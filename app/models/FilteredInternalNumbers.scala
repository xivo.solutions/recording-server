package models

import java.sql.Connection
import anorm._
import anorm.SqlParser._

object FilteredInternalNumbers {

  def simpleString: RowParser[String] = get[String]("number")

  def delete(number: String)(implicit c: Connection): Unit = {
    SQL(
      "DELETE FROM filtered_numbers WHERE number = {number} AND type = 'internal_number'::number_type"
    )
      .on(Symbol("number") -> number)
      .executeUpdate()
    ()
  }

  def create(number: String)(implicit c: Connection): Unit =
    SQL(
      "SELECT number FROM filtered_numbers WHERE number = {number} AND type = 'internal_number'::number_type"
    )
      .on(Symbol("number") -> number) as scalar[String].singleOpt match {
      case None =>
        SQL(
          "INSERT INTO filtered_numbers(number, type) VALUES ({number}, 'internal_number'::number_type)"
        ).on(Symbol("number") -> number).executeUpdate()
        ()
      case Some(_) =>
        throw new NumberAlreadyExistsException(number)
    }

  def find(number: String)(implicit c: Connection): Option[String] =
    SQL(
      "SELECT number FROM filtered_numbers WHERE number = {number} AND type = 'internal_number'::number_type"
    )
      .on(Symbol("number") -> number)
      .as(simpleString.*)
      .headOption

  def all()(implicit c: Connection): List[String] =
    SQL(
      "SELECT number FROM filtered_numbers WHERE type = 'internal_number'::number_type"
    )
      .as(simpleString.*)

}
