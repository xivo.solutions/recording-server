package models

import akka.NotUsed
import akka.stream.Materializer
import akka.stream.scaladsl.{Concat, Source}

import java.sql.{Connection, Timestamp}
import java.util.Date
import anorm.SqlParser._
import anorm._
import org.joda.time.{DateTime, Duration}
import play.api.Logger

import scala.concurrent.Future

case class Record(
    callDataId: String,
    start: DateTime,
    duration: Option[Duration],
    srcNum: String,
    dstNum: String,
    agent: List[String],
    queue: List[String],
    status: QueueCallStatus.QueueCallStatus = QueueCallStatus.Abandoned,
    attachedData: Map[String, String] = Map()
) {
  val recordId: Option[String] = attachedData.get("recording")
}

case class AgentDetail(firstName: String, lastName: String, number: String)
case class QueueDetail(name: String, number: String)
case class AttachedDataKey(id: Long, key: String, value: String)
object AttachedDataKey {
  def simple: RowParser[AttachedDataKey] =
    Macro.namedParser[AttachedDataKey]
  def getAllKeys()(implicit c: Connection): List[String] = {
    SQL(
      """
        | SELECT DISTINCT key FROM attached_data
      """.stripMargin
    ).as(SqlParser.str("key").*)
  }
}

object Record {
  val logger = Logger(getClass.getName)

  def simple(implicit c: Connection): RowParser[Record] =
    get[String]("call_data_uniqueid") ~
      get[Option[Date]]("start_time") ~
      get[Option[Date]]("end_time") ~
      get[Option[String]]("src_num") ~
      get[Option[String]]("dst_num") ~
      get[Option[String]]("callstatus") map {
        case callDataUniqueId ~ start ~ end ~ srcNum ~ dstNum ~ callstatus =>
          Record(
            callDataUniqueId,
            new DateTime(start.get),
            end.map(d =>
              new Duration(new DateTime(start.get), new DateTime(d))
            ),
            srcNum.getOrElse(""),
            dstNum.getOrElse(""),
            agent = getAgentData(callDataUniqueId).map(a =>
              s"${a.firstName} ${a.lastName} (${a.number})"
            ),
            queue = getQueueData(callDataUniqueId).map(q =>
              s"${q.name} (${q.number})"
            ),
            status = QueueCallStatus.withName(
              callstatus.getOrElse(QueueCallStatus.Abandoned.toString)
            ),
            attachedData = getAttachedData(callDataUniqueId)
          )
      }

  def simpleAttachedData: RowParser[(String, String)] =
    get[String]("key") ~ get[String]("value") ~ get[Boolean]("purged") map {
      case key ~ _ ~ purged if purged && key == AttachedData.RecordingKey =>
        key -> AttachedData.WasPurged
      case key ~ value ~ _ =>
        key -> value
    }

  def simpleAgentData: RowParser[Option[AgentDetail]] =
    get[Option[String]]("firstname") ~
      get[Option[String]]("lastname") ~
      get[Option[String]]("number") map {
        case firstName ~ lastName ~ Some(number) =>
          Some(
            AgentDetail(firstName.getOrElse(""), lastName.getOrElse(""), number)
          )
        case _ => None
      }

  def simpleQueueData: RowParser[Option[QueueDetail]] =
    get[Option[String]]("queue_name") ~
      get[Option[String]]("queue_number") map {
        case queueName ~ Some(queueNumber) =>
          Some(QueueDetail(queueName.getOrElse(""), queueNumber))
        case _ => None
      }

  private def getAttachedData(
      callDataUniqueId: String
  )(implicit c: Connection): Map[String, String] =
    SQL(s"""SELECT
         |           key, value, purged
         |        FROM
         |
         |          attached_data AS ad
         |
         |        LEFT JOIN call_data cd ON cd.id = ad.id_call_data
         |        WHERE cd.uniqueid = {callDataUniqueId}
         |
     """.stripMargin)
      .on(Symbol("callDataUniqueId") -> callDataUniqueId)
      .as(simpleAttachedData.*)
      .toMap

  private def getAgentData(
      callDataUniqueId: String
  )(implicit c: Connection): List[AgentDetail] = {
    SQL(s"""
         |  SELECT
         |        a.firstname AS firstname, a.lastname AS lastname, a.number AS number
         |  FROM
         |        call_data AS cd
         |  LEFT JOIN
         |        transfers t ON t.callidfrom = cd.uniqueid
         |  LEFT JOIN
         |        call_on_queue cq ON COALESCE(t.callidto, cd.uniqueid) = cq.callid
         |  LEFT JOIN
         |        agentfeatures a ON COALESCE(cq.agent_num, src_agent, dst_agent) = a.number
         |  WHERE
         |        cd.uniqueid = {callDataUniqueId}
         |  ORDER BY
         |        t.callidto ASC, cq.id ASC
         |
     """.stripMargin)
      .on(Symbol("callDataUniqueId") -> callDataUniqueId)
      .as(simpleAgentData.*)
      .distinct
      .flatten
  }

  private def getQueueData(
      callDataUniqueId: String
  )(implicit c: Connection): List[QueueDetail] = {
    SQL(s"""
         |  SELECT
         |        cq.queue_ref as queue_name, q.number as queue_number
         |  FROM
         |        call_data AS cd
         |  LEFT JOIN
         |        transfers t ON t.callidfrom = cd.uniqueid
         |  LEFT JOIN
         |        call_on_queue cq ON COALESCE(t.callidto, cd.uniqueid) = cq.callid
         |  LEFT JOIN
         |        queuefeatures q ON cq.queue_ref = q.name
         |  WHERE
         |        cd.uniqueid = {callDataUniqueId}
         |  ORDER BY
         |        t.callidto ASC, cq.id ASC
         |
     """.stripMargin)
      .on(Symbol("callDataUniqueId") -> callDataUniqueId)
      .as(simpleQueueData.*)
      .distinct
      .flatten
  }

  def getAttachedDataToPurgeWithoutKey(
      purgeKey: String,
      recordingKey: String,
      weeksToKeepRecording: Timestamp,
      fetchSize: Int
  )(implicit c: Connection, m: Materializer): Source[String, Future[Int]] = {
    AkkaStream.source[String](
      SQL(
        s"""SELECT DISTINCT id_call_data, value as filename FROM attached_data,
            (SELECT id_call_data as sb1_id_call_data FROM attached_data WHERE key <> {purgeKey} AND key = {recordingKey}
              EXCEPT
            SELECT id_call_data FROM attached_data WHERE key = {purgeKey}) regular_ids
          INNER JOIN call_data ON call_data.id = regular_ids.sb1_id_call_data
          WHERE
            call_data.end_time <= {refTime}
              AND
            attached_data.purged = FALSE
              AND
            attached_data.id_call_data = regular_ids.sb1_id_call_data
              AND
            attached_data.key = {recordingKey}"""
      )
        .on(
          Symbol("purgeKey")     -> purgeKey,
          Symbol("recordingKey") -> recordingKey,
          Symbol("refTime")      -> weeksToKeepRecording
        )
        .withFetchSize(Some(fetchSize)),
      get[String]("filename")
    )
  }

  def getAttachedDataToPurge(
      purgeKey: String,
      recordingKey: String,
      fetchSize: Int
  )(implicit c: Connection, m: Materializer): Source[String, Future[Int]] = {
    AkkaStream.source[String](
      SQL(
        s"""SELECT attached_data.value AS filename, expiration_ids.value AS value
            FROM attached_data,
              (SELECT id_call_data, value FROM attached_data
                WHERE key = {purgeKey} AND LENGTH(value) >= 10
                  AND
                TO_TIMESTAMP(value, 'YYYY-MM-DD HH24:MI:SS')::timestamp <= now()::timestamp
                  AND
                purged = FALSE)
              expiration_ids
            WHERE
              attached_data.id_call_data = expiration_ids.id_call_data AND attached_data.key = {recordingKey}"""
      )
        .on(
          Symbol("purgeKey")     -> purgeKey,
          Symbol("recordingKey") -> recordingKey
        )
        .withFetchSize(Some(fetchSize)),
      get[String]("filename")
    )
  }

  def getAttachedDataToPurgeAll(
      purgeKey: String,
      recordingKey: String,
      weeksToKeepRecording: Timestamp,
      fetchSize: Int = 1500
  )(implicit c: Connection, m: Materializer): Source[String, NotUsed] = {
    Source.combine(
      getAttachedDataToPurge(purgeKey, recordingKey, fetchSize),
      getAttachedDataToPurgeWithoutKey(
        purgeKey,
        recordingKey,
        weeksToKeepRecording,
        fetchSize
      )
    )(Concat(_))
  }

  def search(
      criteria: SearchCriteria,
      right: models.authentication.Right
  )(implicit connection: Connection, paginator: Paginator): List[Record] = {
    def retrieveCallDetails: String =
      s"""
         |  SELECT
         |        DISTINCT ON(start_time, cd.id) cd.id AS call_data_id, cd.uniqueid as call_data_uniqueid,
         |        cd.start_time, cd.end_time, cd.src_num, cd.dst_num, cq.queue_ref, cq.status::text AS callstatus
         |  FROM
         |        call_data cd
         |  LEFT JOIN
         |        attached_data ad ON ad.id_call_data = cd.id
         |  LEFT JOIN
         |        call_on_queue cq ON cq.callid = cd.uniqueid
         |  WHERE
         |        (${criteria.buildWhereClause()}) AND (${right
        .buildWhereClause()})
         |  ORDER BY
         |        start_time DESC, cd.id DESC
         |  OFFSET
         |        {offset}
         |  LIMIT
         |        {limit}
         |
     """.stripMargin

    SQL(retrieveCallDetails)
      .on(criteria.buildParams(): _*)
      .on(
        Symbol("offset") -> paginator.offset,
        Symbol("limit")  -> paginator.size
      )
      .as(Record.simple.*)
      .distinct
  }

  def transfer(
      records: List[Record]
  )(implicit connection: Connection): List[Record] = {
    def retrieveTransferCallDetails: String =
      s"""
         |  SELECT
         |        DISTINCT ON(start_time, cd.id) cd.id AS call_data_id, cd.uniqueid as call_data_uniqueid,
         |        cd.start_time, cd.end_time, cd.src_num, cd.dst_num, cq.queue_ref, cq.status::text AS callstatus
         |  FROM
         |        transfers t
         |  LEFT JOIN
         |        call_data cd ON cd.uniqueid = t.callidfrom
         |  LEFT JOIN
         |        attached_data ad ON ad.id_call_data = cd.id
         |  LEFT JOIN
         |        call_on_queue cq ON cq.callid = cd.uniqueid
         |  LEFT JOIN
         |        agentfeatures a ON COALESCE(cq.agent_num, cd.src_agent, cd.dst_agent) = a.number
         |  LEFT JOIN
         |        queuefeatures q ON cq.queue_ref = q.name
         |  WHERE
         |        t.callidto IN ({call_unique_ids})
         |  ORDER BY
         |        start_time DESC, cd.id DESC
         |
         """.stripMargin

    val call_unique_ids: Seq[String] = records.map(rec => rec.callDataId)
    SQL(retrieveTransferCallDetails)
      .on(Symbol("call_unique_ids") -> call_unique_ids)
      .as(Record.simple.*)
  }

  def searchByCallid(
      callid: String,
      right: models.authentication.Right
  )(implicit connection: Connection, paginator: Paginator): List[Record] =
    search(CallidSearchCriteria(callid), right)

  def globalIdExists(globalId: String)(implicit c: Connection): Boolean =
    SQL(
      "SELECT id FROM attached_data ad WHERE ad.key = \'recording\' AND ad.value = {globalId}"
    ).on(Symbol("globalId") -> globalId)
      .as(scalar[Long].singleOpt)
      .isDefined

  def callidByGlobalId(
      globalId: String
  )(implicit c: Connection): Option[String] =
    SQL(
      "SELECT cd.uniqueid FROM call_data cd JOIN attached_data ad ON cd.id = ad.id_call_data WHERE ad.key = 'recording' AND ad.value = {globalId}"
    )
      .on(Symbol("globalId") -> globalId)
      .as(get[String]("uniqueid").*)
      .headOption

  def recordsWithTransfers(
      records: List[Record],
      transfers: List[Record]
  ): List[Record] = {
    (records ::: transfers).distinct.sortBy(r => r.callDataId).reverse
  }

  def setAttachedDataPurgeFlag(
      values: Seq[String]
  )(implicit c: Connection): Int = {
    SQL(
      "UPDATE attached_data SET purged = TRUE WHERE id_call_data IN (SELECT id_call_data FROM attached_data WHERE value in ({values}))"
    )
      .on(Symbol("values") -> values)
      .executeUpdate()
  }
}

object QueueCallStatus extends Enumeration {
  type QueueCallStatus = Value
  val Abandoned: models.QueueCallStatus.Value      = Value("abandoned")
  val Answered: models.QueueCallStatus.Value       = Value("answered")
  val DivertCaRatio: models.QueueCallStatus.Value  = Value("divert_ca_ratio")
  val DivertWaitTime: models.QueueCallStatus.Value = Value("divert_waittime")
  val Closed: models.QueueCallStatus.Value         = Value("closed")
  val Full: models.QueueCallStatus.Value           = Value("full")
  val JoinEmpty: models.QueueCallStatus.Value      = Value("joinempty")
  val LeaveEmpty: models.QueueCallStatus.Value     = Value("leaveempty")
  val Timeout: models.QueueCallStatus.Value        = Value("timeout")

}
