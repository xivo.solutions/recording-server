package models.authentication

import javax.inject.Inject
import play.api.Configuration
import play.api.cache.SyncCacheApi
import play.api.libs.ws.WSClient

import scala.concurrent.Await
import scala.concurrent.duration._

class RightsHelper @Inject() (
    configuration: Configuration,
    ws: WSClient,
    cache: SyncCacheApi
) {

  private def WsAddress = configuration.get[String]("configManagement.address")
  def httpContext: Option[String] =
    configuration.getOptional[String]("configManagement.httpContext")

  def getUserRights(username: String): Option[Right] =
    cache.get[Right](username).orElse(getFreshUserRights(username))

  def getFreshUserRights(username: String): Option[Right] =
    getUserRightFromWebService(username).map(right => {
      cache.set(username, right)
      right
    })

  def getUrl(username: String): String = {
    val httpContextURLPart = httpContext.getOrElse("").lastOption match {
      case None      => ""
      case Some('/') => httpContext.get
      case _         => httpContext.get + "/"
    }
    s"http://$WsAddress/${httpContextURLPart}api/1.0/rights/user/$username"
  }

  private def getUserRightFromWebService(username: String): Option[Right] = {
    val res = Await.result(ws.url(getUrl(username)).get(), 5.second)
    if (res.status != 200)
      return None
    Some(Right.fromJson(res.json))
  }

}
