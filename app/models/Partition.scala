package models

import java.io.File

case class Partition(
    name: String,
    mountPoint: String,
    totalSpace: Long,
    freeSpace: Long
)

object Partition {
  def forDirectory(dir: String): Partition = {
    val file = new File(dir)
    Partition("", dir, file.getTotalSpace, file.getUsableSpace)
  }
}
