package models

import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Reads}

case class AttachedData(key: String, value: String, `type`: Option[String])

object AttachedData {
  type AttachedDataKey = String
  val PurgeKey: AttachedDataKey     = "xivo_recording_expiration"
  val RecordingKey: AttachedDataKey = "recording"
  val WasPurged: AttachedDataKey    = "was purged"

  implicit val reads: Reads[AttachedData] = (
    (JsPath \ "key").read[String] and
      (JsPath \ "value").read[String] and
      (JsPath \ "type").readNullable[String]
  )(AttachedData.apply _)
}
