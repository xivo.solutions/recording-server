package models

object CallStatus extends Enumeration {
  type CallStatus = Value

  val Emitted: models.CallStatus.Value        = Value("emitted")
  val Answered: models.CallStatus.Value       = Value("answered")
  val Missed: models.CallStatus.Value         = Value("missed")
  val Ongoing: models.CallStatus.Value        = Value("ongoing")
  val Abandoned: models.CallStatus.Value      = Value("abandoned")
  val Timeout: models.CallStatus.Value        = Value("timeout")
  val DivertCaRatio: models.CallStatus.Value  = Value("divert_ca_ratio")
  val DivertWaitTime: models.CallStatus.Value = Value("divert_waittime")
  val Closed: models.CallStatus.Value         = Value("closed")
  val Full: models.CallStatus.Value           = Value("full")
  val JoinEmpty: models.CallStatus.Value      = Value("joinempty")
  val LeaveEmpty: models.CallStatus.Value     = Value("leaveempty")
  val ExitWithKey: models.CallStatus.Value    = Value("exit_with_key")
}
