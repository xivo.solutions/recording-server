var PAGE_SIZE = 50;

angular.module('recording-server', ['Disk'])
.directive('emptyToNull', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, elem, attrs, ctrl) {
            ctrl.$parsers.push(function(viewValue) {
                if(viewValue === "") {
                    return null;
                }
                return viewValue;
            });
        }
    };
})
.directive("listenPlayEvent", function($http){
    return function(scope, element, attrs){
        element.bind("play", function(){
            $http.get('records/'+scope.record.id+'/audio/listen');
        });
    }
})
.controller('PageController', function($scope, diskSpace, $log, $http, $window) {
    const calendarLang = document.getElementsByTagName("body")[0].getAttribute("lang") == 'fr' ? 'fr': 'en';
    $scope.loading = false;
    $scope.checkAlert = function() {
        diskSpace.getAlertMessage().then(function(alertMsg) {
            if (alertMsg !== "") {
                alert(alertMsg);
            } else {
                 $log.info('Disk space checked OK');
            }
        });
    };
    $scope.checkAlert();

    $scope.request = {
        direction: 'all'
    };
    $scope.search_attached_data = false;

    $scope.getRecordResultPath = (record) => {
        if (record.id) return `records/${record.id}/audio/result`
        else return ""
    }

    $scope.processJson = function(json) {
        $scope.loading=false;
        $scope.records = json.data.records;
        $scope.hasNext = json.data.hasNext;
    }

    $('.start').datetimepicker({
      language: calendarLang,
      defaultDate: moment().subtract(1, 'd')
    });

    $('.end').datetimepicker({
        language: calendarLang
    });

    $('.info-tooltip').tooltip();


    $scope.find = (pageNum) => {
        $scope.currentPage = pageNum
        $scope.fetchFunction = $scope.find

        if($('#start-advanced').val() != '')
            $scope.request.start = formatDate($('#start-advanced').data("DateTimePicker").getDate());
        else
            delete $scope.request.start;
        if($('#end-advanced').val() != '')
            $scope.request.end= formatDate($('#end-advanced').data("DateTimePicker").getDate());
        else
            delete $scope.request.end;

        $scope.request.queueCallStatus = 'answered';
        if ($scope.request.key == "") delete $scope.request.key;
        if ($scope.request.value == "") delete $scope.request.value;
        $scope.loading=true;
        $http.post('records/search?page=' + pageNum + '&pageSize=' + PAGE_SIZE,
          JSON.stringify($scope.request)).then($scope.processJson, function(error) {
          $scope.loading=false;
          $log.error('Unable to get records ', error);
        });
    }

     $scope.simpleSearch = (pageNum) => {
        $scope.currentPage = pageNum
        $scope.fetchFunction = $scope.simpleSearch

        if($('#start-simple').val() != '')
            $scope.request.start = formatDate($('#start-simple').data("DateTimePicker").getDate());
        else
            delete $scope.request.start;
        if($('#end-simple').val() != '')
            $scope.request.end= formatDate($('#end-simple').data("DateTimePicker").getDate());
        else
            delete $scope.request.end;

        $scope.request.queueCallStatus = 'answered';
        if ($scope.request.key == "") delete $scope.request.key;
        if ($scope.request.value == "") delete $scope.request.value;
        if ($scope.request.number == "") delete $scope.request.number;
        $scope.loading=true;
        $http.post('records/simple_search?page=' + pageNum + '&pageSize=' + PAGE_SIZE,
          JSON.stringify($scope.request)).then($scope.processJson, function(error) {
          $scope.loading=false;
          $log.error('Unable to get records ', error);
        });
    }

    $scope.getAttachedData = function() {
      $http.get('attached_data')
        .then(function(response) {
          if(Array.isArray(response.data) && response.data.includes("recording")){
            var indexes=response.data;
          } else {
           var indexes=["recording",...response.data];
          }
          $scope.attachedDataKeys=indexes;
          if($scope.request.key == undefined) {
           $scope.request.key="recording"
           $scope.simpleSearch(1)
          }
        })
        .catch(function(error) {
          $log.error('Unable to fetch attached data keys: ', error);
        });
    };
    $scope.getAttachedData();

    function formatDate(date) {
        return date.format('YYYY-MM-DD HH:mm:ss');
    }

    $scope.searchByCallid = function () {
        $scope.loading=true;
        $http.post('records/callid_search?callid=' + $scope.callid,{}).then($scope.processJson, function(error) {
          $scope.loading=false;
          $log.error(
          'Unable to get record for callid '+ $scope.callid, error);
        });
    }

    $scope.getAccessLog = function () {
        $window.open('records/access');
    }

});
