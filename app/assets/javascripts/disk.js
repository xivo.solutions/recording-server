angular.module('Disk', ['angularCharts'])
.controller('DiskController', function($scope, diskSpace) {

  diskSpace.getChartData().then(function(data) {
    $scope.partitions = data;
    $scope.$apply();
  });

})
