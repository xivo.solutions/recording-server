function makeAuthCall(type, url, data) {
    var req = {
        type: type,
        beforeSend: function(request) {
            request.setRequestHeader("Authorization", 'Bearer ' + $('#rs_csrf_token').val());
        },
        url: url,
        headers: {
            'X-XSRF-TOKEN': csrfToken,
            'X-Auth-Token': authToken
        },
        dataType: 'text'
    };

    if(data) {
        req.data = JSON.stringify(data);
        req.contentType = 'application/json;charset=utf-8';
    }

    return $.ajax(req);
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
