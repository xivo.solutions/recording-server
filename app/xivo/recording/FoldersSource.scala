package xivo.recording

import akka.NotUsed
import akka.stream.Materializer
import akka.stream.scaladsl.{Concat, Source, StreamConverters}

import java.io.File
import java.nio.file.{Files, Path}

class FoldersSource(path: String)(implicit val materializer: Materializer)
    extends DirVisitorStream {

  private val rootFolder = new File(path)

  val rootFolderSource: Source[Path, NotUsed] = Source
    .single(rootFolder.toPath)
    .filter(_.toFile.isDirectory)
    .log(
      "Recording root folder",
      { el: Path =>
        streamLogger(s"Found recording root folder ${el.toAbsolutePath}")
      }
    )

  val subFoldersSource: Source[Path, NotUsed] = StreamConverters
    .fromJavaStream { () =>
      Files
        .list(new File(path).toPath)
        .filter(_.toFile.isDirectory)
    }
    .log(
      "Recording sub folders",
      { el: Path =>
        streamLogger(s"Found recording sub-folders ${el.toAbsolutePath}")
      }
    )

  val folders: Source[List[Path], NotUsed] = {
    Source
      .combine(rootFolderSource, subFoldersSource)(Concat(_))
      .fold(List[Path]())((foldersList, el) => foldersList.::(el))
  }

}
