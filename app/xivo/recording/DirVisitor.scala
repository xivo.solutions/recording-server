package xivo.recording

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.Attributes
import akka.stream.scaladsl.{Flow, FlowWithContext, Sink, Source}
import models.Record
import org.joda.time.DateTime
import org.slf4j.{Logger, LoggerFactory}
import play.api.db.{Database, NamedDatabase}

import java.io.File
import java.nio.file.Path
import java.sql.Timestamp
import javax.inject.Inject
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success, Try}

trait DirVisitorStream {
  val logger: Logger = LoggerFactory.getLogger(getClass)

  def logLevels: Attributes = {
    Attributes.logLevels(
      onElement = Attributes.LogLevels.Off,
      onFinish = Attributes.LogLevels.Info,
      onFailure = Attributes.LogLevels.Error
    )
  }

  def logWithContext[Out, Ctx](
      msg: String
  ): Flow[(Out, Ctx), (Out, Ctx), NotUsed] = {
    Flow[(Out, Ctx)].alsoTo {
      Sink.foreach[(Out, Ctx)] { e =>
        logger.info(s"File - ${e._2} $msg")
      }
    }
  }

  def toTimeStamp(weeks: Int): Timestamp =
    new Timestamp(DateTime.now().minusWeeks(weeks).getMillis)
  def streamLogger(msg: String): Unit = logger.info(s"$msg ")
}

class DirVisitor @Inject() (
    path: String,
    extensions: List[String],
    weeksToKeep: Int,
    purgeFetchSize: Int,
    @NamedDatabase("stats") dbStats: Database
) extends Runnable
    with DirVisitorStream {

  implicit val actorSystem: ActorSystem = ActorSystem()

  private val rootAudioFolder = new File(path)
  val foldersSource           = new FoldersSource(path)
  val attachedDataSource      = new AttachedDataSource(dbStats)

  def dirVisitorStream(refTime: Timestamp): Source[String, NotUsed] = {
    foldersSource.folders
      .flatMapConcat(addFoldersWithAttachedDataFilenames(refTime))
      .asSourceWithContext(_.getName)
      .filter(_.exists())
      .via(
        logWithContext(
          s"processing file older than $refTime."
        )
      )
      .map(_.delete())
      .filter(isDeleted)
      .via(
        logWithContext(s"deleted file older than $refTime.")
      )
      .via(setPurgeFlagInDatabase())
      .filter(isPurged)
      .via(logWithContext(s"flagged as purged."))
      .asSource
      .map(_._2)
      .log("dir visitor")
      .addAttributes(logLevels)
  }

  def run(): Unit = {
    logger.info(
      s"Start - Processing recording folder ${rootAudioFolder.getAbsolutePath} started"
    )

    val refTime: Timestamp = toTimeStamp(weeksToKeep)

    dirVisitorStream(refTime)
      .runWith(Sink.fold[Long, String](0)((counter, _) => counter + 1))
      .onComplete {
        case Failure(e) =>
          logger.error(
            s"An exception occurred while processing file purging. ${e.getMessage}."
          )
        case Success(counter) =>
          logger.info(
            s"Stop - Processing recording folder ${rootAudioFolder.getAbsolutePath} ended (deleted: $counter)."
          )
      }
  }

  private def isPurged = { purgeAction: Int => purgeAction >= 1 }
  private def isDeleted = { deleted: Boolean => deleted }

  private def addFoldersWithAttachedDataFilenames(
      refTime: Timestamp
  ): List[Path] => Source[File, NotUsed] = { foldersList: List[Path] =>
    attachedDataSource
      .getAttachedDataToPurge(refTime, purgeFetchSize)
      .map(addExtensionToFilename())
      .mapConcat(identity)
      .flatMapConcat(addPathToFilenameAsSource(foldersList))
  }

  private def addPathToFilenameAsSource(
      foldersList: List[Path]
  ): String => Source[File, NotUsed] = { filename: String =>
    Source(foldersList.map(f => f.resolve(filename).toFile))
  }

  private def addExtensionToFilename(): String => List[String] = { f: String =>
    extensions.map(e => Set(f, e).mkString("."))
  }

  def setPurgeFlagInDatabase()
      : FlowWithContext[Boolean, String, Int, String, NotUsed] = {
    new FlowWithContext(
      Flow[(Boolean, String)]
        .map { case (_, ctx) => (setPurgedFlag(ctx), ctx) }
        .map(e => (e._1, e._2))
    )
  }

  private def setPurgedFlag(filename: String): Int =
    dbStats.withConnection(implicit connection => {
      Record.setAttachedDataPurgeFlag(Seq(trimFileExtension(filename)))
    })

  def trimFileExtension(filename: String): String = {
    Try(filename.substring(0, filename.lastIndexOf('.'))).getOrElse(filename)
  }
}
