package xc

import java.util.Date
import anorm.SqlParser._
import anorm._

import javax.inject.Inject
import models.CallStatus
import org.joda.time.format.{
  DateTimeFormat,
  DateTimeFormatter,
  PeriodFormatter,
  PeriodFormatterBuilder
}
import org.joda.time.{DateTime, Period}
import play.api.db.{Database, NamedDatabase}
import play.api.libs.functional.syntax._
import play.api.libs.json._

case class CustomerCallDetail(
    start: DateTime,
    duration: Option[Period],
    waitTime: Option[Period],
    agentName: Option[String],
    agentNum: Option[String],
    queueName: Option[String],
    queueNum: Option[String],
    status: CallStatus.CallStatus
)

object CustomerCallDetail {
  val format: DateTimeFormatter =
    DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
  val periodFormat: PeriodFormatter = new PeriodFormatterBuilder()
    .minimumPrintedDigits(2)
    .printZeroAlways()
    .appendHours()
    .appendSeparator(":")
    .appendMinutes()
    .appendSeparator(":")
    .appendSeconds()
    .toFormatter

  def validate(json: JsValue): JsResult[CustomerCallDetail] =
    json.validate[CustomerCallDetail]

  implicit val reads: Reads[CustomerCallDetail] =
    ((JsPath \ "start").read[String].map(format.parseDateTime) and
      (JsPath \ "duration")
        .readNullable[String]
        .map(_.map(periodFormat.parsePeriod)) and
      (JsPath \ "wait_time")
        .readNullable[String]
        .map(_.map(periodFormat.parsePeriod)) and
      (JsPath \ "agent_name").readNullable[String] and
      (JsPath \ "agent_num").readNullable[String] and
      (JsPath \ "queue_name").readNullable[String] and
      (JsPath \ "queue_num").readNullable[String] and
      (JsPath \ "status")
        .readNullable[String]
        .map(s => CallStatus.withName(s.getOrElse("answered"))))(
      CustomerCallDetail.apply _
    )

  implicit val writes: Writes[CustomerCallDetail] =
    (call: CustomerCallDetail) =>
      Json.obj(
        "start"      -> call.start.toString(format),
        "duration"   -> call.duration.map(d => periodFormat.print(d.toPeriod)),
        "wait_time"  -> call.waitTime.map(d => periodFormat.print(d.toPeriod)),
        "agent_name" -> call.agentName,
        "agent_num"  -> call.agentNum,
        "queue_name" -> call.queueName,
        "queue_num"  -> call.queueNum,
        "status"     -> call.status.toString
      )
}

class CustomerCallDetailDao @Inject() (
    @NamedDatabase("stats") dbStats: Database
) {

  implicit val typer: FieldValueTyper = (field: String, value: String) =>
    field match {
      case _ => ToParameterValue[String].apply(value)
    }

  val simple: RowParser[CustomerCallDetail] = get[Date]("start_time") ~
    get[Option[Date]]("answer_time") ~
    get[Option[Date]]("end_time") ~
    get[Option[String]]("firstname") ~
    get[Option[String]]("lastname") ~
    get[Option[String]]("number") ~
    get[Option[String]]("displayName") ~
    get[Option[String]]("number_q") ~
    get[String]("status") map {
      case start ~ answer ~ end ~ firstname ~ lastname ~ number ~ displayName ~ numberQ ~ status =>
        CustomerCallDetail(
          new DateTime(start),
          end.map(d =>
            new Period(new DateTime(start), new DateTime(d))
          ) orElse Some(new Period(new DateTime(start), new DateTime())),
          answer.map(d =>
            new Period(new DateTime(start), new DateTime(d))
          ) orElse Some(new Period()),
          Some(Seq(firstname, lastname).flatten.mkString(" ")),
          number,
          displayName,
          numberQ,
          CallStatus.withName(status)
        )
    }

  def findHistoryByCustomer(
      filters: List[DynamicFilter],
      size: Int
  ): FindCustomerCallHistoryResponse =
    dbStats.withConnection({ implicit c =>
      implicit val casing: ToSnakeCase.type = ToSnakeCase

      val columns =
        "cd.start_time, cd.answer_time, cd.end_time, af.firstname, af.lastname," +
          " af.number, qf.displayName, qf.number as number_q, coalesce(cq.status::varchar, 'ongoing') as status "
      val from     = " FROM call_on_queue cq "
      val callData = "LEFT JOIN call_data cd ON cq.callid = cd.uniqueid "
      val features = "LEFT JOIN agentfeatures af ON cq.agent_num = af.number " +
        "LEFT JOIN queuefeatures qf ON cq.queue_ref = qf.name "
      val attach        = "LEFT JOIN attached_data ad ON cd.id = ad.id_call_data"
      val limitAndOrder = s" ORDER BY start_time DESC LIMIT $size"

      val count = if (filters.isEmpty) {
        SQL(s"SELECT count(*) $from").asSimple().as(scalar[Long].single)
      } else {
        SQL(
          s"SELECT COUNT(DISTINCT cq.callid) $from $callData $attach WHERE " +
            DynamicFilter.toSqlCondition(filters)
        )
          .on(DynamicFilter.toNamedParameterList(filters): _*)
          .as(scalar[Long].single)
      }
      val list = if (filters.isEmpty) {
        SQL(s"SELECT $columns $from $callData $features $limitAndOrder")
          .asSimple()
          .as(simple.*)
      } else {
        SQL(
          s"SELECT * FROM (SELECT DISTINCT ON (cq.callid) $columns $from $callData $features $attach WHERE " +
            DynamicFilter.toSqlCondition(
              filters
            ) + s" ORDER BY cq.callid DESC) cat $limitAndOrder"
        )
          .on(DynamicFilter.toNamedParameterList(filters): _*)
          .as(simple.*)
      }

      FindCustomerCallHistoryResponse(count, list)
    })
}

case class FindCustomerCallHistoryRequest(
    filters: List[DynamicFilter],
    size: Int = 10
)

case class FindCustomerCallHistoryResponse(
    total: Long,
    list: List[CustomerCallDetail]
)

object FindCustomerCallHistoryRequest {
  implicit val writesFind: Writes[FindCustomerCallHistoryRequest] =
    (f: FindCustomerCallHistoryRequest) =>
      Json.obj(
        "filters" -> f.filters,
        "size"    -> f.size
      )

  implicit val readsFind: Reads[FindCustomerCallHistoryRequest] = (
    (JsPath \ "filters").read[List[DynamicFilter]] and
      (JsPath \ "size").read[Int]
  )(FindCustomerCallHistoryRequest.apply _)

  def validate(json: JsValue): JsResult[FindCustomerCallHistoryRequest] =
    json.validate[FindCustomerCallHistoryRequest]
}

object FindCustomerCallHistoryResponse {
  implicit val readsResp: Reads[FindCustomerCallHistoryResponse] = (
    (JsPath \ "total").read[Long] and
      (JsPath \ "list").read[List[CustomerCallDetail]]
  )(FindCustomerCallHistoryResponse.apply _)

  implicit val writesResp: Writes[FindCustomerCallHistoryResponse] =
    (f: FindCustomerCallHistoryResponse) =>
      Json.obj(
        "total" -> f.total,
        "list"  -> f.list
      )
}
