package xc

import anorm.{NamedParameter, ParameterValue}
import play.api.libs.functional.syntax._
import play.api.libs.json._

sealed trait DynamicFilterOperator {
  def toSqlOperator: String
}

case object OperatorEq extends DynamicFilterOperator {
  val toSqlOperator = "="
}

case object OperatorNeq extends DynamicFilterOperator {
  val toSqlOperator = "!="
}

case object OperatorGt extends DynamicFilterOperator {
  val toSqlOperator = ">"
}

case object OperatorGte extends DynamicFilterOperator {
  val toSqlOperator = ">="
}

case object OperatorLt extends DynamicFilterOperator {
  val toSqlOperator = "<"
}

case object OperatorLte extends DynamicFilterOperator {
  val toSqlOperator = "<="
}

case object OperatorLike extends DynamicFilterOperator {
  val toSqlOperator = "like"
}

case object OperatorIlike extends DynamicFilterOperator {
  val toSqlOperator = "ilike"
}

case object OperatorIsNull extends DynamicFilterOperator {
  val toSqlOperator = "is null"
}

case object OperatorIsNotNull extends DynamicFilterOperator {
  val toSqlOperator = "is not null"
}

sealed trait DynamicFilterOrder {
  def toSqlOperator: String
}

case object OrderAsc extends DynamicFilterOrder {
  val toSqlOperator = "ASC"
}

case object OrderDesc extends DynamicFilterOrder {
  val toSqlOperator = "DESC"
}

object DynamicFilterOperator {
  def toObject(s: Option[String]): Option[DynamicFilterOperator] =
    s.map {
      case OperatorEq.toSqlOperator        => OperatorEq
      case OperatorNeq.toSqlOperator       => OperatorNeq
      case OperatorGt.toSqlOperator        => OperatorGt
      case OperatorGte.toSqlOperator       => OperatorGte
      case OperatorLt.toSqlOperator        => OperatorLt
      case OperatorLte.toSqlOperator       => OperatorLte
      case OperatorLike.toSqlOperator      => OperatorLike
      case OperatorIlike.toSqlOperator     => OperatorIlike
      case OperatorIsNull.toSqlOperator    => OperatorIsNull
      case OperatorIsNotNull.toSqlOperator => OperatorIsNotNull
    }
}

object DynamicFilterOrder {
  def toObject(s: Option[String]): Option[DynamicFilterOrder] =
    s.map {
      case OrderAsc.toSqlOperator  => OrderAsc
      case OrderDesc.toSqlOperator => OrderDesc
    }
}

case class DynamicFilter(
    field: String,
    operator: Option[DynamicFilterOperator],
    value: Option[String],
    order: Option[DynamicFilterOrder] = None
)

trait CasingTransformation {
  def transform(s: String): String
}

object ToSnakeCase extends CasingTransformation {
  override def transform(s: String): String =
    "([A-Z])".r.replaceAllIn(s, m => "_" + m.group(1).toLowerCase)
}

trait FieldValueTyper {
  def to(field: String, value: String): ParameterValue
}

object DynamicFilter {

  def toSqlCondition(
      filter: DynamicFilter
  )(implicit casing: CasingTransformation): String =
    filter.operator match {
      case Some(operator) =>
        s"${casing.transform(filter.field)} ${operator.toSqlOperator}" + filter.value
          .map(_ => s" {${filter.field}}")
          .getOrElse("")
      case _ => ""
    }

  def toSqlCondition(
      filters: List[DynamicFilter]
  )(implicit casing: CasingTransformation): String =
    filters.filter(_.operator.isDefined) map toSqlCondition mkString (" AND ")

  def toNamedParameter(
      filter: DynamicFilter
  )(implicit typer: FieldValueTyper): NamedParameter =
    NamedParameter(
      filter.field,
      typer.to(filter.field, filter.value.getOrElse(""))
    )

  def toNamedParameterList(
      filters: List[DynamicFilter]
  )(implicit typer: FieldValueTyper): List[NamedParameter] =
    filters.filter(_.value.isDefined) map toNamedParameter

  implicit val filterReads: Reads[DynamicFilter] = (
    (JsPath \ "field").read[String] and
      (JsPath \ "operator")
        .readNullable[String]
        .map(DynamicFilterOperator.toObject) and
      (JsPath \ "value").readNullable[String] and
      (JsPath \ "order").readNullable[String].map(DynamicFilterOrder.toObject)
  )(DynamicFilter.apply _)

  implicit val filterWrites: Writes[DynamicFilter] = (f: DynamicFilter) =>
    Json.obj(
      "field"    -> f.field,
      "operator" -> f.operator.map(_.toSqlOperator),
      "value"    -> f.value,
      "order"    -> f.order.map(_.toSqlOperator)
    )

}
