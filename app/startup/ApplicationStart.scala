package startup

import akka.actor.{ActorSystem, Cancellable}
import play.api.db.{Database, NamedDatabase}
import play.api.inject.ApplicationLifecycle
import play.api.{Configuration, Logger}
import xivo.recording.DirVisitor

import java.util.concurrent.TimeUnit
import javax.inject.{Inject, Singleton}
import scala.jdk.CollectionConverters._
import scala.concurrent.Future
import scala.concurrent.duration.FiniteDuration

@Singleton
class ApplicationStart @Inject() (
    lifecycle: ApplicationLifecycle,
    actorSystem: ActorSystem,
    configuration: Configuration,
    @NamedDatabase("stats") dbStats: Database
) {

  val logger: Logger = Logger(getClass.getName)

  private var appSched = Option.empty[Cancellable]

  lifecycle.addStopHook { () =>
    Future.successful {
      logger.info("Stopping recording server")
      appSched.foreach(_.cancel())
    }
  }

  dumpConfiguration()

  startPurgingFiles()

  private def startPurgingFiles(): Unit = {
    import scala.concurrent.ExecutionContext.Implicits.global

    val phase     = FiniteDuration(2, TimeUnit.MINUTES)
    val frequency = FiniteDuration(1, TimeUnit.DAYS)

    val audioDir = configuration
      .getOptional[String]("audio.folder")
      .getOrElse("/var/spool/recording-server")
    val weeksToKeep =
      configuration.getOptional[Int]("audio.weeksToKeep").getOrElse(52)
    val audioExtensions =
      configuration.underlying.getStringList("audio.extensions").asScala.toList
    val purgeFetchSize: Int =
      configuration.underlying.getInt("audio.purgeQueryFetchSize")

    logger.info(
      s"Starting purge files (DirVisitor) scheduler : phase : $phase frequency : $frequency"
    )

    appSched = Some(
      actorSystem.scheduler.scheduleWithFixedDelay(phase, frequency)(
        new DirVisitor(
          audioDir,
          audioExtensions,
          weeksToKeep,
          purgeFetchSize,
          dbStats
        )
      )
    )

  }

  private def dumpConfiguration(): Unit = {
    val audioFolder      = configuration.get[String]("audio.folder")
    val audioWeeksToKeep = configuration.get[Int]("audio.weeksToKeep")
    val xivoHost         = configuration.get[String]("xivohost")

    logger.info("Starting recording server")
    logger.info("----------Configuration----------")
    logger.info(s"xivohost           : $xivoHost")
    logger.info(s"audio.folder       : $audioFolder")
    logger.info(s"audio.weeksToKeep  : $audioWeeksToKeep")
  }
}
