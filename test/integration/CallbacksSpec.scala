package integration

import java.util.UUID

import helpers.{AppTest, CallbacksHelper}
import models.authentication.AdminRight
import models.{CallbackStatus, CallbackTicket, CallbackTicketDao, DBUtil}
import org.joda.time.{DateTime, LocalDate}
import play.api.cache.SyncCacheApi
import play.api.http.Status.CREATED
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.{JsSuccess, Json}
import play.api.test.FakeRequest
import play.api.test.Helpers.{route, _}


class CallbacksSpec extends AppTest {


  override def fakeApplication() = new GuiceApplicationBuilder()
    .configure(testConfig).build()


  val callbackTicketDao = app.injector.instanceOf(classOf[CallbackTicketDao])
  val cache = app.injector.instanceOf(classOf[SyncCacheApi])
  val context = testConfig("play.http.context")


  "Callbacks service" should {

    "create a new ticket" in {
      DBUtil.cleanTable("callback_ticket")
      val listUuid = UUID.randomUUID()
      val requestUuid = UUID.randomUUID()
      val json = Json.obj("listUuid" -> listUuid,
        "requestUuid" -> requestUuid,
        "queueRef" -> "thequeue",
        "agentNum" -> "4000",
        "dueDate" -> "2015-01-01",
        "started" -> "2015-01-01 08:00:00",
        "phoneNumber" -> "33654879"
      )
      cache.set("test", AdminRight())

      val req = FakeRequest("POST", s"$context/callback_tickets").withSession(("username", "test"))
        .withJsonBody(json)
      val Some(res) = route(app, req)

      status(res) should be(CREATED)


      val newTicket = contentAsJson(res).validate[CallbackTicket].get
      newTicket.uuid.isEmpty should be(false)
      CallbacksHelper.allTickets should equal(List(CallbackTicket(
        uuid = newTicket.uuid,
        listUuid = listUuid,
        requestUuid = requestUuid,
        queueRef = "thequeue",
        agentNum = "4000",
        dueDate = new LocalDate(2015, 1, 1),
        started = formatter.parseDateTime("2015-01-01 08:00:00"),
        lastUpdate = Some(formatter.parseDateTime("2015-01-01 08:00:00")),
        phoneNumber = Some("33654879"))))
    }
    "update an existing ticket" in {
      DBUtil.cleanTable("callback_ticket")
      val ticket = callbackTicketDao.create(CallbackTicket(None, UUID.randomUUID(), UUID.randomUUID(), "thequeue", "4000",
        new LocalDate(), new DateTime(), Some(new DateTime), firstName = Some("John")))

      val json = Json.obj(
        "comment" -> "Small comment",
        "status" -> "noanswer",
        "callid" -> "123456.789"
      )

      cache.set("test", AdminRight())
      val res = route(app, FakeRequest("PUT", s"$context/callback_tickets/${ticket.uuid.get}").withSession(("username", "test")).withJsonBody(json)).get

      status(res) should be(OK)
      CallbacksHelper.allTickets.map(_.copy(lastUpdate = None)) shouldEqual List(
        ticket.copy(lastUpdate = None, comment = Some("Small comment"), status = Some(CallbackStatus.NoAnswer), callid = Some("123456.789"))
      )
    }

    "try to update and return 404 if the ticket does not exist" in {
      val res = route(app, FakeRequest("PUT", s"$context/callback_tickets/${UUID.randomUUID()}").withSession(("username", "test")).withJsonBody(Json.obj())).get
      status(res) should be(NOT_FOUND)
    }

    "export tickets in CSV" in {
      DBUtil.cleanTable("callback_ticket")
      val ticket = callbackTicketDao.create(CallbackTicket(None, UUID.randomUUID(), UUID.randomUUID(), "thequeue", "4000",
        new LocalDate(2015, 1, 1), formatter.parseDateTime("2015-01-01 08:00:00"), Some(formatter.parseDateTime("2015-01-01 08:30:00")), firstName = Some("John")))

      cache.set("test", AdminRight())
      val res = route(app, FakeRequest("POST", s"$context/callback_tickets/search?listUuid=${ticket.listUuid}").withSession(("username", "test"))).get

      status(res) should be(OK)

      contentAsString(res) shouldBe
        s"""uuid,listUuid,requestUuid,queueRef,agentNum,dueDate,started,lastUpdate,callid,status,comment,phoneNumber,mobilePhoneNumber,firstName,lastName,company,description,periodStart,periodEnd
           |"${ticket.uuid.get}","${ticket.listUuid}","${ticket.requestUuid}","thequeue","4000","2015-01-01","2015-01-01 08:00:00","2015-01-01 08:30:00","","","","","","John","","","","",""""".stripMargin
    }

    "find a ticket by uuid" in {
      DBUtil.cleanTable("callback_ticket")
      val ticket = callbackTicketDao.create(CallbackTicket(
        uuid = None,
        listUuid = UUID.randomUUID(),
        requestUuid = UUID.randomUUID(),
        queueRef = "thequeue",
        agentNum = "4000",
        dueDate = new LocalDate(2015, 1, 1),
        started = formatter.parseDateTime("2015-01-01 08:00:00"),
        lastUpdate = Some(formatter.parseDateTime("2015-01-01 08:00:00")),
        phoneNumber = Some("33654879")))

      cache.set("test", AdminRight())
      val res = route(app, FakeRequest("GET", s"$context/callback_tickets/${ticket.uuid.get}").withSession(("username", "test"))).get

      status(res) should be(OK)
      contentAsJson(res).validate[CallbackTicket] shouldBe JsSuccess(ticket)
    }
  }
}
