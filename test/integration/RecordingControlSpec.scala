package integration

import helpers.{AppTest, RecordHelper}
import models.authentication.AdminRight
import models.{DBUtil, FilteredIncalls, FilteredInternalNumbers}
import play.api.cache.SyncCacheApi
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.Json
import play.api.test.FakeRequest
import play.api.test.Helpers.{status, _}

class RecordingControlSpec extends AppTest {

  override def fakeApplication() = new GuiceApplicationBuilder()
    .configure(testConfig).build()

  val cache = app.injector.instanceOf(classOf[SyncCacheApi])
  val context = testConfig("play.http.context")

  "The RecordingControl Controller" should {
    "list filtered incalls" in {
      DBUtil.cleanTable("filtered_numbers")
      RecordHelper.insertFilteredIncall("123456")
      cache.set("toto", AdminRight())

      val res = route(app,FakeRequest("GET", s"$context/filtered_incalls").withSession("username" -> "toto")).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.toJson(List("123456"))
    }

    "return a single filtered incall" in {
      DBUtil.cleanTable("filtered_numbers")
      RecordHelper.insertFilteredIncall("123456")

      val res = route(app,FakeRequest("GET", s"$context/filtered_incalls/123456")).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.toJson("123456")
    }

    "return 404 if the provided number does not exist" in {
      DBUtil.cleanTable("filtered_numbers")
      RecordHelper.insertFilteredIncall("123456")

      val res = route(app,FakeRequest("GET", s"$context/filtered_incalls/456789")).get

      status(res) shouldEqual NOT_FOUND
    }

    "delete a filtered incall" in {
      DBUtil.cleanTable("filtered_numbers")
      List("123456", "456789").foreach(RecordHelper.insertFilteredIncall)
      cache.set("toto", AdminRight())

      val res = route(app,FakeRequest("DELETE", s"$context/filtered_incalls/123456").withSession("username" -> "toto")).get

      status(res) shouldEqual NO_CONTENT
      FilteredIncalls.all() shouldEqual List("456789")
    }

    "create a filtered incall" in {
      DBUtil.cleanTable("filtered_numbers")
      cache.set("toto", AdminRight())

      val res = route(app,FakeRequest("POST", s"$context/filtered_incalls").withJsonBody(Json.toJson("123456")).
        withSession("username" -> "toto")).get

      status(res) shouldEqual CREATED
      FilteredIncalls.all() shouldEqual List("123456")
    }

    "return an error when creating an existing filtered incall" in {
      DBUtil.cleanTable("filtered_numbers")
      RecordHelper.insertFilteredIncall("123456")
      cache.set("toto", AdminRight())

      val res = route(app,FakeRequest("POST", s"$context/filtered_incalls").withJsonBody(Json.toJson("123456")).
        withSession("username" -> "toto")).get

      status(res) shouldEqual BAD_REQUEST
      contentAsJson(res) shouldEqual Json.toJson("Le numéro 123456 existe déjà")
    }

    "list filtered internal numbers" in {
      DBUtil.cleanTable("filtered_numbers")
      RecordHelper.insertFilteredInternalNumber("123456")
      cache.set("toto", AdminRight())

      val res = route(app,FakeRequest("GET", s"$context/filtered_internal_numbers").withSession("username" -> "toto")).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.toJson(List("123456"))
    }

    "return a single filtered internal number" in {
      DBUtil.cleanTable("filtered_numbers")
      RecordHelper.insertFilteredInternalNumber("123456")

      val res = route(app,FakeRequest("GET", s"$context/filtered_internal_numbers/123456")).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.toJson("123456")
    }

    "return 404 if the provided internal number does not exist" in {
      DBUtil.cleanTable("filtered_numbers")
      RecordHelper.insertFilteredInternalNumber("123456")

      val res = route(app,FakeRequest("GET", s"$context/filtered_internal_numbers/456789")).get

      status(res) shouldEqual NOT_FOUND
    }

    "delete a filtered internal number" in {
      DBUtil.cleanTable("filtered_numbers")
      List("123456", "456789").foreach(RecordHelper.insertFilteredInternalNumber)
      cache.set("toto", AdminRight())

      val res = route(app,FakeRequest("DELETE", s"$context/filtered_internal_numbers/123456").withSession("username" -> "toto")).get

      status(res) shouldEqual NO_CONTENT
      FilteredInternalNumbers.all() shouldEqual List("456789")
    }

    "create a filtered internal number" in {
      DBUtil.cleanTable("filtered_numbers")
      cache.set("toto", AdminRight())

      val res = route(app,FakeRequest("POST", s"$context/filtered_internal_numbers").withJsonBody(Json.toJson("123456")).
        withSession("username" -> "toto")).get

      status(res) shouldEqual CREATED
      FilteredInternalNumbers.all() shouldEqual List("123456")
    }

    "return an error when creating an existing filtered internal number" in {
      DBUtil.cleanTable("filtered_numbers")
      RecordHelper.insertFilteredInternalNumber("123456")
      cache.set("toto", AdminRight())

      val res = route(app,FakeRequest("POST", s"$context/filtered_internal_numbers").withJsonBody(Json.toJson("123456")).
        withSession("username" -> "toto")).get

      status(res) shouldEqual BAD_REQUEST
      contentAsJson(res) shouldEqual Json.toJson("Le numéro 123456 existe déjà")
    }
  }
}
