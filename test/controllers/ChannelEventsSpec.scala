package controllers

import akka.stream.Materializer
import helpers.AppTest
import models.{ChannelEventDao, ChannelEventLog}
import java.time.LocalDateTime
import org.mockito.Mockito.when
import org.scalatestplus.mockito.MockitoSugar
import play.api.db.Database
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.Json
import play.api.test.FakeRequest
import play.api.test.Helpers.{call, status, _}

import scala.util.{Failure, Success}

class ChannelEventsSpec extends AppTest with MockitoSugar {

  implicit lazy val materializer: Materializer = app.materializer
  val ChannelEventDaoMock: ChannelEventDao = mock[ChannelEventDao]
  val dbStatsMock: Database = mock[Database]

  override def fakeApplication(): play.api.Application = new GuiceApplicationBuilder()
    .configure(testConfig)
    .overrides(bind[ChannelEventDao].to(ChannelEventDaoMock))
    .overrides(bind[Database].qualifiedWith("stats").to(dbStatsMock))
    .build()

  class Helper() {
    val controller: ChannelEvents = app.injector.instanceOf(classOf[ChannelEvents])
  }

  "ChannelEventsSpec" should {
    "can retrieve cel list" in new Helper {
      val request = FakeRequest(GET, "/cel")
        .withSession(("username", "test"))
      val date = LocalDateTime.of(2020,3,26, 13,18,27, 123456000)
      val dateString = "2020-03-26 13:18:27.123456"
      val returnList = List(
        createChannelEventLog(1, date, "James Bond", "1000", "SIP/abcd-00000001", "1584976887.1"),
        createChannelEventLog(2, date, "Bruce Willis", "1001", "SIP/abcd-00000002", "mds1-1584976887.2")
      )
      when(ChannelEventDaoMock.list(0, 10)).thenReturn(Success(returnList))

      val result = call(controller.list(0, 10), request)

      status(result) shouldEqual OK
      contentAsJson(result) shouldEqual Json.parse(
        s"""
           |[{
           |    "id": "1",
           |    "eventtype": "",
           |    "eventtime": "${dateString}",
           |    "userdeftype": "",
           |    "cid_name": "James Bond",
           |    "cid_num": "1000",
           |    "cid_ani": "1000",
           |    "cid_rdnis": "",
           |    "cid_dnid": "",
           |    "exten": "",
           |    "context": "",
           |    "channame": "SIP/abcd-00000001",
           |    "appname": "",
           |    "appdata": "",
           |    "amaflags": "0",
           |    "accountcode": "",
           |    "peeraccount": "",
           |    "uniqueid": "1584976887.1",
           |    "linkedid": "",
           |    "userfield": "",
           |    "peer": "",
           |    "call_log_id": "0",
           |    "extra": ""
           |  }, {
           |    "id": "2",
           |    "eventtype": "",
           |    "eventtime": "${dateString}",
           |    "userdeftype": "",
           |    "cid_name": "Bruce Willis",
           |    "cid_num": "1001",
           |    "cid_ani": "1001",
           |    "cid_rdnis": "",
           |    "cid_dnid": "",
           |    "exten": "",
           |    "context": "",
           |    "channame": "SIP/abcd-00000002",
           |    "appname": "",
           |    "appdata": "",
           |    "amaflags": "0",
           |    "accountcode": "",
           |    "peeraccount": "",
           |    "uniqueid": "mds1-1584976887.2",
           |    "linkedid": "",
           |    "userfield": "",
           |    "peer": "",
           |    "call_log_id": "0",
           |    "extra": ""
           |  }
           |]
           |""".stripMargin
      )
    }

    "raise an error if the cel retrive fail" in new Helper {
      val request = FakeRequest(GET, "/cel")
        .withSession(("username", "test"))
      when(ChannelEventDaoMock.list(0, 10)).thenReturn(Failure(new Error("Cannot retrive cels")))

      val result = call(controller.list(0, 10), request)

      status(result) shouldEqual INTERNAL_SERVER_ERROR
    }

    "send a Forbidden error if there is no active session" in new Helper {
      val request = FakeRequest(GET, "/cel")
      when(ChannelEventDaoMock.list(0, 10)).thenReturn(Failure(new Error("Cannot retrive cels")))

      val result = call(controller.list(0, 10), request)

      status(result) shouldEqual FORBIDDEN
    }
  }

  def createChannelEventLog(id: Int, date: LocalDateTime, name: String, num: String, channame: String, uniqueid: String) =
    ChannelEventLog(id, "", date, "", name, num, num, "", "", "", "", channame, "", "", 0, "", "", uniqueid, "", "", "", Some(0), None)
}
