package models

import play.api.libs.json.{JsSuccess, Json}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class CallbackTicketPatchSpec extends AnyFlatSpec with Matchers {

  "CallbackTicketPatch" should "be parsed from JSON" in {
    val json = Json.obj("status" -> "fax", "comment" -> "A small comment", "callid" -> "123456.789")
    Json.parse(json.toString()).validate[CallbackTicketPatch] shouldEqual JsSuccess(
      CallbackTicketPatch(
        Some(CallbackStatus.Fax),
        Some("A small comment"),
        Some("123456.789")
      )
    )
  }

}
