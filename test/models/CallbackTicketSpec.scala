package models

import java.util.UUID

import helpers.{CallbacksHelper, UnitDbTest}
import org.joda.time.{LocalDate, LocalTime}

class CallbackTicketSpec extends UnitDbTest {

  val periodStart = new LocalTime(8, 0, 0)
  val periodEnd = new LocalTime(11, 0, 0)
  val periodUuid = CallbacksHelper.insertPeriod(periodStart, periodEnd)
  val requestUuid = CallbacksHelper.insertRequest(periodUuid, "1000")


  "The CallbackTicket" should {

    "export a list of tickets to CSV" in {
      val t1 = CallbackTicket(Some(UUID.randomUUID()), UUID.randomUUID(), UUID.randomUUID(), "thequeue", "4000", new LocalDate(2014, 1, 1), formatter.parseDateTime("2014-01-01 08:00:00"), Some(formatter.parseDateTime("2014-01-01 08:00:00")),
        firstName = Some("John"), phoneNumber = Some("1000"), description = Some("the description"), comment = Some("the comment"), periodStart = Some(new LocalTime(8, 0, 0)), periodEnd = Some(new LocalTime(11, 0, 0)))
      val t2 = CallbackTicket(Some(UUID.randomUUID()), UUID.randomUUID(), UUID.randomUUID(), "otherqueue", "4002", new LocalDate(2014, 1, 1), formatter.parseDateTime("2014-01-02 08:00:00"), Some(formatter.parseDateTime("2014-01-02 08:00:00")),
        lastName = Some("Doe"), mobilePhoneNumber = Some("3366578487"), status = Some(CallbackStatus.Answered))

      CallbackTicket.toCsv(List(t1, t2)) shouldBe (
        s"""uuid,listUuid,requestUuid,queueRef,agentNum,dueDate,started,lastUpdate,callid,status,comment,phoneNumber,mobilePhoneNumber,firstName,lastName,company,description,periodStart,periodEnd
           |"${t1.uuid.get}","${t1.listUuid}","${t1.requestUuid}","thequeue","4000","2014-01-01","2014-01-01 08:00:00","2014-01-01 08:00:00","","","the comment","1000","","John","","","the description","08:00:00","11:00:00"
           |"${t2.uuid.get}","${t2.listUuid}","${t2.requestUuid}","otherqueue","4002","2014-01-01","2014-01-02 08:00:00","2014-01-02 08:00:00","","answered","","","3366578487","","Doe","","","",""""".stripMargin)
    }

  }
}
