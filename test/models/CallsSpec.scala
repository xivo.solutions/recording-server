package models

import helpers.{DbTest, RecordHelper}
import models.authentication.AdminRight
import org.specs2.mutable.Before
import play.api.test.PlaySpecification

class CallsSpec extends PlaySpecification with DbTest {

  trait clean extends Before {
    val tables = List("attached_data", "call_data")
    def before = {
      for (table <- tables)
        DBUtil.cleanTable(table)
    }
  }

  "The Calls singleton" should {
    "tell if there is a call more recent than the given date" in new clean {
      RecordHelper.insertCallData("123456", formatter.parseDateTime("2014-12-01 08:35:12").toDate, None)
      Calls.callExistsAfterTime(formatter.parseDateTime("2014-12-01 01:00:00").toDate) should beTrue
      Calls.callExistsAfterTime(formatter.parseDateTime("2014-12-01 09:00:00").toDate) should beFalse
    }

    "add attached data to a call" in new clean {
      val id = RecordHelper.insertCallData("123456", formatter.parseDateTime("2014-12-01 08:35:12").toDate, None).get
      Calls.addAttachedDataById(id, List(AttachedData("key1", "value1", None), AttachedData("key2", "value2", None)))

      implicit val paginator = Paginator(0, 1)
      Record.searchByCallid("123456", AdminRight()).map(_.attachedData) shouldEqual List(Map("key1" -> "value1", "key2" -> "value2"))
    }

    "add attached data with reason to a call" in new clean {
      val id = RecordHelper.insertCallData("123456", formatter.parseDateTime("2014-12-01 08:35:12").toDate, None).get
      Calls.addAttachedDataByCallId(callDataUniqueId = "123456", AttachedData("xivo_call_reason", "2018-08-13 13:21:04", None))

      implicit val paginator = Paginator(0, 1)
      Record.searchByCallid("123456", AdminRight()).map(_.attachedData) shouldEqual List(Map("xivo_call_reason" -> "2018-08-13 13:21:04"))
    }

    "not add attached data with wrong date time format" in new clean {
      val callReason: AttachedData = AttachedData("xivo_call_reason", "wrong_date", None)
      val id = RecordHelper.insertCallData("123456", formatter.parseDateTime("2014-12-01 08:35:12").toDate, None).get
      Calls.addAttachedDataByCallId(callDataUniqueId = "123456", callReason) shouldEqual
        FailedAttachedDataReponse(s"Call data is not correctly formatted: $callReason")

      implicit val paginator = Paginator(0, 1)
      Record.searchByCallid("123456", AdminRight()).map(_.attachedData) shouldEqual List(Map())
    }

    "add attached data with text type" in new clean {
      val callReason: AttachedData = AttachedData("xivo_call_reason", "foobar", Some("text"))
      val id = RecordHelper.insertCallData("123456", formatter.parseDateTime("2014-12-01 08:35:12").toDate, None).get
      Calls.addAttachedDataByCallId(callDataUniqueId = "123456", callReason)

      implicit val paginator = Paginator(0, 1)
      Record.searchByCallid("123456", AdminRight()).map(_.attachedData) shouldEqual List(Map("xivo_call_reason" -> "foobar"))
    }

    "not add attached data with weird type" in new clean {
      val callReason: AttachedData = AttachedData("xivo_call_reason", "nobody", Some("body"))
      val id = RecordHelper.insertCallData("123456", formatter.parseDateTime("2014-12-01 08:35:12").toDate, None).get
      Calls.addAttachedDataByCallId(callDataUniqueId = "123456", callReason) shouldEqual
        FailedAttachedDataReponse(s"Call data is not correctly formatted: $callReason")

      implicit val paginator = Paginator(0, 1)
      Record.searchByCallid("123456", AdminRight()).map(_.attachedData) shouldEqual List(Map())
    }
  }
}
