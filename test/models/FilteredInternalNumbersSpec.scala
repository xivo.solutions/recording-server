package models

import helpers.{RecordHelper, DbTest}
import play.api.test.PlaySpecification

class FilteredInternalNumbersSpec extends PlaySpecification with DbTest {

  "The FilteredInternalNumbers singleton" should {
    "list all filtered internal numbers" in {
      DBUtil.cleanTable("filtered_numbers")
      List("123456", "456789").foreach(RecordHelper.insertFilteredInternalNumber(_))

      FilteredInternalNumbers.all().sorted shouldEqual List("123456", "456789")
    }

    "find a filtered internal number" in {
      DBUtil.cleanTable("filtered_numbers")
      List("123456", "456789").foreach(RecordHelper.insertFilteredInternalNumber(_))

      FilteredInternalNumbers.find("123456") shouldEqual Some("123456")
    }

    "return None if the provided number does not exist" in {
      DBUtil.cleanTable("filtered_numbers")
      List("456789").foreach(RecordHelper.insertFilteredInternalNumber(_))

      FilteredInternalNumbers.find("123456") shouldEqual None
    }

    "delete a filtered internal number" in {
      DBUtil.cleanTable("filtered_numbers")
      List("123456", "456789").foreach(RecordHelper.insertFilteredInternalNumber(_))

      FilteredInternalNumbers.delete("123456")

      FilteredInternalNumbers.all() shouldEqual List("456789")
    }

    "create a filtered incall" in {
      DBUtil.cleanTable("filtered_numbers")

      FilteredInternalNumbers.create("123456")

      FilteredInternalNumbers.all() shouldEqual List("123456")
    }

    "throw an exception if the filtered incall already exists" in {
      DBUtil.cleanTable("filtered_numbers")
      FilteredInternalNumbers.create("123456")
      FilteredInternalNumbers.create("123456") should throwA[NumberAlreadyExistsException]
    }
  }
}
