package models

import java.security.InvalidParameterException

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import play.api.libs.json._
import play.api.test.PlaySpecification

class SearchCriteriaSpec extends PlaySpecification {

  "The SearchCriteria singleton" should {

    val formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")

    "parse the Json" in {
      val json = Json.parse("""{
              "callee": "1000",
              "caller": "2000",
              "agent": "3000",
              "queue": "thequeue",
              "start": "2012-05-13 14:15:16",
              "end": "2012-05-13 19:15:16",
              "direction": "incoming",
              "queueCallStatus": "answered",
              "key": "my_key",
              "value": "the value"
      }""")

      json.validate[SearchCriteria].get shouldEqual GenericSearchCriteria(
        Some("1000"),
        Some("2000"),
        Some("3000"),
        Some("thequeue"),
        Some(DateTime.parse("2012-05-13 14:15:16", formatter)),
        Some(DateTime.parse("2012-05-13 19:15:16", formatter)),
        CallDirection.Incoming,
        Some(QueueCallStatus.Answered),
        Some("my_key"),
        Some("the value")
      )
    }

    "not fail with a missing parameter" in {
      val json = Json.parse("""{
              "caller": "2000",
              "agent": "3000"
      }""")
      json.validate[SearchCriteria].get shouldEqual GenericSearchCriteria(
        caller = Some("2000"),
        agent = Some("3000")
      )
    }

    "not be valid if value is defined but not key" in {
      GenericSearchCriteria(
        None,
        None,
        None,
        None,
        None,
        None,
        CallDirection.All,
        None,
        None,
        Some("the value")
      ).validate() should
        throwA[InvalidParameterException](
          "key is mandatory when searching a value"
        )
    }
  }

  "The SimpleSearchCriteria" should {
    val formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")

    "parse the Json" in {
      val json = Json.parse("""{
              "number": "1000",
              "start": "2012-05-13 14:15:16",
              "end": "2012-05-13 19:15:16",
              "key": "my_key",
              "value": "the value",
              "queueCallStatus": "answered"
      }""")

      json.validate[SimpleSearchCriteria].get shouldEqual SimpleSearchCriteria(
        Some("1000"),
        Some(DateTime.parse("2012-05-13 14:15:16", formatter)),
        Some(DateTime.parse("2012-05-13 19:15:16", formatter)),
        Some("my_key"),
        Some("the value"),
        Some(QueueCallStatus.Answered)
      )
    }

    "not fail with a missing parameter" in {
      val json = Json.parse("""{
              "number": "1000",
              "queueCallStatus": "answered"
      }""")
      json.validate[SimpleSearchCriteria].get shouldEqual SimpleSearchCriteria(
        number = Some("1000"),
        queueCallStatus = Some(QueueCallStatus.Answered)
      )
    }
  }

}
