package models

import akka.actor.ActorSystem
import akka.stream.scaladsl.{Flow, Keep, Sink}
import akka.stream.testkit.scaladsl.TestSink
import helpers.{DbTest, RecordHelper, Status}
import models.authentication.{AdminRight, LimitedRight}
import org.joda.time.{DateTime, Duration}
import org.scalatest.concurrent.ScalaFutures
import org.specs2.mutable.Before
import play.api.test.PlaySpecification

import java.sql.Timestamp
import scala.concurrent.Future

class RecordSpec extends PlaySpecification with DbTest with ScalaFutures {
  implicit val paginator = Paginator(0, 20)

  implicit val system: ActorSystem = ActorSystem()

  List("agentfeatures", "agentgroup", "extensions", "queuefeatures").foreach(DBUtil.cleanTable)
  val groupId1 = RecordHelper.insertAgentGroup("group1").get.toInt
  val groupId2 = RecordHelper.insertAgentGroup("group2").get.toInt
  val agentId1 = RecordHelper.insertAgent("Pierre", "Martin", "3000", groupId1).get.toInt
  val agentId2 = RecordHelper.insertAgent("Jean", "Dupond", "3001", groupId1).get.toInt
  val agentId3 = RecordHelper.insertAgent("Jean", "Martin", "3002", groupId2).get.toInt
  val agentId4 = RecordHelper.insertAgent("Philippe", "Joli", "3003", groupId2).get.toInt
  val queueId1 = RecordHelper.insertQueue("thequeue", "5000").get.toInt
  val queueId2 = RecordHelper.insertQueue("une_file", "1234").get.toInt
  val incallId1 = RecordHelper.insertIncall("5300").get.toInt
  val incallId2 = RecordHelper.insertIncall("5301").get.toInt

  trait clean extends Before {
    val tables = List("attached_data", "call_data", "call_on_queue", "call_element", "transfers")
    def before = {
      for (table <- tables)
        DBUtil.cleanTable(table)
    }
  }


  "The Record singleton" should {

    "search records for callee, caller and agent" in new clean {
      val startDate = DateTime.now
      val endDate = startDate.plusMinutes(5)
      val id1 = "123.456"
      val id2 = "456.789"
      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate, Some(endDate), Some("1000"), Some("2000"))
      RecordHelper.insertCallOnQueue(id1, "3000", "thequeue",status=QueueCallStatus.Timeout)
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate, Some(endDate), Some("1001"), Some("2001"))
      RecordHelper.insertCallOnQueue(id2, "3001", "thequeue")

      val records:List[Record] = Record.search(GenericSearchCriteria(callee=Some("2000"), caller=Some("1000"), agent=Some("3000")), AdminRight())

      records.size shouldEqual 1
      records(0).recordId shouldEqual Some(s"gw1-$id1")
      records(0).agent shouldEqual List("Pierre Martin (3000)")
      records(0).queue shouldEqual List("thequeue (5000)")
      records(0).status shouldEqual QueueCallStatus.Timeout
    }

    "search records for callee and caller" in new clean {
      val startDate = DateTime.now
      val endDate = startDate.plusMinutes(5)
      val id1 = "123.456"
      val id2 = "456.789"
      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate, Some(endDate), Some("1000"), None)
      RecordHelper.insertCallOnQueue(id1, "3000", "thequeue")
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate, Some(endDate), Some("1001"), Some("2001"))
      RecordHelper.insertCallOnQueue(id2, "3001", "thequeue")

      val records = Record.search(GenericSearchCriteria(None, Some("1000"), Some("3000")), AdminRight())

      records.size shouldEqual 1
      records(0).recordId shouldEqual Some(s"gw1-$id1")
    }

    "search records only for agent criterion do not select null end date" in new clean {
      val startDate = DateTime.now
      val endDate = startDate.plusMinutes(5)
      val id1 = "123.456"
      val id2 = "456.789"
      val id3 = "789.123"
      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate, None, Some("1000"), Some("2000"))
      RecordHelper.insertCallOnQueue(id1, "3000", "thequeue")
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate, Some(endDate), Some("1001"), Some("2001"))
      RecordHelper.insertCallOnQueue(id2, "3001", "thequeue")
      RecordHelper.insertRecord(id3, s"gw1-$id3", startDate, Some(endDate), None, Some("2000"))
      RecordHelper.insertCallOnQueue(id3, "3000", "thequeue")

      val records = Record.search(GenericSearchCriteria(agent=Some("3000")), AdminRight())

      records.size shouldEqual 1
      records.head.recordId shouldEqual Some(s"gw1-$id3")
    }

    "find records by agent even if they are not on the first page" in new clean {
      val startDate = DateTime.now
      val endDate = startDate.plusMinutes(5)
      val id1 = "123.456"
      val id2 = "456.789"
      implicit val paginator = Paginator(0,1)
      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate, Some(endDate), Some("1000"), Some("2000"))
      RecordHelper.insertCallOnQueue(id1, "3001", "thequeue")
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate, Some(endDate), Some("1001"), Some("2001"))
      RecordHelper.insertCallOnQueue(id2, "3000", "thequeue")

      val records = Record.search(GenericSearchCriteria(agent = Some("3000")), AdminRight())

      records.size shouldEqual 1
      records(0).recordId shouldEqual Some(s"gw1-$id2")
    }

    "search records by queue" in new clean {
      val startDate = DateTime.now
      val endDate = startDate.plusMinutes(5)
      val id1 = "123.456"
      val id2 = "456.789"

      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate, Some(endDate), Some("1001"), Some("2001"))
      RecordHelper.insertCallOnQueue(id1, "3001", "myqueue")
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate, Some(endDate), Some("1000"), Some("2000"))
      RecordHelper.insertCallOnQueue(id2, "3000", "thequeue")

      val records = Record.search(GenericSearchCriteria(None, None, None, Some("thequeue"), None, None, CallDirection.All, None, Some("recording"), None), AdminRight())


      records.size shouldEqual 1
      records.head.recordId shouldEqual Some(s"gw1-$id2")
    }

    "search only answered queue calls" in new clean {
      private val id1 = "123.456"
      private val id2 = "456.789"

      RecordHelper.insertFullRecord(id=id1, status=QueueCallStatus.Answered)
      RecordHelper.insertFullRecord(id=id2, status=QueueCallStatus.Abandoned)

      private val records = Record.search(GenericSearchCriteria(queueCallStatus=Some(QueueCallStatus.Answered)), AdminRight())

      records.size shouldEqual 1
      records.head.callDataId shouldEqual id1
    }

    "search only answered queue calls using SimpleSearchCriteria" in new clean {
      private val id1 = "123.456"
      private val id2 = "456.789"

      RecordHelper.insertFullRecord(id=id1, status=QueueCallStatus.Answered)
      RecordHelper.insertFullRecord(id=id2, status=QueueCallStatus.Abandoned)

      private val records = Record.search(SimpleSearchCriteria(queueCallStatus=Some(QueueCallStatus.Answered)), AdminRight())

      records.size shouldEqual 1
      records.head.callDataId shouldEqual id1
    }

    "search only answered queue calls and outcalls" in new clean {
      private val id1 = "123.456"
      private val id2 = "456.789"
      private val id3 = "123.567"

      val startDate: DateTime = DateTime.now
      val endDate: DateTime = startDate.plusMinutes(5)

      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate, Some(endDate), Some("1001"), Some("2001"))
      RecordHelper.insertCallOnQueue(id1, "3001", "myqueue", QueueCallStatus.Answered)
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate, Some(endDate), Some("1001"), Some("2001"))
      RecordHelper.insertCallOnQueue(id1, "3001", "myqueue", QueueCallStatus.Abandoned)

      RecordHelper.insertCallData(uniqueId = id3, startDate.toDate, Some(endDate.toDate), Some("1000"), Some("2000"), status = Some(Status.Answer))

      private val records = Record.search(GenericSearchCriteria(queueCallStatus=Some(QueueCallStatus.Answered)), AdminRight())

      records.size shouldEqual 2
      records.head.callDataId shouldEqual id3
      records(1).callDataId shouldEqual id1
    }

    "search and return paginated data" in new clean {
      var startDate = DateTime.now
      var endDate = startDate.plusMinutes(5)
      for (i <- 0 to 19) {
        RecordHelper.insertRecord(i.toString, s"gw1-$i", startDate.plusSeconds(19-i), Some(endDate), Some("1000"), Some("2000"))
      }
      RecordHelper.insertCallData("22", startDate.toDate, Some(endDate.toDate), Some("1000"), Some("2000"))

      implicit var paginator = Paginator(0, 10)
      val records = Record.search(GenericSearchCriteria(None, Some("1000"), None, None, None, None, CallDirection.All, None, Some("recording"), None), AdminRight())
      records.size shouldEqual 10
      records(0).recordId shouldEqual Some("gw1-0")
      records(9).recordId shouldEqual Some("gw1-9")

      paginator = Paginator(10, 10)
      val records2 = Record.search(GenericSearchCriteria(None, Some("1000"), None, None, None, None, CallDirection.All, None, Some("recording"), None), AdminRight())
      records2.size shouldEqual 10
      records2(0).recordId shouldEqual Some("gw1-10")
      records2(9).recordId shouldEqual Some("gw1-19")
    }

    "search records between two dates" in new clean {
      val startSearch = formatter.parseDateTime("2013-05-12 14:00:00")
      val endSearch = formatter.parseDateTime("2013-05-12 15:00:00")

      val avantStart1 = formatter.parseDateTime("2013-05-12 13:30:00")
      val avantStart2 = formatter.parseDateTime("2013-05-12 13:45:00")
      val entreStartEnd1 = formatter.parseDateTime("2013-05-12 14:15:00")
      val entreStartEnd2 = formatter.parseDateTime("2013-05-12 14:30:00")
      val apresEnd1 = formatter.parseDateTime("2013-05-12 15:15:00")
      val apresEnd2 = formatter.parseDateTime("2013-05-12 15:30:00")

      RecordHelper.insertRecord("11", "gw1-11", avantStart1, Some(avantStart2), Some("1000"), Some("2000"))
      RecordHelper.insertRecord("22", "gw1-22", avantStart1, Some(entreStartEnd1), Some("1000"), Some("2000"))
      RecordHelper.insertRecord("33", "gw1-33", entreStartEnd1, Some(entreStartEnd2), Some("1000"), Some("2000"))
      RecordHelper.insertRecord("44", "gw1-44", entreStartEnd2, Some(apresEnd1), Some("1000"), Some("2000"))
      RecordHelper.insertRecord("55", "gw1-55", apresEnd1, Some(apresEnd2), Some("1000"), Some("2000"))
      RecordHelper.insertRecord("66", "gw1-66", avantStart1, Some(apresEnd1), Some("1000"), Some("2000"))

      implicit var paginator = Paginator(0, 10)
      val records = Record.search(GenericSearchCriteria(None, None, None, None, Some(startSearch), Some(endSearch), CallDirection.All, None, Some("recording"), None), AdminRight())
      records.size shouldEqual 4
      val ids = records.flatMap(r => r.recordId)
      ids should contain("gw1-22")
      ids should contain("gw1-33")
      ids should contain("gw1-44")
      ids should contain("gw1-66")
    }

    "retrieve records with all its attached data" in new clean {
      val startDate = DateTime.now
      val endDate = startDate.plusMinutes(5)
      val id = "123.456"
      val data = Map("key1" -> "value1",
                     "key2" -> "value2")

      RecordHelper.insertRecord(id, s"gw1-$id", startDate, Some(endDate), Some("1001"), Some("2001"), data)

      val records = Record.search(GenericSearchCriteria(None, None, None, None, None, None, CallDirection.All, None, Some("recording"), None), AdminRight())

      records.size shouldEqual 1
      records(0).recordId shouldEqual Some(s"gw1-$id")
      records(0).attachedData shouldEqual Map("key1" -> "value1",
                                              "key2" -> "value2",
                                              "recording" -> s"gw1-$id")
    }

    "merge two list of records and transfers together" in new clean {
      val startDate: DateTime = DateTime.now
      val endDate: DateTime = startDate.plusMinutes(5)
      val records: List[Record] = List(Record("1", startDate, Some(new Duration(1)), "1000", "1001", List("agent1"), List("queue1")),
        Record("4", startDate, Some(new Duration(1)), "1000", "1001", List("agent1"), List("queue1")))
      val transfers: List[Record] = List(Record("3", startDate, Some(new Duration(1)), "1000", "1001", List("agent1"), List("queue1")))

      val result: List[Record] = Record.recordsWithTransfers(records, transfers)

      result(0).callDataId shouldEqual "4"
      result(1).callDataId shouldEqual "3"
      result(2).callDataId shouldEqual "1"
    }

    "retrieve files to delete with xivo recording expiration date from attached data" in new clean {
      val startDate: DateTime = DateTime.now
      val endDate: DateTime = startDate.plusMinutes(5)
      val weeksToKeepRecording = 2
      val refTime: Timestamp = new Timestamp(DateTime.now().minusWeeks(weeksToKeepRecording).getMillis)

      val id1 = "123.456"
      val id2 = "123.555"
      val purgeDate1 = "2016-08-27 10:28:10"
      val purgeDate2 = "2015-08-27 10:28:10"

      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate, Some(endDate), Some("1001"), Some("2001"), Map("xivo_recording_expiration" -> purgeDate1))
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate, Some(endDate), Some("1001"), Some("2001"), Map("xivo_recording_expiration" -> purgeDate2))

      val source = Record.getAttachedDataToPurgeAll("xivo_recording_expiration", "recording", refTime)
      val testSink = TestSink.probe[String]

      source.runWith(testSink)
        .request(3)
        .expectNext(s"gw1-$id1")
        .expectNext(s"gw1-$id2")
        .expectComplete()
    }

    "retrieve files to delete without xivo recording expiration date from attached data" in new clean {
      val weeksToKeepRecording = 2
      val refTime: Timestamp = new Timestamp(DateTime.now().minusWeeks(weeksToKeepRecording).getMillis)
      val startDate1 = DateTime.now.minusWeeks(2).minusDays(1)
      val endDate1 = startDate1.plusMinutes(5)

      val startDate2 = DateTime.now.minusWeeks(2).minusDays(1)
      val endDate2 = startDate2.plusMinutes(5)
      val id1 = "123.456"
      val id2 = "123.555"

      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate1, Some(endDate1), Some("1001"), Some("2001"))
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate2, Some(endDate2), Some("1001"), Some("2001"))

      val source = Record.getAttachedDataToPurgeAll("xivo_recording_expiration", "recording", refTime)
      val sinkUnderTest: Sink[String, Future[Int]] = Flow[String].toMat(Sink.fold(0)((counter, _) => counter + 1))(Keep.right)

      source.runWith(sinkUnderTest).futureValue shouldEqual 2
    }

    "retrieve files to delete with & without xivo recording expiration date from attached data" in new clean {
      val startDate: DateTime = DateTime.now
      val endDate: DateTime = startDate.plusMinutes(5)

      val pastDate: DateTime = startDate.minusYears(1)
      val futureDate: DateTime = startDate.plusYears(1)

      val weeksToKeepRecording = 2
      val refTime: Timestamp = new Timestamp(DateTime.now().minusWeeks(weeksToKeepRecording).getMillis)

      val id1 = "123.456"
      val id2 = "123.555"
      val id3 = "123.777"
      val id4 = "123.888"
      val purgeDate1 = "2016-08-27 10:28:10"
      val purgeDate2 = "2015-08-27 10:28:10"

      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate, Some(endDate), Some("1001"), Some("2001"), Map("xivo_recording_expiration" -> purgeDate1))
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate, Some(endDate), Some("1001"), Some("2001"), Map("xivo_recording_expiration" -> purgeDate2))
      RecordHelper.insertRecord(id3, s"gw1-$id3", pastDate, Some(pastDate.plusMinutes(5)), Some("1001"), Some("2001"))
      RecordHelper.insertRecord(id4, s"gw1-$id4", futureDate, Some(futureDate.plusMinutes(5)), Some("1001"), Some("2001"))

      val source = Record.getAttachedDataToPurgeAll("xivo_recording_expiration", "recording", refTime)
      val testSink = TestSink.probe[String]

      source.runWith(testSink)
        .requestNext(s"gw1-$id1")
        .requestNext(s"gw1-$id2")
        .requestNext(s"gw1-$id3")
        .expectComplete()
    }

    "not retrieve any future files with xivo recording expiration date from attached data" in new clean {
      val weeksToKeepRecording = 2
      val refTime: Timestamp = new Timestamp(DateTime.now().minusWeeks(weeksToKeepRecording).getMillis)
      val startDate: DateTime = DateTime.now
      val endDate: DateTime = startDate.plusMinutes(5)
      val id1 = "123.456"
      val id2 = "123.555"
      val purgeDate1 = "2059-05-21 10:28:10"
      val purgeDate2 = "2057-07-07 10:28:10"

      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate, Some(endDate), Some("1001"), Some("2001"), Map("xivo_recording_expiration" -> purgeDate1))
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate, Some(endDate), Some("1001"), Some("2001"), Map("xivo_recording_expiration" -> purgeDate2))

      val source = Record.getAttachedDataToPurgeAll("xivo_recording_expiration", "recording", refTime)
      val sinkUnderTest: Sink[String, Future[Int]] = Flow[String].toMat(Sink.fold(0)((counter, _) => counter + 1))(Keep.right)

      source.runWith(sinkUnderTest).futureValue shouldEqual 0
    }

    "not retrieve any future files without xivo recording expiration date from attached data" in new clean {
      val weeksToKeepRecording = 2
      val refTime: Timestamp = new Timestamp(DateTime.now().minusWeeks(weeksToKeepRecording).getMillis)
      val startDate1 = DateTime.now.plusWeeks(2)
      val endDate1 = startDate1.plusMinutes(5)

      val startDate2 = DateTime.now.plusWeeks(2)
      val endDate2 = startDate2.plusMinutes(5)
      val id1 = "123.456"
      val id2 = "123.555"

      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate1, Some(endDate1), Some("1001"), Some("2001"))
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate2, Some(endDate2), Some("1001"), Some("2001"))

      val source = Record.getAttachedDataToPurgeAll("xivo_recording_expiration", "recording", refTime)
      val sinkUnderTest: Sink[String, Future[Int]] = Flow[String].toMat(Sink.fold(0)((counter, _) => counter + 1))(Keep.right)

      source.runWith(sinkUnderTest).futureValue shouldEqual 0
    }

    "not retrieve already purge flagged files with & without xivo recording expiration date from attached data" in new clean {
      val startDate: DateTime = DateTime.now
      val endDate: DateTime = startDate.plusMinutes(5)

      val pastDate: DateTime = startDate.minusYears(1)
      val futureDate: DateTime = startDate.plusYears(1)

      val weeksToKeepRecording = 2
      val refTime: Timestamp = new Timestamp(DateTime.now().minusWeeks(weeksToKeepRecording).getMillis)

      val id1 = "123.456"
      val id2 = "123.555"
      val id3 = "123.777"
      val id4 = "123.888"
      val purgeDate1 = "2016-08-27 10:28:10"
      val purgeDate2 = "2015-08-27 10:28:10"

      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate, Some(endDate), Some("1001"), Some("2001"), Map("xivo_recording_expiration" -> purgeDate1))
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate, Some(endDate), Some("1001"), Some("2001"), Map("xivo_recording_expiration" -> purgeDate2))
      RecordHelper.insertRecord(id3, s"gw1-$id3", pastDate, Some(pastDate.plusMinutes(5)), Some("1001"), Some("2001"))
      RecordHelper.insertRecord(id4, s"gw1-$id4", futureDate, Some(futureDate.plusMinutes(5)), Some("1001"), Some("2001"))

      Record.setAttachedDataPurgeFlag(Seq(s"gw1-$id1", s"gw1-$id2", s"gw1-$id3", s"gw1-$id4")) shouldEqual 6

      val source = Record.getAttachedDataToPurgeAll("xivo_recording_expiration", "recording", refTime)
      val sinkUnderTest: Sink[String, Future[Int]] = Flow[String].toMat(Sink.fold(0)((counter, _) => counter + 1))(Keep.right)

      source.runWith(sinkUnderTest).futureValue shouldEqual 0
    }

    "not retrieve any files with xivo recording expiration date if date is incomplete" in new clean {
      val startDate: DateTime = DateTime.now
      val endDate: DateTime = startDate.plusMinutes(5)

      val pastDate: DateTime = startDate.minusYears(1)
      val futureDate: DateTime = startDate.plusYears(1)

      val weeksToKeepRecording = 2
      val refTime: Timestamp = new Timestamp(DateTime.now().minusWeeks(weeksToKeepRecording).getMillis)

      val id1 = "123.456"
      val purgeDate1 = "2016-08-"

      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate, Some(endDate), Some("1001"), Some("2001"), Map("xivo_recording_expiration" -> purgeDate1))

      val source = Record.getAttachedDataToPurgeAll("xivo_recording_expiration", "recording", refTime)
      val sinkUnderTest: Sink[String, Future[Int]] = Flow[String].toMat(Sink.fold(0)((counter, _) => counter + 1))(Keep.right)

      source.runWith(sinkUnderTest).futureValue shouldEqual 0
    }

    "set purged flag in attached data with xivo recording expiration" in new clean {
      val startDate1 = DateTime.now.plusWeeks(2)
      val endDate1 = startDate1.plusMinutes(5)

      val startDate2 = DateTime.now.plusWeeks(2)
      val endDate2 = startDate2.plusMinutes(5)
      val id1 = "123.456"
      val id2 = "123.555"
      val purgeDate1 = "2015-08-27 10:28:10"
      val purgeDate2 = "2016-08-27 10:28:10"

      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate2, Some(endDate2), Some("1001"), Some("2001"), Map("xivo_recording_expiration" -> purgeDate1))
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate2, Some(endDate2), Some("1001"), Some("2001"), Map("xivo_recording_expiration" -> purgeDate2))

      val records: List[Record] = Record.search(GenericSearchCriteria(None, Some("1001"), None, None, None, None, CallDirection.All, None, None, None), AdminRight())
      records.size shouldEqual 2

      val flaggedRecords: Int = Record.setAttachedDataPurgeFlag(Seq(s"gw1-$id1"))
      flaggedRecords shouldEqual 2

    }

    "set purged flag in attached data without xivo recording expiration" in new clean {
      val startDate1 = DateTime.now.plusWeeks(2)
      val endDate1 = startDate1.plusMinutes(5)

      val startDate2 = DateTime.now.plusWeeks(2)
      val endDate2 = startDate2.plusMinutes(5)
      val id1 = "123.456"
      val id2 = "123.555"

      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate1, Some(endDate1), Some("1001"), Some("2001"))
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate1, Some(endDate1), Some("1001"), Some("2001"))

      val records: List[Record] = Record.search(GenericSearchCriteria(None, Some("1001"), None, None, None, None, CallDirection.All, None, None, None), AdminRight())
      records.size shouldEqual 2

      val flaggedRecords: Int = Record.setAttachedDataPurgeFlag(Seq(s"gw1-$id1"))
      flaggedRecords shouldEqual 1

    }

    "set purged flag in attached data with & without xivo recording expiration" in new clean {
      val startDate1 = DateTime.now.plusWeeks(2)
      val endDate1 = startDate1.plusMinutes(5)

      val startDate2 = DateTime.now.plusWeeks(2)
      val endDate2 = startDate2.plusMinutes(5)
      val id1 = "123.456"
      val id2 = "123.555"
      val id3 = "123.777"
      val id4 = "123.888"
      val purgeDate1 = "2015-08-27 10:28:10"
      val purgeDate2 = "2016-08-27 10:28:10"

      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate1, Some(endDate1), Some("1001"), Some("2001"))
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate1, Some(endDate1), Some("1001"), Some("2001"))
      RecordHelper.insertRecord(id3, s"gw1-$id3", startDate2, Some(endDate2), Some("1001"), Some("2001"), Map("xivo_recording_expiration" -> purgeDate1))
      RecordHelper.insertRecord(id4, s"gw1-$id4", startDate2, Some(endDate2), Some("1001"), Some("2001"), Map("xivo_recording_expiration" -> purgeDate2))

      val records: List[Record] = Record.search(GenericSearchCriteria(None, Some("1001"), None, None, None, None, CallDirection.All, None, None, None), AdminRight())
      records.size shouldEqual 4

      val flaggedRecords: Int = Record.setAttachedDataPurgeFlag(Seq(s"gw1-$id1", s"gw1-$id3"))
      flaggedRecords shouldEqual 3

    }

    "give information that file was purged" in new clean {
      val startDate1 = DateTime.now.plusWeeks(2)
      val endDate1 = startDate1.plusMinutes(5)

      val id1 = "123.456"
      val purgeDate1 = "2015-08-27 10:28:10"

      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate1, Some(endDate1), Some("1001"), Some("2001"), Map("xivo_recording_expiration" -> purgeDate1))

      val flaggedRecords: Int = Record.setAttachedDataPurgeFlag(Seq(s"gw1-$id1"))
      flaggedRecords shouldEqual 2

      val recordsAfterFlagging: List[Record] = Record.search(GenericSearchCriteria(None, Some("1001"), None, None, None, None, CallDirection.All, None, None, None), AdminRight())

      recordsAfterFlagging.size shouldEqual 1
      recordsAfterFlagging(0).attachedData shouldEqual Map("recording" -> "was purged",
        "xivo_recording_expiration" -> "2015-08-27 10:28:10")
    }

    "search only incoming calls" in new clean {
      val startDate = DateTime.now
      val endDate = startDate.plusMinutes(5)
      val id1 = "123.456"
      val id2 = "123.457"

      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate, Some(endDate), Some("1001"), Some("2001"), direction=CallDirection.Incoming)
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate, Some(endDate), Some("1001"), Some("2001"), direction=CallDirection.Outgoing)

      val records = Record.search(GenericSearchCriteria(None, None, None, None, None, None, CallDirection.Incoming, None, Some("recording"), None), AdminRight())

      records.size shouldEqual 1
      records(0).recordId shouldEqual Some(s"gw1-$id1")
    }

    "search only outgoing calls" in new clean {
      val startDate = DateTime.now
      val endDate = startDate.plusMinutes(5)
      val id1 = "123.456"
      val id2 = "123.457"

      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate, Some(endDate), Some("1001"), Some("2001"), direction=CallDirection.Incoming)
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate, Some(endDate), Some("1001"), Some("2001"), direction=CallDirection.Outgoing)

      val records = Record.search(GenericSearchCriteria(None, None, None, None, None, None, CallDirection.Outgoing, None, Some("recording"), None), AdminRight())

      records.size shouldEqual 1
      records(0).recordId shouldEqual Some(s"gw1-$id2")
    }

    "search by substring in agent name, firstname and number" in new clean {
      val startDate = DateTime.now
      val endDate = startDate.plusMinutes(5)
      val id1 = "123.456"
      val id2 = "123.457"
      val id3 = "123.458"
      val id4 = "123.459"

      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate, Some(endDate), Some("1001"), Some("2001"), direction=CallDirection.Incoming)
      RecordHelper.insertCallOnQueue(id1, "3000", "the_queue")
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate, Some(endDate), Some("1001"), Some("2001"), direction=CallDirection.Outgoing)
      RecordHelper.insertCallOnQueue(id2, "3001", "the_queue")
      RecordHelper.insertRecord(id3, s"gw1-$id3", startDate, Some(endDate), Some("1001"), Some("2001"), direction=CallDirection.Incoming, dstAgent=Some("3002"))
      RecordHelper.insertRecord(id4, s"gw1-$id4", startDate, Some(endDate), Some("1001"), Some("2001"), direction=CallDirection.Outgoing, srcAgent=Some("3003"))

      Record.search(GenericSearchCriteria(None, None, Some("300"), None, None, None, CallDirection.All, None, Some("recording"), None), AdminRight())
        .flatMap(r => r.recordId).sorted shouldEqual List(s"gw1-$id1", s"gw1-$id2", s"gw1-$id3", s"gw1-$id4").sorted

      Record.search(GenericSearchCriteria(None, None, Some("ierr"), None, None, None, CallDirection.All, None, Some("recording"), None), AdminRight())
        .flatMap(r => r.recordId).sorted shouldEqual List(s"gw1-$id1")

      Record.search(GenericSearchCriteria(None, None, Some("dupo"), None, None, None, CallDirection.All, None, Some("recording"), None), AdminRight())
        .flatMap(r => r.recordId).sorted shouldEqual List(s"gw1-$id2")

      Record.search(GenericSearchCriteria(None, None, Some("jol"), None, None, None, CallDirection.All, None, Some("recording"), None), AdminRight())
        .flatMap(r => r.recordId).sorted shouldEqual List(s"gw1-$id4")

      Record.search(GenericSearchCriteria(None, None, Some("3002"), None, None, None, CallDirection.All, None, Some("recording"), None), AdminRight())
        .flatMap(r => r.recordId).sorted shouldEqual List(s"gw1-$id3")
    }

    "search by callid" in new clean {
      RecordHelper.insertRecord("123", "gw1-123", DateTime.now, Some(DateTime.now.plusMinutes(5)), Some("1001"), Some("2001"), direction=CallDirection.Incoming)
      RecordHelper.insertCallOnQueue("123", "3000", "the_queue")

      Record.searchByCallid("123", AdminRight()).flatMap(r => r.recordId) shouldEqual List("gw1-123")
    }

    "tell if a globalId exists" in new clean {
      RecordHelper.insertRecord("123", "gw1-123", DateTime.now, Some(DateTime.now.plusMinutes(5)), Some("1001"), Some("2001"), direction=CallDirection.Incoming)

      Record.globalIdExists("gw1-123") should beTrue
      Record.globalIdExists("gw1-456") should beFalse
    }

    "return the call uniqueid for the given globalId" in new clean {
      RecordHelper.insertRecord("123", "gw1-123", DateTime.now, Some(DateTime.now.plusMinutes(5)), Some("1001"), Some("2001"), direction=CallDirection.Incoming)

      Record.callidByGlobalId("gw1-123") shouldEqual Some("123")
      Record.callidByGlobalId("gw1-456") shouldEqual None
    }

    "search by substring in queue name and number" in new clean {
      val startDate = DateTime.now
      val endDate = startDate.plusMinutes(5)
      val id1 = "123.456"
      val id2 = "123.457"

      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate, Some(endDate), Some("1001"), Some("2001"), direction=CallDirection.Incoming)
      RecordHelper.insertCallOnQueue(id1, "3000", "thequeue")
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate, Some(endDate), Some("1001"), Some("2001"), direction=CallDirection.Outgoing)
      RecordHelper.insertCallOnQueue(id2, "3001", "une_file")

      Record.search(GenericSearchCriteria(None, None, Some("300"), None, None, None, CallDirection.All, None, Some("recording"), None), AdminRight())
        .flatMap(r => r.recordId).sorted shouldEqual List(s"gw1-$id1", s"gw1-$id2").sorted

      Record.search(GenericSearchCriteria(None, None, None, Some("23"), None, None, CallDirection.All, None, Some("recording"), None), AdminRight())
        .flatMap(r => r.recordId).sorted shouldEqual List(s"gw1-$id2")

      Record.search(GenericSearchCriteria(None, None, None, Some("hequ"), None, None, CallDirection.All, None, Some("recording"), None), AdminRight())
        .flatMap(r => r.recordId).sorted shouldEqual List(s"gw1-$id1")
    }

    "use the agent number provided by call_data if there is no call_on_queue" in new clean {
      val startDate = DateTime.now
      val endDate = startDate.plusMinutes(5)
      val id1 = "123.456"
      val id2 = "123.457"
      val id3 = "123.458"

      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate, Some(endDate), Some("1001"), Some("2001"), direction=CallDirection.Incoming)
      RecordHelper.insertCallOnQueue(id1, "3000", "thequeue")
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate, Some(endDate), Some("3000"), Some("1001"), direction=CallDirection.Outgoing, srcAgent = Some("3000"))
      RecordHelper.insertRecord(id3, s"gw1-$id3", startDate, Some(endDate), Some("1001"), Some("3000"), direction=CallDirection.Incoming, dstAgent = Some("3000"))

      Record.search(GenericSearchCriteria(None, None, None, None, Some(DateTime.now()), None, CallDirection.All, None, Some("recording"), None), AdminRight()).map(r => r.agent) shouldEqual
        List(List("Pierre Martin (3000)"), List("Pierre Martin (3000)"), List("Pierre Martin (3000)"))
    }

    "search the calls with an attached data containing the given key" in new clean {
      val call_data_uniqueid = "123"
      val callDataId = RecordHelper.insertCallData(call_data_uniqueid, DateTime.now.toDate, Some(DateTime.now.plusMinutes(5).toDate), Some("1001"), Some("2001"), direction=CallDirection.Incoming)
      RecordHelper.insertAttachedData(callDataId.get, "the_key", "the_value")
      RecordHelper.insertCallData("456", DateTime.now.toDate, Some(DateTime.now.plusMinutes(5).toDate), Some("2001"), Some("3001"), direction=CallDirection.Incoming)

      val sc = GenericSearchCriteria(None, None, None, None, Some(DateTime.now()), None, CallDirection.All, None, Some("the_key"), None)
      Record.search(sc, AdminRight()).map(_.callDataId) shouldEqual  List(call_data_uniqueid)
    }

    "search the calls with an attached data containing the given key - value pair" in new clean {
      val call_data_uniqueid = "123"
      val callDataId1 = RecordHelper.insertCallData(call_data_uniqueid, DateTime.now.toDate, Some(DateTime.now.plusMinutes(5).toDate), Some("1001"), Some("2001"), direction=CallDirection.Incoming)
      RecordHelper.insertAttachedData(callDataId1.get, "the_key", "the_value")
      val callDataId2 = RecordHelper.insertCallData("456", DateTime.now.toDate, Some(DateTime.now.plusMinutes(5).toDate), Some("2001"), Some("3001"), direction=CallDirection.Incoming)
      RecordHelper.insertAttachedData(callDataId2.get, "the_key", "other_value")

      val sc = GenericSearchCriteria(None, None, None, None, Some(DateTime.now()), None, CallDirection.All, None, Some("the_key"), Some("the_value"))
      Record.search(sc, AdminRight()).map(_.callDataId) shouldEqual  List(call_data_uniqueid)
    }

    "return only one row if there are several attached data" in new clean {
      val call_data_uniqueid = "123"
      val callDataId = RecordHelper.insertCallData(call_data_uniqueid, DateTime.now.toDate, Some(DateTime.now.plusMinutes(5).toDate), Some("1001"), Some("2001"), direction=CallDirection.Incoming)
      RecordHelper.insertAttachedData(callDataId.get, "the_key", "the_value")
      RecordHelper.insertAttachedData(callDataId.get, "other_key", "other_value")

      val sc = GenericSearchCriteria(None, None, None, None, Some(DateTime.now()), None, CallDirection.All, None, None, None)
      Record.search(sc, AdminRight()).map(_.callDataId) shouldEqual  List(call_data_uniqueid)
    }
    
    "filter the rows by agent group when the user has limited rights" in new clean {
      val startDate = DateTime.now
      val endDate = startDate.plusMinutes(5)
      val id1 = "123.456"
      val id2 = "123.457"
      val id3 = "123.458"
      val right = new LimitedRight(List(), List(groupId1), List()) {}

      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate, Some(endDate), Some("1001"), Some("2001"), direction=CallDirection.Incoming)
      RecordHelper.insertCallOnQueue(id1, "3000", "thequeue")
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate, Some(endDate), Some("3000"), Some("1001"), direction=CallDirection.Outgoing, srcAgent = Some("3001"))
      RecordHelper.insertRecord(id3, s"gw1-$id3", startDate, Some(endDate), Some("1001"), Some("3000"), direction=CallDirection.Incoming, dstAgent = Some("3002"))

      Record.search(GenericSearchCriteria(None, None, None, None, Some(DateTime.now()), None, CallDirection.All, None, None, None), right).flatMap(r => r.recordId).sorted shouldEqual
        List(s"gw1-$id1", s"gw1-$id2").sorted
    }

    "filter the rows by queue when the user is Supervisor" in new clean {
      val startDate = DateTime.now
      val endDate = startDate.plusMinutes(5)
      val id1 = "123.456"
      val id2 = "123.457"
      val id3 = "123.458"
      val right = new LimitedRight(List(queueId1), List(), List()){}

      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate, Some(endDate), Some("1001"), Some("2001"), direction=CallDirection.Incoming)
      RecordHelper.insertCallOnQueue(id1, "3000", "thequeue")
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate, Some(endDate), Some("3000"), Some("1001"), direction=CallDirection.Outgoing, srcAgent = Some("3001"))
      RecordHelper.insertCallOnQueue(id2, "3000", "une_file")
      RecordHelper.insertRecord(id3, s"gw1-$id3", startDate, Some(endDate), Some("1001"), Some("3000"), direction=CallDirection.Incoming, dstAgent = Some("3002"))

      Record.search(GenericSearchCriteria(None, None, None, None, Some(DateTime.now()), None, CallDirection.All, None, None, None), right).flatMap(r => r.recordId) shouldEqual
        List(s"gw1-$id1")
    }

    "filter the rows by incall when the user is Supervisor" in new clean {
      val startDate = DateTime.now
      val endDate = startDate.plusMinutes(5)
      val id1 = "123.456"
      val id2 = "123.457"
      val id3 = "123.458"
      val right = new LimitedRight(List(), List(), List(incallId1)){}

      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate, Some(endDate), Some("1001"), Some("5300"), direction=CallDirection.Incoming)
      RecordHelper.insertCallOnQueue(id1, "3000", "thequeue")
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate, Some(endDate), Some("3000"), Some("5300"), direction=CallDirection.Outgoing, srcAgent = Some("3001"))
      RecordHelper.insertCallOnQueue(id2, "3000", "une_file")
      RecordHelper.insertRecord(id3, s"gw1-$id3", startDate, Some(endDate), Some("1001"), Some("5301"), direction=CallDirection.Incoming, dstAgent = Some("3002"))

      Record.search(GenericSearchCriteria(None, None, None, None, Some(DateTime.now()), None, CallDirection.All, None, None, None), right).flatMap(r => r.recordId).sorted shouldEqual
        List(s"gw1-$id1", s"gw1-$id2").sorted
    }

    "return nothing if the Supervisor has no rights" in new clean {
      val startDate = DateTime.now
      val endDate = startDate.plusMinutes(5)
      val id1 = "123.456"
      val id2 = "123.457"
      val id3 = "123.458"
      val right = new LimitedRight(List(), List(), List()){}

      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate, Some(endDate), Some("1001"), Some("5300"), direction=CallDirection.Incoming)
      RecordHelper.insertCallOnQueue(id1, "3000", "thequeue")
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate, Some(endDate), Some("3000"), Some("5300"), direction=CallDirection.Outgoing, srcAgent = Some("3001"))
      RecordHelper.insertCallOnQueue(id2, "3000", "une_file")
      RecordHelper.insertRecord(id3, s"gw1-$id3", startDate, Some(endDate), Some("1001"), Some("5301"), direction=CallDirection.Incoming, dstAgent = Some("3002"))

      Record.search(GenericSearchCriteria(None, None, None, None, Some(DateTime.now()), None, CallDirection.All, None, None, None), right) shouldEqual List()
    }

    "filter nothing for an Administrator" in new clean {
      val startDate = DateTime.now
      val endDate = startDate.plusMinutes(5)
      val id1 = "123.456"
      val id2 = "123.457"
      val id3 = "123.458"
      val right = AdminRight()

      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate, Some(endDate), Some("1001"), Some("5300"), direction=CallDirection.Incoming)
      RecordHelper.insertCallOnQueue(id1, "3000", "thequeue")
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate, Some(endDate), Some("3000"), Some("5300"), direction=CallDirection.Outgoing, srcAgent = Some("3001"))
      RecordHelper.insertCallOnQueue(id2, "3000", "une_file")
      RecordHelper.insertRecord(id3, s"gw1-$id3", startDate, Some(endDate), Some("1001"), Some("5301"), direction=CallDirection.Incoming, dstAgent = Some("3002"))

      Record.search(GenericSearchCriteria(None, None, None, None, Some(DateTime.now()), None, CallDirection.All, None, None, None), right)
        .flatMap(r => r.recordId) should containAllOf(List(s"gw1-$id3", s"gw1-$id2", s"gw1-$id1"))
    }
    "search records for only part of caller" in new clean {
      val startDate = DateTime.now
      val endDate = startDate.plusMinutes(5)
      val id1 = "123.456"
      val id2 = "456.789"
      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate, Some(endDate), Some("1000"), None)
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate, Some(endDate), Some("1001"), Some("2001"))

      Record.search(GenericSearchCriteria(None, Some("1%")), AdminRight())
        .flatMap(r => r.recordId) should containAllOf(List(s"gw1-$id1", s"gw1-$id2"))
    }

    "search records for only part of callee" in new clean {
      val startDate = DateTime.now
      val endDate = startDate.plusMinutes(5)
      val id1 = "123.456"
      val id2 = "456.789"
      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate, Some(endDate), Some("1000"), Some("1001"))
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate, Some(endDate), Some("1001"), Some("2001"))

      Record.search(GenericSearchCriteria(Some("%01")), AdminRight())
        .flatMap(r => r.recordId) should containAllOf(List(s"gw1-$id1", s"gw1-$id2"))
    }

    "search the calls with only part of key value in attached_data" in new clean {
      val call_data_uniqueid = "123"
      val callDataId = RecordHelper.insertCallData(call_data_uniqueid, DateTime.now.toDate, Some(DateTime.now.plusMinutes(5).toDate), Some("1001"), Some("2001"), direction=CallDirection.Incoming)
      RecordHelper.insertAttachedData(callDataId.get, "the_key", "the_value")
      RecordHelper.insertCallData("456", DateTime.now.toDate, Some(DateTime.now.plusMinutes(5).toDate), Some("2001"), Some("3001"), direction=CallDirection.Incoming)

      val sc = GenericSearchCriteria(None, None, None, None, Some(DateTime.now()), None, CallDirection.All, None, Some("%_%"), Some("%_%"))
      Record.search(sc, AdminRight()).map(_.callDataId) shouldEqual  List(call_data_uniqueid)
    }
  }
}
