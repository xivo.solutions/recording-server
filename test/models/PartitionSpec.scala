package models

import play.api.test.PlaySpecification

class PartitionSpec extends PlaySpecification {

  "The Partition singleton" should {
    "return the total and free size of the given folder" in {
      val res = Partition.forDirectory("/tmp")
      res.name shouldEqual ""
      res.mountPoint shouldEqual "/tmp"
      res.freeSpace should be greaterThanOrEqualTo 0
      res.totalSpace should be greaterThan res.freeSpace
    }
  }
}
