package models

import helpers.{AppTest, RecordHelper}
import org.joda.time.Duration
import org.scalatestplus.mockito.MockitoSugar
import org.specs2.mutable.Before
import play.api.db.Database
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.{JsArray, Json}

class CallDetailSpec extends AppTest with MockitoSugar {
  val dbStatsMock: Database = mock[Database]

  override def fakeApplication() = new GuiceApplicationBuilder()
    .configure(testConfig)
    .overrides(bind[Database].qualifiedWith("stats").to(dbStatsMock))
    .build()

  List("agentfeatures", "agentgroup", "extensions", "queuefeatures", "userfeatures").foreach(DBUtil.cleanTable)

  implicit val paginator: Paginator = Paginator(0, 20)

  val tables = List("attached_data", "call_data", "call_on_queue", "call_element", "userfeatures", "extensions")

  class Helper extends Before {

    def before = {
      for (table <- tables)
        DBUtil.cleanTable(table)
    }

    val callDetailDao = app.injector.instanceOf(classOf[CallDetailDao])
  }

  "history by interface" should {

    "all interface should return SIP and PJSIP without and with _w interfaces for a SIP interface" in new Helper {
      val interface = "SIP/abcde"
      val expected = List("SIP/abcde", "SIP/abcde_w", "PJSIP/abcde", "PJSIP/abcde_w")

      callDetailDao.all_interface_names(interface) shouldEqual expected
    }

    "all interface should return SIP and PJSIP without and with _w interfaces for a PJSIP interface" in new Helper {
      val interface = "PJSIP/abcde"
      val expected = List("SIP/abcde", "SIP/abcde_w", "PJSIP/abcde", "PJSIP/abcde_w")

      callDetailDao.all_interface_names(interface) shouldEqual expected
    }

    "all interface should return it without and with _w for non SIP/PJSIP interface" in new Helper {
      val interface = "Local/abcde"
      val expected = List("Local/abcde", "Local/abcde_w")

      callDetailDao.all_interface_names(interface) shouldEqual expected
    }

    "search the call history by peer" in new Helper {
      val interface = "SIP/jnfkoz"
      val id1 = RecordHelper.insertCallData("123456.789", formatter.parseDateTime("2014-01-01 08:00:00").toDate,
        Some(formatter.parseDateTime("2014-01-01 08:12:00").toDate), srcNum = Some("1000"), dstNum = None)
      RecordHelper.insertCallElement(id1.get, formatter.parseDateTime("2014-01-01 08:00:01"), Some(formatter.parseDateTime("2014-01-01 08:00:31")), interface)
      RecordHelper.insertCallElement(id1.get, formatter.parseDateTime("2014-01-01 08:00:31"), Some(formatter.parseDateTime("2014-01-01 08:12:00")), "SIP/oabfde")
      RecordHelper.insertCallData("123456.795", formatter.parseDateTime("2014-01-01 09:00:00").toDate,
        Some(formatter.parseDateTime("2014-01-01 09:12:00").toDate), srcInterface = Some(interface), srcNum = None, dstNum = Some("2001"))
      RecordHelper.insertCallData("123456.784", formatter.parseDateTime("2014-01-01 09:01:00").toDate,
        Some(formatter.parseDateTime("2014-01-01 09:08:00").toDate), srcInterface = Some("SIP/mljdad"))

      callDetailDao.historyByInterface(interface, 10) shouldEqual List(
        CallDetail(formatter.parseDateTime("2014-01-01 09:00:00"), Some(new Duration(formatter.parseDateTime("2014-01-01 09:00:00"),
          formatter.parseDateTime("2014-01-01 09:12:00"))), None, Some("2001"), CallStatus.Emitted),
        CallDetail(formatter.parseDateTime("2014-01-01 08:00:01"), Some(new Duration(formatter.parseDateTime("2014-01-01 08:00:01"),
          formatter.parseDateTime("2014-01-01 08:00:31"))), Some("1000"), None, CallStatus.Missed)
      )
    }

    "limit the search to the given size" in new Helper {
      val interface = "SIP/jnfkoz"
      RecordHelper.insertCallData("123456.795", formatter.parseDateTime("2014-01-01 09:00:00").toDate,
        Some(formatter.parseDateTime("2014-01-01 09:12:00").toDate), srcInterface = Some(interface), srcNum = Some("1001"), dstNum = Some("2001"))
      RecordHelper.insertCallData("123456.796", formatter.parseDateTime("2014-01-01 09:30:00").toDate,
        Some(formatter.parseDateTime("2014-01-01 09:45:00").toDate), srcInterface = Some(interface), srcNum = Some("1001"), dstNum = Some("2001"))

      callDetailDao.historyByInterface(interface, 1) shouldEqual List(
        CallDetail(formatter.parseDateTime("2014-01-01 09:30:00"), Some(new Duration(formatter.parseDateTime("2014-01-01 09:30:00"),
          formatter.parseDateTime("2014-01-01 09:45:00"))), Some("1001"), Some("2001"), CallStatus.Emitted))
    }

    "retrieve first & last names of caller and callee" in new Helper {
      val interface = "SIP/jnfkoz"

      val srcId = RecordHelper.insertUser("James", "Bond")
      val dstId = RecordHelper.insertUser("Jason", "Bourne")

      RecordHelper.insertExtension("1001", "user", srcId.get.toString)
      RecordHelper.insertExtension("2001", "user", dstId.get.toString)

      RecordHelper.insertCallData("123456.796", formatter.parseDateTime("2014-01-01 09:30:00").toDate,
        Some(formatter.parseDateTime("2014-01-01 09:45:00").toDate), srcInterface = Some(interface), srcNum = Some("1001"), dstNum = Some("2001"))

      callDetailDao.historyByInterface(interface, 1) shouldEqual List(
        CallDetail(formatter.parseDateTime("2014-01-01 09:30:00"), Some(new Duration(formatter.parseDateTime("2014-01-01 09:30:00"),
          formatter.parseDateTime("2014-01-01 09:45:00"))), Some("1001"), Some("2001"), CallStatus.Emitted, Some("James"), Some("Bond"), Some("Jason"), Some("Bourne")))
    }

    "retrieve first & last names of caller and callee with PJSIP" in new Helper {
      val interface = "SIP/jnfkoz"

      val srcId = RecordHelper.insertUser("James", "Bond")
      val dstId = RecordHelper.insertUser("Jason", "Bourne")

      RecordHelper.insertExtension("1001", "user", srcId.get.toString)
      RecordHelper.insertExtension("2001", "user", dstId.get.toString)

      RecordHelper.insertCallData("123456.796", formatter.parseDateTime("2014-01-01 09:30:00").toDate,
        Some(formatter.parseDateTime("2014-01-01 09:45:00").toDate), srcInterface = Some(interface), srcNum = Some("1001"), dstNum = Some("2001"))

      callDetailDao.historyByInterface("PJ" + interface, 1) shouldEqual List(
        CallDetail(formatter.parseDateTime("2014-01-01 09:30:00"), Some(new Duration(formatter.parseDateTime("2014-01-01 09:30:00"),
          formatter.parseDateTime("2014-01-01 09:45:00"))), Some("1001"), Some("2001"), CallStatus.Emitted, Some("James"), Some("Bond"), Some("Jason"), Some("Bourne")))
    }

    "search the call history by peer with peer_w to handle unique account use cases" in new Helper {
      val interface = "SIP/jnfkoz"
      val id1 = RecordHelper.insertCallData("123456.789", formatter.parseDateTime("2014-01-01 08:00:00").toDate,
        Some(formatter.parseDateTime("2014-01-01 08:12:00").toDate), srcNum = Some("1000"), dstNum = None)
      RecordHelper.insertCallElement(id1.get, formatter.parseDateTime("2014-01-01 08:00:01"), Some(formatter.parseDateTime("2014-01-01 08:00:31")), interface)

      RecordHelper.insertCallData("123456.795", formatter.parseDateTime("2014-01-01 09:00:00").toDate,
        Some(formatter.parseDateTime("2014-01-01 09:12:00").toDate), srcInterface = Some(interface), srcNum = None, dstNum = Some("2001"))

      RecordHelper.insertCallData("123456.796", formatter.parseDateTime("2014-01-01 09:15:00").toDate,

        Some(formatter.parseDateTime("2014-01-01 09:16:00").toDate), srcInterface = Some(interface+"_w"), srcNum = None, dstNum = Some("3001"))

      callDetailDao.historyByInterface(interface, 10) shouldEqual List(
        CallDetail(formatter.parseDateTime("2014-01-01 09:15:00"), Some(new Duration(formatter.parseDateTime("2014-01-01 09:15:00"),
          formatter.parseDateTime("2014-01-01 09:16:00"))), None, Some("3001"), CallStatus.Emitted),
        CallDetail(formatter.parseDateTime("2014-01-01 09:00:00"), Some(new Duration(formatter.parseDateTime("2014-01-01 09:00:00"),
          formatter.parseDateTime("2014-01-01 09:12:00"))), None, Some("2001"), CallStatus.Emitted),
        CallDetail(formatter.parseDateTime("2014-01-01 08:00:01"), Some(new Duration(formatter.parseDateTime("2014-01-01 08:00:01"),
          formatter.parseDateTime("2014-01-01 08:00:31"))), Some("1000"), None, CallStatus.Missed)
      )
    }

    "retrieve the call history by interface without duplicates in case it is a mobile app user" in new Helper {
      val myInterface = "PJSIP/abbeqbc"

      val idMissedDuplicate: Option[Long] = RecordHelper.insertCallData("1", formatter.parseDateTime("2014-01-01 09:00:00").toDate,
        Some(formatter.parseDateTime("2014-01-01 09:00:30").toDate), srcInterface = Some(myInterface), srcNum = Some("2001"), dstNum = Some("2002"))
      RecordHelper.insertCallElement(idMissedDuplicate.get, formatter.parseDateTime("2014-01-01 09:00:00"), Some(formatter.parseDateTime("2014-01-01 09:00:30")), myInterface)
      RecordHelper.insertCallElement(idMissedDuplicate.get, formatter.parseDateTime("2014-01-01 09:00:00"), Some(formatter.parseDateTime("2014-01-01 09:00:30")), myInterface)

      val idEmittedCall: Option[Long] = RecordHelper.insertCallData("2", formatter.parseDateTime("2014-01-01 09:13:00").toDate,
        Some(formatter.parseDateTime("2014-01-01 09:25:00").toDate), srcInterface = Some(myInterface), direction = CallDirection.Outgoing, srcNum = Some("2001"), dstNum = Some("2002"))
      RecordHelper.insertCallElement(idEmittedCall.get, formatter.parseDateTime("2014-01-01 09:13:00"), Some(formatter.parseDateTime("2014-01-01 09:25:00")), "SIP/oabfde")
      
      val idAnsweredDuplicate: Option[Long] = RecordHelper.insertCallData("3", formatter.parseDateTime("2014-01-01 09:26:00").toDate,
        Some(formatter.parseDateTime("2014-01-01 09:38:00").toDate), srcInterface = Some("PJSIP/znnrsnv"), srcNum = Some("2002"), dstNum = Some("2001"), direction = CallDirection.Incoming)
      RecordHelper.insertCallElement(idAnsweredDuplicate.get, formatter.parseDateTime("2014-01-01 09:26:00"), Some(formatter.parseDateTime("2014-01-01 09:38:00")), myInterface, Some(formatter.parseDateTime("2014-01-01 09:26:10")))
      RecordHelper.insertCallElement(idAnsweredDuplicate.get, formatter.parseDateTime("2014-01-01 09:26:00"), Some(formatter.parseDateTime("2014-01-01 09:26:30")), myInterface)

      callDetailDao.historyByInterface(myInterface, 10) shouldEqual List(
        CallDetail(formatter.parseDateTime("2014-01-01 09:26:00"), Some(new Duration(formatter.parseDateTime("2014-01-01 09:26:00"),
          formatter.parseDateTime("2014-01-01 09:38:00"))), Some("2002"), Some("2001"), CallStatus.Answered),
        CallDetail(formatter.parseDateTime("2014-01-01 09:13:00"), Some(new Duration(formatter.parseDateTime("2014-01-01 09:13:00"),
          formatter.parseDateTime("2014-01-01 09:25:00"))), Some("2001"), Some("2002"), CallStatus.Emitted),
        CallDetail(formatter.parseDateTime("2014-01-01 09:00:00"), Some(new Duration(formatter.parseDateTime("2014-01-01 09:00:00"),
          formatter.parseDateTime("2014-01-01 09:00:30"))), Some("2001"), Some("2002"), CallStatus.Missed),
      )
    }
  }

  "history by date" should {
    "retrieve the calls of the last x days" in new Helper {
      val interface = "SIP/fndsjl"

      val srcId: Option[Long] = RecordHelper.insertUser("James", "Bond")
      val dstId: Option[Long] = RecordHelper.insertUser("Jason", "Bourne")

      RecordHelper.insertExtension("1000", "user", srcId.get.toString)
      RecordHelper.insertExtension("2000", "user", dstId.get.toString)

      val id1: Option[Long] = RecordHelper.insertCallData("1", formatter.parseDateTime("2018-05-30 18:00:00").toDate,
        Some(formatter.parseDateTime("2018-05-30 18:05:00").toDate), Some("1000"), Some("2000"))
      RecordHelper.insertCallElement(id1.get, formatter.parseDateTime("2018-05-30 18:00:00"), Some(formatter.parseDateTime("2018-05-30 18:05:00")), "SIP/fndsjl")

      val id2: Option[Long] = RecordHelper.insertCallData("2", formatter.parseDateTime("2018-06-23 19:00:00").toDate,
        Some(formatter.parseDateTime("2018-06-23 19:05:00").toDate), Some("1000"), Some("2000"))
      RecordHelper.insertCallElement(id2.get, formatter.parseDateTime("2018-06-23 19:00:00"), Some(formatter.parseDateTime("2018-06-23 19:05:00")), "SIP/fndsjl")

      val id3: Option[Long] = RecordHelper.insertCallData("3", formatter.parseDateTime("2018-06-29 17:00:00").toDate,
        Some(formatter.parseDateTime("2018-06-29 17:05:00").toDate), Some("1000"), Some("2000"))
      RecordHelper.insertCallElement(id3.get, formatter.parseDateTime("2018-06-29 17:00:00"), Some(formatter.parseDateTime("2018-06-29 17:05:00")), "SIP/fndsjl")

      val id4: Option[Long] = RecordHelper.insertCallData("4", formatter.parseDateTime("2018-06-30 12:00:00").toDate,
        Some(formatter.parseDateTime("2018-06-30 12:05:00").toDate), Some("1000"), Some("2000"))
      RecordHelper.insertCallElement(id4.get, formatter.parseDateTime("2018-06-30 12:00:00"), Some(formatter.parseDateTime("2018-06-30 12:05:00")), "SIP/fndsjl")

      callDetailDao.historyByDate(interface, "2018-06-25 18:00:00") shouldEqual List(
        CallDetail(formatter.parseDateTime("2018-06-30 12:00:00"), Some(new Duration(300 * 1000)), Some("1000"), Some("2000"), CallStatus.Missed, Some("James"), Some("Bond"), Some("Jason"), Some("Bourne")),
        CallDetail(formatter.parseDateTime("2018-06-29 17:00:00"), Some(new Duration(300 * 1000)), Some("1000"), Some("2000"), CallStatus.Missed, Some("James"), Some("Bond"), Some("Jason"), Some("Bourne"))
      )
    }

    "search the call history by last x days with peer_w to handle unique account use cases" in new Helper {
      val interface = "SIP/fndsjl"

      val srcId: Option[Long] = RecordHelper.insertUser("James", "Bond")
      val dstId: Option[Long] = RecordHelper.insertUser("Jason", "Bourne")

      RecordHelper.insertExtension("1000", "user", srcId.get.toString)
      RecordHelper.insertExtension("2000", "user", dstId.get.toString)

      val id3: Option[Long] = RecordHelper.insertCallData("1", formatter.parseDateTime("2018-06-29 17:00:00").toDate,
        Some(formatter.parseDateTime("2018-06-29 17:05:00").toDate),Some("1000"), Some("2000"))
      RecordHelper.insertCallElement(id3.get, formatter.parseDateTime("2018-06-29 17:00:00"), Some(formatter.parseDateTime("2018-06-29 17:05:00")), interface+"_w")

      val id4: Option[Long] = RecordHelper.insertCallData("2", formatter.parseDateTime("2018-06-30 12:00:00").toDate,
        Some(formatter.parseDateTime("2018-06-30 12:05:00").toDate),Some("1000"), Some("2000"))
      RecordHelper.insertCallElement(id4.get, formatter.parseDateTime("2018-06-30 12:00:00"), Some(formatter.parseDateTime("2018-06-30 12:05:00")), interface)

      callDetailDao.historyByDate(interface, "2018-06-25 18:00:00") shouldEqual List(
        CallDetail(formatter.parseDateTime("2018-06-30 12:00:00"), Some(new Duration(300 * 1000)), Some("1000"), Some("2000"), CallStatus.Missed, Some("James"), Some("Bond"), Some("Jason"), Some("Bourne")),
        CallDetail(formatter.parseDateTime("2018-06-29 17:00:00"), Some(new Duration(300 * 1000)), Some("1000"), Some("2000"), CallStatus.Missed, Some("James"), Some("Bond"), Some("Jason"), Some("Bourne"))
      )
    }

    "retrieve the call history by date without duplicates in case it is a mobile app user" in new Helper {
      val myInterface = "PJSIP/abbeqbc"

      val idMissedDuplicate: Option[Long] = RecordHelper.insertCallData("1", formatter.parseDateTime("2014-01-01 09:00:00").toDate,
        Some(formatter.parseDateTime("2014-01-01 09:00:30").toDate), srcInterface = Some(myInterface), srcNum = Some("2001"), dstNum = Some("2002"))
      RecordHelper.insertCallElement(idMissedDuplicate.get, formatter.parseDateTime("2014-01-01 09:00:00"), Some(formatter.parseDateTime("2014-01-01 09:00:30")), myInterface)
      RecordHelper.insertCallElement(idMissedDuplicate.get, formatter.parseDateTime("2014-01-01 09:00:00"), Some(formatter.parseDateTime("2014-01-01 09:00:30")), myInterface)

      val idEmittedCall: Option[Long] = RecordHelper.insertCallData("2", formatter.parseDateTime("2014-01-01 09:13:00").toDate,
        Some(formatter.parseDateTime("2014-01-01 09:25:00").toDate), srcInterface = Some(myInterface), direction = CallDirection.Outgoing, srcNum = Some("2001"), dstNum = Some("2002"))
      RecordHelper.insertCallElement(idEmittedCall.get, formatter.parseDateTime("2014-01-01 09:13:00"), Some(formatter.parseDateTime("2014-01-01 09:25:00")), "SIP/oabfde")
      
      val idAnsweredDuplicate: Option[Long] = RecordHelper.insertCallData("3", formatter.parseDateTime("2014-01-01 09:26:00").toDate,
        Some(formatter.parseDateTime("2014-01-01 09:38:00").toDate), srcInterface = Some("PJSIP/znnrsnv"), srcNum = Some("2002"), dstNum = Some("2001"), direction = CallDirection.Incoming)
      RecordHelper.insertCallElement(idAnsweredDuplicate.get, formatter.parseDateTime("2014-01-01 09:26:00"), Some(formatter.parseDateTime("2014-01-01 09:38:00")), myInterface, Some(formatter.parseDateTime("2014-01-01 09:26:10")))
      RecordHelper.insertCallElement(idAnsweredDuplicate.get, formatter.parseDateTime("2014-01-01 09:26:00"), Some(formatter.parseDateTime("2014-01-01 09:26:30")), myInterface)

      callDetailDao.historyByDate(myInterface, "2014-01-01 08:00:00") shouldEqual List(
        CallDetail(formatter.parseDateTime("2014-01-01 09:26:00"), Some(new Duration(formatter.parseDateTime("2014-01-01 09:26:00"),
          formatter.parseDateTime("2014-01-01 09:38:00"))), Some("2002"), Some("2001"), CallStatus.Answered),
        CallDetail(formatter.parseDateTime("2014-01-01 09:13:00"), Some(new Duration(formatter.parseDateTime("2014-01-01 09:13:00"),
          formatter.parseDateTime("2014-01-01 09:25:00"))), Some("2001"), Some("2002"), CallStatus.Emitted),
        CallDetail(formatter.parseDateTime("2014-01-01 09:00:00"), Some(new Duration(formatter.parseDateTime("2014-01-01 09:00:00"),
          formatter.parseDateTime("2014-01-01 09:00:30"))), Some("2001"), Some("2002"), CallStatus.Missed),
      )
    }
  }

  "history by agentNum" should {
    "retrieve internal incoming call answered" in new Helper {
      val agentNum = "1004"
      val limit = 10
      val fromDate = "2017-01-01 00:00:00"

      val srcId = RecordHelper.insertUser("Jerome", "Gerber")
      val dstId = RecordHelper.insertUser("Brice", "de Nice")

      RecordHelper.insertExtension("1001", "user", srcId.get.toString)
      RecordHelper.insertExtension("1004", "user", dstId.get.toString)

      RecordHelper.insertCallData("123456.123", formatter.parseDateTime("2017-09-05 15:53:39").toDate,
        Some(formatter.parseDateTime("2017-09-05 15:53:44").toDate), srcNum = Some("1001"), dstNum = Some("1001"), srcAgent = Some("1001"), dstAgent = Some("1002"), status = None)
      RecordHelper.insertCallData("123456.456", formatter.parseDateTime("2017-09-05 15:53:47").toDate,
        Some(formatter.parseDateTime("2017-09-05 15:53:51").toDate), srcNum = Some("1001"), dstNum = Some("1004"), srcAgent = Some("1001"), dstAgent = Some("1004"), status = Some(helpers.Status.Answer))

      RecordHelper.insertAgent("Jerome", "Gerber", "1001", 0)
      RecordHelper.insertAgent("Brice", "de Nice", "1004", 0)

      val historyResult: List[CallDetail] = callDetailDao.historyByAgentNum(agentNum, limit, fromDate)
      val jsonResult = Json.toJson(historyResult)
      jsonResult.as[JsArray].value.size shouldEqual 1
      (jsonResult(0) \ "status").as[String] shouldEqual "answered"
      (jsonResult(0) \ "src_firstname").as[String] shouldEqual "Jerome"
      (jsonResult(0) \ "src_lastname").as[String] shouldEqual "Gerber"
      (jsonResult(0) \ "dst_firstname").as[String] shouldEqual "Brice"
      (jsonResult(0) \ "dst_lastname").as[String] shouldEqual "de Nice"
      (jsonResult(0) \ "src_num").as[String] shouldEqual "1001"
      (jsonResult(0) \ "dst_num").as[String] shouldEqual "1004"
    }

    "retrieve internal incoming call abandoned/rejected" in new Helper {
      val agentNum = "1004"
      val limit = 10
      val fromDate = "2017-01-01 00:00:00"

      val srcId = RecordHelper.insertUser("Jerome", "Gerber")
      val dstId = RecordHelper.insertUser("Brice", "de Nice")

      RecordHelper.insertExtension("1001", "user", srcId.get.toString)
      RecordHelper.insertExtension("1004", "user", dstId.get.toString)

      RecordHelper.insertCallData("123456.123", formatter.parseDateTime("2017-09-05 15:53:39").toDate,
        Some(formatter.parseDateTime("2017-09-05 15:53:44").toDate),
        srcNum = Some("1001"), dstNum = Some("1004"), srcAgent = None, dstAgent = Some("1004"), direction = CallDirection.Internal, status = None)

      RecordHelper.insertAgent("Jerome", "Gerber", "1001", 0)
      RecordHelper.insertAgent("Brice", "de Nice", "1004", 0)

      val historyResult: List[CallDetail] = callDetailDao.historyByAgentNum(agentNum, limit, fromDate)
      val jsonResult = Json.toJson(historyResult)
      jsonResult.as[JsArray].value.size shouldEqual 1
      (jsonResult(0) \ "status").as[String] shouldEqual "abandoned"
      (jsonResult(0) \ "src_firstname").as[String] shouldEqual "Jerome"
      (jsonResult(0) \ "src_lastname").as[String] shouldEqual "Gerber"
      (jsonResult(0) \ "dst_firstname").as[String] shouldEqual "Brice"
      (jsonResult(0) \ "dst_lastname").as[String] shouldEqual "de Nice"
      (jsonResult(0) \ "src_num").as[String] shouldEqual "1001"
      (jsonResult(0) \ "dst_num").as[String] shouldEqual "1004"
    }

    "retrieve internal outgoing call answered" in new Helper {
      val agentNum = "1004"
      val limit = 10
      val fromDate = "2017-01-01 00:00:00"

      val srcId = RecordHelper.insertUser("Brice", "de Nice")
      val dstId = RecordHelper.insertUser("Jerome", "Gerber")

      RecordHelper.insertExtension("1004", "user", srcId.get.toString)
      RecordHelper.insertExtension("1001", "user", dstId.get.toString)

      RecordHelper.insertCallData("123456.123", formatter.parseDateTime("2017-09-05 15:53:39").toDate,
        Some(formatter.parseDateTime("2017-09-05 15:53:44").toDate), srcNum = Some("1004"), dstNum = Some("1001"), srcAgent = Some("1004"), dstAgent = Some("1001"), status = Some(helpers.Status.Answer), direction = CallDirection.Internal)

      RecordHelper.insertAgent("Jerome", "Gerber", "1001", 0)
      RecordHelper.insertAgent("Brice", "de Nice", "1004", 0)

      val historyResult: List[CallDetail] = callDetailDao.historyByAgentNum(agentNum, limit, fromDate)
      val jsonResult = Json.toJson(historyResult)
      jsonResult.as[JsArray].value.size shouldEqual 1
      (jsonResult(0) \ "status").as[String] shouldEqual "emitted"
      (jsonResult(0) \ "src_firstname").as[String] shouldEqual "Brice"
      (jsonResult(0) \ "src_lastname").as[String] shouldEqual "de Nice"
      (jsonResult(0) \ "dst_firstname").as[String] shouldEqual "Jerome"
      (jsonResult(0) \ "dst_lastname").as[String] shouldEqual "Gerber"
      (jsonResult(0) \ "src_num").as[String] shouldEqual "1004"
      (jsonResult(0) \ "dst_num").as[String] shouldEqual "1001"
    }

    "not retrieve internal incoming call when agent logged out" in new Helper {
      val agentNum = "1004"
      val limit = 10
      val fromDate = "2017-01-01 00:00:00"

      RecordHelper.insertCallData("123456.123", formatter.parseDateTime("2017-09-05 15:53:39").toDate,
        Some(formatter.parseDateTime("2017-09-05 15:53:44").toDate), srcNum = Some("1001"), dstNum = Some("1004"), srcAgent = None, dstAgent = None, status = None, direction = CallDirection.Internal)

      RecordHelper.insertAgent("Jerome", "Gerber", "1001", 0)
      RecordHelper.insertAgent("Brice", "de Nice", "1004", 0)

      val historyResult: List[CallDetail] = callDetailDao.historyByAgentNum(agentNum, limit, fromDate)
      val jsonResult = Json.toJson(historyResult)
      jsonResult.as[JsArray].value.size shouldEqual 0
    }

    "retrieve internal outgoing call canceled/rejected" in new Helper {
      val agentNum = "1004"
      val limit = 10
      val fromDate = "2017-01-01 00:00:00"

      val srcId = RecordHelper.insertUser("Brice", "de Nice")
      val dstId = RecordHelper.insertUser("Jerome", "Gerber")

      RecordHelper.insertCallData("123456.123", formatter.parseDateTime("2017-09-05 15:53:39").toDate,
        Some(formatter.parseDateTime("2017-09-05 15:53:44").toDate), srcNum = Some("1004"), dstNum = Some("1001"), srcAgent = Some("1004"), dstAgent = Some("1001"), status = None, direction = CallDirection.Internal)

      RecordHelper.insertExtension("1004", "user", srcId.get.toString)
      RecordHelper.insertExtension("1001", "user", dstId.get.toString)

      RecordHelper.insertAgent("Jerome", "Gerber", "1001", 0)
      RecordHelper.insertAgent("Brice", "de Nice", "1004", 0)

      val historyResult: List[CallDetail] = callDetailDao.historyByAgentNum(agentNum, limit, fromDate)
      val jsonResult = Json.toJson(historyResult)
      jsonResult.as[JsArray].value.size shouldEqual 1
      (jsonResult(0) \ "status").as[String] shouldEqual "emitted"
      (jsonResult(0) \ "src_firstname").as[String] shouldEqual "Brice"
      (jsonResult(0) \ "src_lastname").as[String] shouldEqual "de Nice"
      (jsonResult(0) \ "dst_firstname").as[String] shouldEqual "Jerome"
      (jsonResult(0) \ "dst_lastname").as[String] shouldEqual "Gerber"
      (jsonResult(0) \ "src_num").as[String] shouldEqual "1004"
      (jsonResult(0) \ "dst_num").as[String] shouldEqual "1001"
    }

    "retrieve internal outgoing call answered without source agent" in new Helper {
      val agentNum = "1004"
      val limit = 10
      val fromDate = "2017-01-01 00:00:00"

      RecordHelper.insertCallData("123456.123", formatter.parseDateTime("2017-09-05 15:53:39").toDate,
        Some(formatter.parseDateTime("2017-09-05 15:53:44").toDate), srcNum = Some("1001"), dstNum = Some("1001"), status = None)
      RecordHelper.insertCallData("123456.456", formatter.parseDateTime("2017-09-05 15:53:47").toDate,
        Some(formatter.parseDateTime("2017-09-05 15:53:51").toDate), srcNum = Some("1001"), dstNum = Some("1004"), dstAgent = Some("1004"), status = Some(helpers.Status.Answer))

      RecordHelper.insertAgent("Jerome", "Gerber", "1001", 0)
      RecordHelper.insertAgent("Brice", "de Nice", "1004", 0)

      val historyResult: List[CallDetail] = callDetailDao.historyByAgentNum(agentNum, limit, fromDate)
      val jsonResult = Json.toJson(historyResult)
      jsonResult.as[JsArray].value.size shouldEqual 1
      (jsonResult(0) \ "status").as[String] shouldEqual "answered"
      (jsonResult(0) \ "src_num").as[String] shouldEqual "1001"
      (jsonResult(0) \ "dst_num").as[String] shouldEqual "1004"
    }

    "retrieve external outgoing call answered" in new Helper {
      val agentNum = "1004"
      val limit = 10
      val fromDate = "2017-01-01 00:00:00"

      val srcId = RecordHelper.insertUser("Brice", "de Nice")

      RecordHelper.insertExtension("1004", "user", srcId.get.toString)

      RecordHelper.insertCallData("123456.123", formatter.parseDateTime("2017-09-05 15:53:39").toDate,
        Some(formatter.parseDateTime("2017-09-05 15:53:44").toDate), srcNum = Some("1004"), dstNum = Some("01230041304"), srcAgent = Some("1004"), status = Some(helpers.Status.Answer), direction = CallDirection.Outgoing)

      RecordHelper.insertAgent("Jerome", "Gerber", "1001", 0)
      RecordHelper.insertAgent("Brice", "de Nice", "1004", 0)

      val historyResult: List[CallDetail] = callDetailDao.historyByAgentNum(agentNum, limit, fromDate)
      val jsonResult = Json.toJson(historyResult)
      jsonResult.as[JsArray].value.size shouldEqual 1
      (jsonResult(0) \ "status").as[String] shouldEqual "emitted"
      (jsonResult(0) \ "src_firstname").as[String] shouldEqual "Brice"
      (jsonResult(0) \ "src_lastname").as[String] shouldEqual "de Nice"
      (jsonResult(0) \ "dst_firstname").asOpt[String] shouldEqual None
      (jsonResult(0) \ "dst_lastname").asOpt[String] shouldEqual None
      (jsonResult(0) \ "src_num").as[String] shouldEqual "1004"
      (jsonResult(0) \ "dst_num").as[String] shouldEqual "01230041304"
    }

    "retrieve external outgoing call canceled/rejected" in new Helper {
      val agentNum = "1004"
      val limit = 10
      val fromDate = "2017-01-01 00:00:00"

      val srcId = RecordHelper.insertUser("Brice", "de Nice")

      RecordHelper.insertExtension("1004", "user", srcId.get.toString)

      RecordHelper.insertCallData("123456.123", formatter.parseDateTime("2017-09-05 15:53:39").toDate,
        Some(formatter.parseDateTime("2017-09-05 15:53:44").toDate), srcNum = Some("1004"), dstNum = Some("01230041304"), srcAgent = Some("1004"), status = None, direction = CallDirection.Outgoing)

      RecordHelper.insertAgent("Jerome", "Gerber", "1001", 0)
      RecordHelper.insertAgent("Brice", "de Nice", "1004", 0)

      val historyResult: List[CallDetail] = callDetailDao.historyByAgentNum(agentNum, limit, fromDate)
      val jsonResult = Json.toJson(historyResult)
      jsonResult.as[JsArray].value.size shouldEqual 1
      (jsonResult(0) \ "status").as[String] shouldEqual "emitted"
      (jsonResult(0) \ "src_firstname").as[String] shouldEqual "Brice"
      (jsonResult(0) \ "src_lastname").as[String] shouldEqual "de Nice"
      (jsonResult(0) \ "dst_firstname").asOpt[String] shouldEqual None
      (jsonResult(0) \ "dst_lastname").asOpt[String] shouldEqual None
      (jsonResult(0) \ "src_num").as[String] shouldEqual "1004"
      (jsonResult(0) \ "dst_num").as[String] shouldEqual "01230041304"
    }

    "retrieve external outgoing call without status" in new Helper {
      val agentNum = "1004"
      val limit = 10
      val fromDate = "2017-01-01 00:00:00"

      RecordHelper.insertCallData("123456.123", formatter.parseDateTime("2017-09-05 15:53:39").toDate,
        Some(formatter.parseDateTime("2017-09-05 15:53:44").toDate), srcNum = Some("1004"), dstNum = Some("01230041304"), srcAgent = Some("1004"), status = None, direction = CallDirection.Outgoing)

      RecordHelper.insertAgent("Jerome", "Gerber", "1001", 0)
      RecordHelper.insertAgent("Brice", "de Nice", "1004", 0)

      val historyResult: List[CallDetail] = callDetailDao.historyByAgentNum(agentNum, limit, fromDate)
      val jsonResult = Json.toJson(historyResult)
      jsonResult.as[JsArray].value.size shouldEqual 1
      (jsonResult(0) \ "status").as[String] shouldEqual "emitted"
      (jsonResult(0) \ "dst_firstname").asOpt[String] shouldEqual None
      (jsonResult(0) \ "dst_lastname").asOpt[String] shouldEqual None
      (jsonResult(0) \ "src_num").as[String] shouldEqual "1004"
      (jsonResult(0) \ "dst_num").as[String] shouldEqual "01230041304"
    }

    "not retrieve external outgoing call without source agent" in new Helper {
      val agentNum = "1004"
      val limit = 10
      val fromDate = "2017-01-01 00:00:00"

      RecordHelper.insertCallData("123456.123", formatter.parseDateTime("2017-09-05 15:53:39").toDate,
        Some(formatter.parseDateTime("2017-09-05 15:53:44").toDate), srcNum = Some("1004"), dstNum = Some("01230041304"), status = None, direction = CallDirection.Outgoing)

      RecordHelper.insertAgent("Jerome", "Gerber", "1001", 0)
      RecordHelper.insertAgent("Brice", "de Nice", "1004", 0)

      val historyResult: List[CallDetail] = callDetailDao.historyByAgentNum(agentNum, limit, fromDate)
      val jsonResult = Json.toJson(historyResult)
      jsonResult.as[JsArray].value.size shouldEqual 0
    }

    "retrieve external incoming ACD call answered" in new Helper {
      val agentNum = "1004"
      val limit = 10
      val fromDate = "2017-01-01 00:00:00"

      val srcId = RecordHelper.insertUser("Brice", "de Nice")

      RecordHelper.insertExtension("1004", "user", srcId.get.toString)

      RecordHelper.insertCallData("123456.123", formatter.parseDateTime("2017-09-05 15:53:39").toDate,
        Some(formatter.parseDateTime("2017-09-05 15:53:44").toDate), srcNum = Some("41304"), dstNum = None, srcAgent = None, status = Some(helpers.Status.Answer), direction = CallDirection.Incoming)

      RecordHelper.insertCallOnQueue("123456.123", "1004", "queue1", QueueCallStatus.Answered)

      RecordHelper.insertAgent("Jerome", "Gerber", "1001", 0)
      RecordHelper.insertAgent("Brice", "de Nice", "1004", 0)

      val historyResult: List[CallDetail] = callDetailDao.historyByAgentNum(agentNum, limit, fromDate)
      val jsonResult = Json.toJson(historyResult)
      jsonResult.as[JsArray].value.size shouldEqual 1
      (jsonResult(0) \ "status").as[String] shouldEqual "answered"
      (jsonResult(0) \ "src_firstname").asOpt[String] shouldEqual None
      (jsonResult(0) \ "src_lastname").asOpt[String] shouldEqual None
      (jsonResult(0) \ "dst_firstname").as[String] shouldEqual "Brice"
      (jsonResult(0) \ "dst_lastname").as[String] shouldEqual "de Nice"
      (jsonResult(0) \ "src_num").as[String] shouldEqual "41304"
      (jsonResult(0) \ "dst_num").as[String] shouldEqual "1004"
    }

    "retrieve external incoming ACD call rejected by agent or ring timeout" in new Helper {
      val agentNum = "1004"
      val limit = 10
      val fromDate = "2017-01-01 00:00:00"

      val srcId = RecordHelper.insertUser("Brice", "de Nice")

      RecordHelper.insertExtension("1004", "user", srcId.get.toString)

      RecordHelper.insertCallData("123456.123", formatter.parseDateTime("2017-09-05 15:53:39").toDate,
        Some(formatter.parseDateTime("2017-09-05 15:53:44").toDate), srcNum = Some("41304"), dstNum = None, srcAgent = None, status = None, direction = CallDirection.Incoming)

      RecordHelper.insertCallOnQueue("123456.123", "1004", "queue1", QueueCallStatus.Abandoned)

      RecordHelper.insertAgent("Jerome", "Gerber", "1001", 0)
      RecordHelper.insertAgent("Brice", "de Nice", "1004", 0)

      val historyResult: List[CallDetail] = callDetailDao.historyByAgentNum(agentNum, limit, fromDate)
      val jsonResult = Json.toJson(historyResult)
      jsonResult.as[JsArray].value.size shouldEqual 1
      (jsonResult(0) \ "status").as[String] shouldEqual "abandoned"
      (jsonResult(0) \ "src_firstname").asOpt[String] shouldEqual None
      (jsonResult(0) \ "src_lastname").asOpt[String] shouldEqual None
      (jsonResult(0) \ "dst_firstname").as[String] shouldEqual "Brice"
      (jsonResult(0) \ "dst_lastname").as[String] shouldEqual "de Nice"
      (jsonResult(0) \ "src_num").as[String] shouldEqual "41304"
      (jsonResult(0) \ "dst_num").as[String] shouldEqual "1004"
    }

    "retrieve agent history from given date" in new Helper {
      val agentNum = "1004"
      val limit = 10
      val fromDate = "2021-01-07 15:35:38"

      val userId = RecordHelper.insertUser("Brice", "de Nice")
      RecordHelper.insertAgent("Brice", "de Nice", agentNum, 0)
      RecordHelper.insertExtension("1004", "user", userId.get.toString)

      val queueId = RecordHelper.insertQueue("support", "3000", Some("Support"))
      RecordHelper.insertExtension("3000", "queue", queueId.get.toString)

      RecordHelper.insertCallData("123456.123",
        formatter.parseDateTime("2017-09-05 15:53:39").toDate,
        Some(formatter.parseDateTime("2017-09-05 15:53:44").toDate),
        dstAgent = Some(agentNum),
        srcNum = Some("41304"), status = Some(helpers.Status.Answer), direction = CallDirection.Incoming)
      RecordHelper.insertCallOnQueue("123456.123", agentNum, "queue1", QueueCallStatus.Answered)

      RecordHelper.insertCallData("234567.123",
        formatter.parseDateTime("2021-01-14 15:41:40").toDate,
        Some(formatter.parseDateTime("2021-01-14 15:45:40").toDate),
        dstAgent = Some(agentNum),
        srcNum = Some("0123456789"),
        dstNum = Some("3000"), status = Some(helpers.Status.Answer), direction = CallDirection.Incoming)
      RecordHelper.insertCallOnQueue("234567.123", agentNum, "support", QueueCallStatus.Answered, queue_time = formatter.parseDateTime("2021-01-14 15:41:40"))

      val historyResult: List[CallDetail] = callDetailDao.historyByAgentNum(agentNum, limit, fromDate)
      val jsonResult = Json.toJson(historyResult)
      jsonResult.as[JsArray].value.size shouldEqual 1
      (jsonResult(0) \ "status").as[String] shouldEqual "answered"
      (jsonResult(0) \ "src_num").as[String] shouldEqual "0123456789"
      (jsonResult(0) \ "dst_num").as[String] shouldEqual "3000"
      (jsonResult(0) \ "start").as[String] shouldEqual "2021-01-14 15:41:40"
    }

    "retrieve agent history from given date with limit" in new Helper {
      val agentNum = "1004"
      val limit = 1
      val fromDate = "2021-01-07 15:35:38"

      val userId = RecordHelper.insertUser("Brice", "de Nice")
      RecordHelper.insertAgent("Brice", "de Nice", agentNum, 0)
      RecordHelper.insertExtension("1004", "user", userId.get.toString)

      val queueId = RecordHelper.insertQueue("support", "3000", Some("Support"))
      RecordHelper.insertExtension("3000", "queue", queueId.get.toString)

      RecordHelper.insertCallData("123456.123",
        formatter.parseDateTime("2017-09-05 15:53:39").toDate,
        Some(formatter.parseDateTime("2017-09-05 15:53:44").toDate),
        dstAgent = Some(agentNum),
        srcNum = Some("41304"), status = Some(helpers.Status.Answer), direction = CallDirection.Incoming)
      RecordHelper.insertCallOnQueue("123456.123", agentNum, "queue1", QueueCallStatus.Answered)

      RecordHelper.insertCallData("234567.123",
        formatter.parseDateTime("2021-01-14 15:41:40").toDate,
        Some(formatter.parseDateTime("2021-01-14 15:45:40").toDate),
        dstAgent = Some(agentNum),
        srcNum = Some("0123456789"),
        dstNum = Some("3000"), status = Some(helpers.Status.Answer), direction = CallDirection.Incoming)
      RecordHelper.insertCallOnQueue("234567.123", agentNum, "support", QueueCallStatus.Answered,
        queue_time = formatter.parseDateTime("2021-01-14 15:41:40"))

      RecordHelper.insertCallData("234589.123",
        formatter.parseDateTime("2021-01-14 18:41:40").toDate,
        Some(formatter.parseDateTime("2021-01-14 18:45:40").toDate),
        dstAgent = Some(agentNum),
        srcNum = Some("0222222222"),
        dstNum = Some("3000"), status = Some(helpers.Status.Answer), direction = CallDirection.Incoming)
      RecordHelper.insertCallOnQueue("234589.123", agentNum, "support", QueueCallStatus.Answered,
        queue_time = formatter.parseDateTime("2021-01-14 18:41:40"))

      val historyResult: List[CallDetail] = callDetailDao.historyByAgentNum(agentNum, limit, fromDate)
      val jsonResult = Json.toJson(historyResult)
      jsonResult.as[JsArray].value.size shouldEqual 1
      (jsonResult(0) \ "status").as[String] shouldEqual "answered"
      (jsonResult(0) \ "src_num").as[String] shouldEqual "0222222222"
      (jsonResult(0) \ "dst_num").as[String] shouldEqual "3000"
      (jsonResult(0) \ "start").as[String] shouldEqual "2021-01-14 18:41:40"
    }
  }
}
