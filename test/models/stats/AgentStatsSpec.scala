package models.stats

import helpers.{Status, DbTest, RecordHelper}
import models.stats.AgentStats.AgentNumber
import org.joda.time.DateTime
import org.specs2.mutable.Before
import play.api.test.PlaySpecification
import models.DBUtil

class AgentStatsSpec extends PlaySpecification with DbTest {

  trait clean extends Before {
    val tables = List("call_on_queue", "call_data")
    def before = {
      for (table <- tables)
        DBUtil.cleanTable(table)
    }
  }

  "return the agent number of last answered call for a given caller" in new clean {
    insertCallsToBeFound()

    AgentStats.searchLastAgentForCaller("1111111111", 3) shouldEqual Some(AgentNumber("2201"))

    def insertCallsToBeFound(): Unit = {
      RecordHelper.insertCallData("1.000", DateTime.now().minusDays(5).toDate,
        Some(DateTime.now().minusDays(5).plusMinutes(10).toDate), srcNum = Some("1111111111"), dstNum = None,
        status=Some(Status.Answer))
      RecordHelper.insertCallOnQueue("1.000", "2201", "3000")

      RecordHelper.insertCallData("1.000", DateTime.now().minusDays(5).toDate,
        Some(DateTime.now().minusDays(5).plusMinutes(10).toDate), srcNum = Some("1111111111"), dstNum = None,
        status=Some(Status.Answer))
      RecordHelper.insertCallOnQueue("1.000", "2201", "3000")

      RecordHelper.insertCallData("2.000", DateTime.now().minusDays(2).toDate,
        Some(DateTime.now().minusDays(2).plusMinutes(10).toDate), srcNum = Some("1111111111"), dstNum = None,
        status=Some(Status.Answer))
      RecordHelper.insertCallOnQueue("2.000", "2201", "4000")
    }
  }

  "return None if the last answered call for a given caller is not found" in new clean {
    insertCallsToBeIgnored()

    AgentStats.searchLastAgentForCaller("2222222222", 3) shouldEqual None

    def insertCallsToBeIgnored(): Unit = {
      RecordHelper.insertCallData("1.000", DateTime.now().minusDays(2).toDate,
        Some(DateTime.now().minusDays(2).plusMinutes(1).toDate), srcNum = Some("2222222222"), dstNum = None,
        status=None)
      RecordHelper.insertCallOnQueue("1.000", "2201", "3000")

      RecordHelper.insertCallData("2.000", DateTime.now().minusDays(1).toDate,
        Some(DateTime.now().minusDays(1).plusMinutes(1).toDate), srcNum = Some("2222222222"), dstNum = None,
        status=None)
      RecordHelper.insertCallOnQueue("2.000", "2201", "4000")
    }
  }

}
