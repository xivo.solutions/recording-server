package models

import java.security.InvalidParameterException
import java.util.UUID

import helpers.{AppTest, CallbacksHelper}
import org.joda.time.{DateTime, LocalDate, LocalTime}
import play.api.cache.SyncCacheApi
import play.api.inject.guice.GuiceApplicationBuilder

class CallbackTicketDaoSpec extends AppTest {

  val periodStart = new LocalTime(8, 0, 0)
  val periodEnd = new LocalTime(11, 0, 0)
  val periodUuid = CallbacksHelper.insertPeriod(periodStart, periodEnd)
  val requestUuid = CallbacksHelper.insertRequest(periodUuid, "1000")

  override def fakeApplication() = new GuiceApplicationBuilder()
    .configure(testConfig).build()

  val callbackTicketDao = app.injector.instanceOf(classOf[CallbackTicketDao])
  val cache = app.injector.instanceOf(classOf[SyncCacheApi])

  "The CallbackTicket singleton" should {
    "insert a ticket" in {
      DBUtil.cleanTable("callback_ticket")
      val ticket = CallbackTicket(None, UUID.randomUUID(), UUID.randomUUID(), "thequeue", "4000", new LocalDate(2015, 1, 8),
        formatter.parseDateTime("2015-01-08 08:00:00"), Some(formatter.parseDateTime("2015-01-08 08:30:00")), firstName = Some("John"))

      val res = callbackTicketDao.create(ticket)

      CallbacksHelper.allTickets shouldBe List(res)
      res.copy(uuid = None) shouldBe ticket
      res.uuid.isEmpty should be(false)
    }

    "update a ticket and change the lastUpdate date" in {
      DBUtil.cleanTable("callback_ticket")
      val ticket = callbackTicketDao.create(
        CallbackTicket(None, UUID.randomUUID(), UUID.randomUUID(), "thequeue", "4000", new LocalDate(), formatter.parseDateTime("2014-01-01 08:00:00"),
          Some(formatter.parseDateTime("2014-01-01 08:00:00")), firstName = Some("John")))

      val lastUpdateBeforeTicketUpdate = CallbacksHelper.allTickets.head.lastUpdate.get

      callbackTicketDao.update(ticket.uuid.get.toString, CallbackTicketPatch(Some(CallbackStatus.Fax), Some("My comment"), Some("123456.789")))

      val allTickets = CallbacksHelper.allTickets
      allTickets.size shouldEqual 1
      allTickets.head.comment shouldEqual Some("My comment")
      allTickets.head.status shouldEqual Some(CallbackStatus.Fax)
      allTickets.head.callid shouldEqual Some("123456.789")
      allTickets.head.lastUpdate.get.isAfter(lastUpdateBeforeTicketUpdate) should be(true)
    }

    "throw an InvalidParameterException if the uuid is invalid" in {
      val e = the[InvalidParameterException] thrownBy callbackTicketDao.update("1234", CallbackTicketPatch(Some(CallbackStatus.Fax), Some("My comment"), None))

      e.getMessage should be("Invalid UUID : 1234")
    }

    "throw a NoSuchElementException when there is no ticket with the provided UUID" in {
      val uuid = UUID.randomUUID().toString
      val e = the[NoSuchElementException] thrownBy callbackTicketDao.update(uuid, CallbackTicketPatch(Some(CallbackStatus.Fax), Some("My comment"), None))

      e.getMessage should be(s"No CallbackTicket found with uuid $uuid")
    }

    "not update the fields set to None" in {
      DBUtil.cleanTable("callback_ticket")
      val ticket = callbackTicketDao.create(
        CallbackTicket(None, UUID.randomUUID(), UUID.randomUUID(), "thequeue", "4000", new LocalDate(), new DateTime(), Some(new DateTime),
          comment = Some("My Comment"), status = Some(CallbackStatus.Callback), callid = Some("123456.789")))

      callbackTicketDao.update(ticket.uuid.get.toString, CallbackTicketPatch(None, None, None))

      val allTickets = CallbacksHelper.allTickets
      allTickets.head.comment shouldBe Some("My Comment")
      allTickets.head.status shouldBe Some(CallbackStatus.Callback)
      allTickets.head.callid shouldBe Some("123456.789")
    }

    "list tickets by list" in {
      DBUtil.cleanTable("callback_ticket")
      val t1 = callbackTicketDao.create(
        CallbackTicket(None, UUID.randomUUID(), requestUuid, "thequeue", "4000", new LocalDate(2015, 1, 1), formatter.parseDateTime("2015-01-01 08:00:00"),
          Some(formatter.parseDateTime("2015-01-01 08:30:00")), firstName = Some("John")))

      callbackTicketDao.byList(t1.listUuid.toString) shouldBe List(t1.copy(periodStart = Some(periodStart), periodEnd = Some(periodEnd)))
    }

    "get a ticket by uuid" in {
      DBUtil.cleanTable("callback_ticket")
      val t1 = callbackTicketDao.create(
        CallbackTicket(None, UUID.randomUUID(), UUID.randomUUID(), "thequeue", "4000", new LocalDate(2015, 1, 1), formatter.parseDateTime("2015-01-01 08:00:00"),
          Some(formatter.parseDateTime("2015-01-01 08:30:00")), firstName = Some("John")))

      callbackTicketDao.getTicket(t1.uuid.get.toString) shouldBe t1
    }

    "throw a NoSuchElementException when asking for a ticket which does not exist" in {
      val uuid = UUID.randomUUID()
      val e = the[NoSuchElementException] thrownBy callbackTicketDao.getTicket(uuid.toString)

      e.getMessage should be(s"No ticket with uuid = $uuid found")
    }

    "create a ticket with Voicemail status" in {
      DBUtil.cleanTable("callback_ticket")
      val ticket = CallbackTicket(
        None,
        UUID.randomUUID(),
        UUID.randomUUID(),
        "thequeue",
        "4000",
        new LocalDate(2015, 1, 8),
        formatter.parseDateTime("2015-01-08 08:00:00"),
        Some(formatter.parseDateTime("2015-01-08 08:30:00")),
        status = Some(CallbackStatus.Voicemail),
        firstName = Some("Jane")
      )

      val res = callbackTicketDao.create(ticket)

      CallbacksHelper.allTickets shouldBe List(res)
      res.status shouldBe Some(CallbackStatus.Voicemail)
    }

    "create a ticket with Email status" in {
      DBUtil.cleanTable("callback_ticket")
      val ticket = CallbackTicket(
        None,
        UUID.randomUUID(),
        UUID.randomUUID(),
        "thequeue",
        "4000",
        new LocalDate(2015, 1, 8),
        formatter.parseDateTime("2015-01-08 08:00:00"),
        Some(formatter.parseDateTime("2015-01-08 08:30:00")),
        status = Some(CallbackStatus.Email),
        firstName = Some("Doe")
      )

      val res = callbackTicketDao.create(ticket)

      CallbacksHelper.allTickets shouldBe List(res)
      res.status shouldBe Some(CallbackStatus.Email)
    }

  }
}
