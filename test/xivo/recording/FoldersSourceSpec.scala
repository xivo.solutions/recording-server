package xivo.recording

import akka.actor.ActorSystem
import akka.stream.scaladsl.{Flow, Keep, Sink}
import helpers.TestUtils.testConfig
import helpers.{ConfigTest, TestUtils}
import org.scalatest.BeforeAndAfterEach
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

import java.io.File
import java.nio.file.{Files, Path}

class FoldersSourceSpec extends AnyWordSpec with Matchers with ConfigTest with BeforeAndAfterEach with ScalaFutures {

  val xivoAudioFolder: String = s"${testConfig("audio.folder")}/xivo"

  class Helper() {
    implicit val system: ActorSystem = ActorSystem()

    val foldersSource = new FoldersSource(xivoAudioFolder)
  }

  override protected def beforeEach(): Unit = {
    TestUtils.cleanDirectory(xivoAudioFolder)
    TestUtils.cleanTables(List("attached_data", "call_data", "call_on_queue", "call_element", "transfers"))
  }

  "A FoldersSource " should {
    "get root folder" in new Helper {
      val testSink = Flow[Path].toMat(Sink.fold[List[Path], Path](List())((acc, p) => acc.::(p)))(Keep.right)
      val root = new File(xivoAudioFolder).toPath

      val res = foldersSource.rootFolderSource.runWith(testSink).futureValue

      res should contain theSameElementsAs List(root)
      res.size shouldEqual 1
    }

    "get sub folder" in new Helper {
      val testSink = Flow[Path].toMat(Sink.fold[List[Path], Path](List())((acc, p) => acc.::(p)))(Keep.right)
      val sub1 = new File(s"$xivoAudioFolder/xivo1").toPath
      val sub2 = new File(s"$xivoAudioFolder/xivo2").toPath

      Files.createDirectory(sub1)
      Files.createDirectory(sub2)

      val res = foldersSource.subFoldersSource.runWith(testSink).futureValue

      res should contain theSameElementsAs List(sub1, sub2)
      res.size shouldEqual 2
    }

    "get root folder and sub folders together" in new Helper {
      val testSink = Flow[List[Path]].toMat(Sink.head)(Keep.right)
      val root = new File(xivoAudioFolder).toPath
      val sub1 = new File(s"$xivoAudioFolder/xivo1").toPath
      val sub2 = new File(s"$xivoAudioFolder/xivo2").toPath

      Files.createDirectory(sub1)
      Files.createDirectory(sub2)

      val res = foldersSource.folders.runWith(testSink).futureValue

      res should contain theSameElementsAs List(root, sub1, sub2)
      res.size shouldEqual 3
    }
  }
}
