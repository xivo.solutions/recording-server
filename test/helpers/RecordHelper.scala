package helpers

import java.sql.Connection
import java.util.Date

import anorm.SQL
import models.CallDirection.CallDirection
import models.QueueCallStatus.QueueCallStatus
import models.{CallDirection, QueueCallStatus}
import org.joda.time.DateTime

import scala.util.Random

object RecordHelper {
  val defaultAgent = "1200"
  val defaultQueue = "myqueue"
  val defaultSrcNum = "1001"
  val defaultDstNum = "2001"


  val dateFormat = "yyyy-MM-dd HH:mm:ss"
  val random = new Random()

  def insertFullRecord(id: String, status:QueueCallStatus)(implicit connection: Connection): Unit = {
    insertRecord(id, s"gw1-$id", DateTime.now, Some(DateTime.now.plusMinutes(5)), Some(defaultSrcNum), Some(defaultDstNum))
    insertCallOnQueue(id, defaultAgent, defaultQueue, status)
  }

  def insertIncall(number: String)(implicit c: Connection): Option[Long] = {
    val incallId = random.nextInt()
    SQL("INSERT INTO extensions(exten, type, typeval) VALUES ({number}, 'incall', {incallId})").on(Symbol("number") -> number, Symbol("incallId") -> incallId).executeUpdate()
    Some(incallId.toLong)
  }

  def insertCallData(uniqueId: String, start: Date, end: Option[Date], srcNum: Option[String] = Some("0312"), dstNum: Option[String] = Some("1000"),
                     direction: CallDirection=CallDirection.Incoming, srcAgent:Option[String]=None, dstAgent: Option[String]=None, srcInterface: Option[String]=None,
                     status: Option[Status.Status] = None)
                    (implicit connection: Connection): Option[Long] =
    status match {
      case None =>
        SQL(s"""INSERT INTO call_data(uniqueid, src_num, dst_num, start_time, end_time, call_direction, src_agent, dst_agent, src_interface) VALUES
          ({uniqueId}, {srcNum}, {dstNum}, {start}, {end}, {direction}::call_direction_type, {srcAgent}, {dstAgent}, {srcInterface})""")
          .on(Symbol("uniqueId") -> uniqueId, Symbol("srcNum") -> srcNum, Symbol("dstNum") -> dstNum, Symbol("start") -> start, Symbol("end") -> end, Symbol("direction") -> direction.toString,
            Symbol("srcAgent") -> srcAgent, Symbol("dstAgent") -> dstAgent, Symbol("srcInterface") -> srcInterface).executeInsert()
      case Some(status) =>
        SQL(s"""INSERT INTO call_data(uniqueid, src_num, dst_num, start_time, end_time, call_direction, src_agent, dst_agent, src_interface, status) VALUES
          ({uniqueId}, {srcNum}, {dstNum}, {start}, {end}, {direction}::call_direction_type, {srcAgent}, {dstAgent}, {srcInterface}, {status})""")
          .on(Symbol("uniqueId") -> uniqueId, Symbol("srcNum") -> srcNum, Symbol("dstNum") -> dstNum, Symbol("start") -> start, Symbol("end") -> end, Symbol("direction") -> direction.toString,
            Symbol("srcAgent") -> srcAgent, Symbol("dstAgent") -> dstAgent, Symbol("srcInterface") -> srcInterface, Symbol("status") -> status.toString).executeInsert()
    }

  def insertRecord(uniqueId: String, globalCallid: String, start: DateTime, end: Option[DateTime], srcNum: Option[String], dstNum: Option[String],
                   data: Map[String, String] = Map(), direction: CallDirection = CallDirection.Incoming,
                   srcAgent: Option[String] = None, dstAgent: Option[String] = None)
                  (implicit connection: Connection): Unit = {
    val theEnd = end.map(d => d.toDate)
    val cdId = insertCallData(uniqueId, start.toDate, theEnd, srcNum, dstNum, direction, srcAgent, dstAgent)
    insertAttachedData(cdId.get, "recording", globalCallid)
    for ((key, value) <- data)
      insertAttachedData(cdId.get, key, value)
    ()
  }

  def insertAttachedData(callDataId: Long, key: String, value: String)(implicit connection: Connection) =
    SQL("INSERT INTO attached_data(id_call_data, key, value) VALUES ({callDataId}, {key}, {value})")
      .on(Symbol("callDataId") -> callDataId, Symbol("key") -> key, Symbol("value") -> value).executeInsert()

  def insertCallOnQueue(linkedId: String, agent: String, queue: String, status: QueueCallStatus.QueueCallStatus = QueueCallStatus.Abandoned, queue_time: DateTime = new DateTime())(implicit connection: Connection):Unit ={
    val nullOrStatus = if (status != null) s"'$status'" else null
    SQL(s"INSERT INTO call_on_queue(callid, queue_time, status, agent_num, queue_ref) VALUES('$linkedId', now(), $nullOrStatus, '$agent', '$queue')").executeUpdate()
    ()
  }

  def insertAgentGroup(name: String)(implicit c: Connection): Option[Long] =
    SQL("INSERT INTO agentgroup(name) VALUES ({name})").on(Symbol("name") -> name).executeInsert()


  def insertAgent(firstName: String, lastName: String, number: String, groupId: Int)(implicit c: Connection): Option[Long] =
    SQL("INSERT INTO agentfeatures(firstname, lastname, number, numgroup) VALUES ({firstname}, {lastname}, {number}, {groupId})").on(
      Symbol("firstname") -> firstName, Symbol("lastname") -> lastName, Symbol("number") -> number, Symbol("groupId") -> groupId).executeInsert()

  def insertUser(firstName: String, lastName: String)(implicit c: Connection): Option[Long] =
    SQL("INSERT INTO userfeatures(firstname, lastname) VALUES ({firstname}, {lastname})").on(
      Symbol("firstname") -> firstName, Symbol("lastname") -> lastName).executeInsert()

  def insertExtension(exten: String, exten_type: String, typeval: String)(implicit c: Connection): Option[Long] =
    SQL("INSERT INTO extensions(exten, type, typeval) VALUES ({exten}, {type}, {typeval})")
      .on("exten" -> exten, "type" -> exten_type, "typeval" -> typeval)
      .executeInsert()

  def insertQueue(name: String, number: String, displayname: Option[String] = None)(implicit c: Connection): Option[Long] =
    SQL("INSERT INTO queuefeatures(name, number, displayname) VALUES ({name}, {number}, {displayname})").on(Symbol("name") -> name, Symbol("number") -> number, "displayname" -> displayname).executeInsert()

  def insertFilteredIncall(number: String)(implicit c: Connection) =
    SQL("INSERT INTO filtered_numbers(type, number) VALUES ('incall'::number_type, {number})").on(Symbol("number") -> number).executeUpdate()


  def insertFilteredInternalNumber(number: String)(implicit c: Connection) =
    SQL("INSERT INTO filtered_numbers(type, number) VALUES ('internal_number'::number_type, {number})").on(Symbol("number") -> number).executeUpdate()

  def insertCallElement(callDataId: Long, start: DateTime, end: Option[DateTime], interface: String, answer: Option[DateTime] = None)(implicit c: Connection) =
    SQL("INSERT INTO call_element(call_data_id, start_time, end_time, interface, answer_time) VALUES ({id}, {start}, {end}, {interface}, {answer})")
    .on(Symbol("id") -> callDataId, Symbol("start") -> start.toDate, Symbol("end") -> end.map(_.toDate), Symbol("interface") -> interface, Symbol("answer") -> answer.map(_.toDate)).executeUpdate()

  def insertTransfers(callidfrom: String, callidto: String)(implicit connection: Connection): Unit = {
    SQL("INSERT INTO transfers(callidfrom, callidto) VALUES ({callidfrom}, {callidto})").on(Symbol("callidfrom") -> callidfrom, Symbol("callidto") -> callidto).executeUpdate()
    ()
  }

  def insertCel(eventtype: String, eventtime: String, cid_name: String, cid_num: String, channame: String, uniqueid: String, linkedid: String)(implicit c:Connection): Option[Long] = {
    SQL("INSERT INTO cel(eventtype, eventtime, userdeftype, cid_name, cid_num, cid_ani, cid_rdnis, cid_dnid, exten, context, channame, appname, appdata, amaflags, accountcode, peeraccount, uniqueid, linkedid, userfield, peer, call_log_id)" +
      " VALUES ({eventtype}, {eventtime}, '',  {cid_name}, {cid_num}, {cid_num}, '', '', '', '', {channame},'', '', 0, '', '', {uniqueid}, {linkedid}, '', '', 0)")
      .on("eventtype" -> eventtype, "eventtime" -> eventtime, "cid_name" -> cid_name, "cid_num" -> cid_num, "channame" -> channame, "uniqueid" -> uniqueid, "linkedid" -> linkedid)
      .executeInsert()
  }
}

object Status extends Enumeration {
  type Status = Value
  val Answer = Value("answer")
  val Abandoned = Value("abandoned")
  val Timeout = Value("timeout")
  val Closed = Value("closed")
}
