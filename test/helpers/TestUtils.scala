package helpers

import java.io.File

import models.DBUtil
import org.joda.time.DateTime
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.mvc.Results
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

object TestUtils extends DbTest{
  def cleanDirectory(dirName: String): Unit = {
    val dir = new File(dirName)
    if(!dir.isDirectory) {
      dir.delete()
      dir.mkdirs()
    }
    for(file <- dir.listFiles())
      file.delete()
    ()
  }

  def cleanTables(tables: List[String]): Unit = {
    tables.foreach(table => DBUtil.cleanTable(table))
    ()
  }

  def createFileWithDate(fileName: String, date: DateTime): Unit = {
    val file = new File(fileName)
    file.createNewFile()
    file.setLastModified(date.toDate.getTime)
    ()
  }
}

trait XiVOUnitTest extends AnyWordSpec with Matchers

trait UnitDbTest extends XiVOUnitTest with DbTest

trait AppTest extends UnitDbTest with ConfigTest with Results with GuiceOneAppPerSuite
