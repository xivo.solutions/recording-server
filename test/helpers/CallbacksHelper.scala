package helpers

import java.util.{Date, UUID}

import models.{CallbackStatus, CallbackTicket}
import org.joda.time.{LocalTime, LocalDate, DateTime}
import org.joda.time.format.DateTimeFormat
import java.sql.Connection
import anorm._
import anorm.SqlParser._

object CallbacksHelper {
  val dateFormat = "yyyy-MM-dd HH:mm:ss"
  val formatter = DateTimeFormat.forPattern(dateFormat)

  val simple = get[Option[UUID]]("uuid") ~
    get[UUID]("callback_list_uuid") ~
    get[UUID]("callback_request_uuid") ~
    get[String]("queue_ref") ~
    get[String]("agent_num") ~
    get[Date]("due_date") ~
    get[Date]("started") ~
    get[Option[Date]]("last_update") ~
    get[Option[String]]("callid") ~
    get[Option[String]]("status") ~
    get[Option[String]]("comment") ~
    get[Option[String]]("phone_number") ~
    get[Option[String]]("mobile_phone_number") ~
    get[Option[String]]("firstname") ~
    get[Option[String]]("lastname") ~
    get[Option[String]]("company") ~
    get[Option[String]]("description") map {
    case uuid ~ listUuid ~ cbUuid ~ queue ~ agent ~ dueDate ~ started ~ updated ~ callid ~ status ~ comment ~
      phone ~  mobile ~ firstname ~ lastname ~ company ~ description =>
      CallbackTicket(uuid, listUuid, cbUuid, queue, agent, new LocalDate(dueDate), new DateTime(started), updated.map(new DateTime(_)),
        callid, status.map(CallbackStatus.withName), comment, phone,  mobile, firstname, lastname, company, description)
    }

  def allTickets(implicit c: Connection): List[CallbackTicket] =
    SQL("SELECT uuid, callback_list_uuid, callback_request_uuid, queue_ref, agent_num, due_date, started, last_update, callid, status::varchar, " +
      "comment, phone_number, mobile_phone_number, firstname, lastname, company, description FROM callback_ticket").as(simple.*)

  def insertPeriod(start: LocalTime, end: LocalTime)(implicit c: Connection): UUID = SQL("INSERT INTO preferred_callback_period(period_start, period_end) VALUES ({start}, {end})")
    .on(Symbol("start") -> start.toDateTimeToday.toDate, Symbol("end") -> end.toDateTimeToday().toDate)
    .executeInsert((get[UUID]("uuid")).*).head

  def insertRequest(periodUuid: UUID, phoneNumber: String)(implicit c: Connection): UUID =
      SQL("INSERT INTO callback_request(preferred_callback_period_uuid, list_uuid, phone_number) VALUES ({pUuid}, {random}, {number})")
        .on(Symbol("pUuid") -> periodUuid, Symbol("number") -> phoneNumber, Symbol("random") -> UUID.randomUUID())
        .executeInsert((get[UUID]("uuid")).*).head
}
