DROP TABLE IF EXISTS userfeatures CASCADE;

CREATE TABLE userfeatures (
    id SERIAL PRIMARY KEY,
    firstname VARCHAR(128),
    lastname VARCHAR(128)
);
