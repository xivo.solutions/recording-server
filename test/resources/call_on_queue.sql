DROP TABLE IF EXISTS "call_on_queue" CASCADE;
DROP TYPE IF EXISTS "call_exit_type" CASCADE;

CREATE TYPE "call_exit_type" AS ENUM (
  'full',
  'closed',
  'joinempty',
  'leaveempty',
  'divert_ca_ratio',
  'divert_waittime',
  'answered',
  'abandoned',
  'timeout'
);

CREATE TABLE "call_on_queue" (
 "id" SERIAL PRIMARY KEY,
 "callid" VARCHAR(32) NOT NULL,
 "queue_time" TIMESTAMP WITHOUT TIME ZONE NOT NULL,
 "status" call_exit_type,
 "queue_ref" VARCHAR(128),
 "agent_num" VARCHAR(128)
);

CREATE INDEX "call_on_queue__idx__callid" ON call_on_queue(callid);
