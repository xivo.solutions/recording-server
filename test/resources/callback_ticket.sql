DROP TABLE IF EXISTS callback_ticket CASCADE;
DROP TYPE IF EXISTS callback_status CASCADE;

CREATE OR REPLACE FUNCTION uuid_generator()
RETURNS TRIGGER
LANGUAGE plpgsql
AS $$
BEGIN
    NEW.uuid := uuid_generate_v4();
    RETURN NEW;
END;
$$;

CREATE TYPE callback_status AS ENUM ('fax', 'noanswer', 'answered', 'callback', 'voicemail','email');

CREATE TABLE callback_ticket (
  uuid UUID PRIMARY KEY,
  callback_list_uuid UUID NOT NULL,
  callback_request_uuid UUID NOT NULL,
  queue_ref VARCHAR(128) NOT NULL,
  agent_num VARCHAR(40) NOT NULL,
  due_date DATE NOT NULL DEFAULT now(),
  started TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  last_update TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  callid VARCHAR(32),
  status callback_status,
  comment TEXT,
  phone_number VARCHAR(40),
  mobile_phone_number VARCHAR(40),
  firstname VARCHAR(128),
  lastname VARCHAR(128),
  company VARCHAR(128),
  description TEXT
);

CREATE TRIGGER callback_ticket_uuid_trigger BEFORE INSERT ON callback_ticket FOR EACH ROW EXECUTE PROCEDURE uuid_generator();