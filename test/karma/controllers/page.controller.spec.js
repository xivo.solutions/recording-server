describe('Page controller', function() {
  var scope;
  var ctrl;
  var $window;

  beforeEach(angular.mock.module('recording-server'));

  beforeEach(angular.mock.inject(function($rootScope, $controller, _$window_) {
    scope = $rootScope.$new();
    $window = _$window_;
    var mockJqueryObject = {getDate:function(){ return moment();}};
    spyOn($.fn, "data").and.returnValue(mockJqueryObject);

    ctrl = $controller('PageController', { '$scope': scope, '$window' : $window});
  }));

  it('can instantiate controller', function(){
    expect(ctrl).not.toBeUndefined();
  });

  it('opens url to download access log file', function(){
    spyOn($window, 'open');
    scope.getAccessLog();
    expect($window.open).toHaveBeenCalledWith('records/access');
  });

});