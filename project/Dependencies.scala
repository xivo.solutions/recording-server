import play.sbt.PlayImport._
import sbt._

object Version {
  val akka = "2.6.21"
}

object Library {
  val playauthentication =
    "solutions.xivo" %% "play-authentication" % "2023.08.00-play2.8"
  val playws     = "com.typesafe.play"       %% "play-ahc-ws-standalone"  % "2.1.2"
  val playwsjson = "com.typesafe.play"       %% "play-ws-standalone-json" % "2.1.2"
  val postgresql = "org.postgresql"           % "postgresql"              % "42.2.23"
  val anorm      = "org.playframework.anorm" %% "anorm"                   % "2.6.8"
  val anormAkka  = "org.playframework.anorm" %% "anorm-akka"              % "2.6.8"
  val dbunit     = "org.dbunit"               % "dbunit"                  % "2.7.0"   % "test"
  val mockito    = "org.mockito"              % "mockito-all"             % "1.10.19" % "test"
  val scalatestplay =
    "org.scalatestplus.play" %% "scalatestplus-play" % "5.1.0" % "test"
  val scalatestmock = "org.scalatestplus" %% "mockito-3-4" % "3.2.3.0" % "test"
  val akkaTestKit =
    "com.typesafe.akka" %% "akka-stream-testkit" % Version.akka % "test"
  val akkaTyped = "com.typesafe.akka" %% "akka-actor-typed" % Version.akka
  val akkaSlf4j = "com.typesafe.akka" %% "akka-slf4j"       % Version.akka
  val akkaJackson =
    "com.typesafe.akka" %% "akka-serialization-jackson" % Version.akka
  val akkaStream = "com.typesafe.akka" %% "akka-stream" % Version.akka
}

object Dependencies {

  import Library._

  val scalaVersion = "2.13.4"

  val resolutionRepos = Seq(
    "Local Maven Repository" at "file:///" + Path.userHome.absolutePath + "/.m2/repository",
    "Typesafe repository" at "https://repo.typesafe.com/typesafe/releases/"
  )

  val runDep = Seq(
    playws,
    playwsjson,
    jdbc,
    anorm,
    evolutions,
    ehcache,
    ws,
    playauthentication,
    postgresql,
    guice,
    dbunit,
    mockito,
    scalatestplay,
    scalatestmock,
    anormAkka,
    specs2 % Test,
    akkaTestKit,
    akkaTyped,
    akkaSlf4j,
    akkaJackson,
    akkaStream
  )
}
