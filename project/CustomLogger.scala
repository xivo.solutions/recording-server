import sbt.ConsoleLogger
import scala.sys.process._

class CustomLogger extends ProcessLogger {
  val buf = new StringBuffer
  val cl = ConsoleLogger()
  def buffer[T](f: ⇒ T): T = f
  def err(s: ⇒ String): Unit = {
    buf.append(s).append('\n')
    cl.error(s)
  }
  def out(s: ⇒ String): Unit = {
    buf.append(s).append('\n')
    cl.info(s)
  }
}
